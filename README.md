## Installation

#### Via pip

* `pip install .`

#### Python venv

* create a virtual environment: `python -m venv venv`
* activate it: `source venv/bin/activate`
* install with pip: `pip install .`
