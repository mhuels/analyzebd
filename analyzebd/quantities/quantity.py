import numpy as np
from pathlib import Path
from analyzebd.helpers.file_reader import skipper
from scipy.fft import fft, ifft
from analyzebd.filters.frequency_filter import FrequencyFilter
from analyzebd.units.unit import Unit, invertString
from copy import deepcopy
import warnings
from analyzebd.helpers.file_reader import find_setting_in_file
from pandas import read_csv
from tqdm import tqdm
from analyzebd.helpers.latex import LatexLabel
from analyzebd.helpers import Clock

class Quantity:
    def __init__(self, **kwargs):
        self.reset()
        self.setup(**kwargs)

    def reset(self):
        self.name = None
        self.name_abr = None
        self.indices = None
        self.numberOfEnsembles = 1
        self.array = None
        self.unit = Unit()
        self.metadata = None
        self.iStart = 0
        self.iEnd = None
        self.iInterval = 1
        self.src = None
        self.name_latex = None
        self.index_latex = None
        self.name_latex_fft = None
        return self

    def setup(self, name="quantity", metadata=None, array=None, name_abr=None, name_latex: LatexLabel=None, name_latex_fft: LatexLabel=None):
        self.name = name
        self.metadata = metadata
        if name_abr is not None:
            self.name_abr = name_abr
        else:
            self.name_abr = self.name[0]
        if array is not None:
            self.array = array
        if isinstance(name_latex, str):
            name_latex = LatexLabel(name=name_latex)
        self.name_latex = name_latex
        if isinstance(name_latex_fft, str):
            name_latex_fft = LatexLabel(name=name_latex_fft)
        self.name_latex_fft = name_latex_fft
        return self

    def __getitem__(self, key):
        try:
            return self._a2v(self.array[key])
        except TypeError:
            return None

    def __setitem__(self, key, value):
        self.array[key] = self._v2a(value)
        return self

    def __str__(self):
        return str(self.getArray())

    def __len__(self):
        return len(self.array)

    def __call__(self, *args, **kwargs):
        if 'v' in kwargs.keys():
            return self._v2i(kwargs['v'])
        elif 'i' in kwargs.keys():
            return self._i2v(kwargs['i'])
        else:
            return None

    def __iadd__(self, other):
        if isinstance(other, Quantity):
            if other.array.shape != self.array.shape:
                raise TypeError(f"you are trying to add two Quantities with different shapes! ({other.array.shape} != {self.array.shape})")
            self.array += other.array
        else:
            try:
                self.array += self._v2a(other)
            except Exception:
                raise TypeError(f"'+=' or '+' operator is not defined for {type(other)} and {type(self)}")
        return self

    def __isub__(self, other):
        if isinstance(other, Quantity):
            if other.array.shape != self.array.shape:
                raise TypeError(f"you are trying to substract two Quantities with different shapes! ({other.array.shape} != {self.array.shape})")
            self.array -= other.array
        else:
            try:
                self.array -= self._v2a(other)
            except Exception:
                raise TypeError(f"'-=' operator is not defined for {type(other)} and {type(self)}")
        return self

    def __imul__(self, other):
        if isinstance(other, Quantity):
            if other.array.shape != self.array.shape:
                raise TypeError(f"you are trying to multiply two Quantities with different shapes! ({other.array.shape} != {self.array.shape})")
            self.array *= other.array
        else:
            try:
                self.array *= other
            except Exception:
                raise TypeError(f"'*=' operator is not defined for {type(other)} and {type(self)}")
        return self

    def __itruediv__(self, other):
        if isinstance(other, Quantity):
            if other.array.shape != self.array.shape:
                raise TypeError(f"you are trying to divide two Quantities with different shapes! ({other.array.shape} != {self.array.shape})")
            self.array /= other.array
        else:
            try:
                self.array /= other
            except Exception:
                raise TypeError(f"'/=' operator is not defined for {type(other)} and {type(self)}")
        return self

    def __neg__(self):
        result = deepcopy(self)
        result.array = -result.array
        return result

    def __add__(self, other):
        result = deepcopy(self)
        result += other
        return result

    def __sub__(self, other):
        result = deepcopy(self)
        result -= other
        return result

    def __mul__(self, other):
        result = deepcopy(self)
        result *= other
        return result

    def __truediv__(self, other):
        result = deepcopy(self)
        result /= other
        return result

    def _i2a(self, i: int):
        """
        index to array (i -> a)
        """
        return self.array[i]

    def _a2i(self, a):
        """
        array to index (a -> i)
        :return int
        """
        d = np.abs(a - self.array)
        argmin = np.asarray(d == d.min()).nonzero()
        if len(argmin[0]) > 1:
            # multiple indices are equally close
            if len(argmin[0]) == 2 and abs(argmin[0][0] - argmin[0][1]) == 1:
                # it's only two indices; that's okay if they are neighbours (+-1)
                pass
            else:
                raise ValueError(f"There are multiple non-neighbour indices ({argmin}), which match a={a} exactly. The function is therefore not invertible.")
        return argmin[0][0]

    def _a2v(self, a):
        """
        array to value (a -> v)
        """
        return a * self.unit.conversion

    def _v2a(self, v):
        """
        value to array (v -> a)
        """
        return v / self.unit.conversion

    def _i2v(self, i):
        """
        index to value (i -> a -> v)
        """
        return self._a2v(self._i2a(i))

    def _v2i(self, v):
        """
        value to index ( v -> a -> i)
        """
        return self._a2i(self._v2a(v))

    # intended to wrap numpy array functionality; leads to problem with deepcopy operations
    # def __getattr__(self, item):
    #     return self.array.item

    def min(self, axis=None):
        return np.nanmin(self.getArray(), axis=axis)

    def max(self, axis=None):
        return np.nanmax(self.getArray(), axis=axis)

    def setArray(self, array):
        self.array = self._v2a(array)
        return self

    def getArray(self):
        return self._a2v(self.array)

    def readMetadata(self, folder, **kwargs):
        folder = Path(folder)
        if self.metadata is not None:
            try:
                self.metadata.readFromFile(folder, **kwargs)
            except AttributeError:
                warnings.warn("metadata does not support file-reading.")
        return self

    # might want to need support for iInterval (involved task though)
    # TODO: unit specification
    def readFromFile(self, folder, filename, iStart=None, iEnd=None, columns=0, numberOfEnsembles=None,
                     skipMetadata=False):
        """
        Needs to be overwritten by its children.
        """
        self.src = Path(folder) / filename
        if not self.src.exists():
            raise FileNotFoundError(f"{self.src} does not exist!")
        self.readHeaderFromFile(folder, filename, columns=columns)
        if not skipMetadata:
            self.readMetadata(self.src.parent)
        if numberOfEnsembles is None:
            numberOfEnsembles = self.__readNumberOfEnsembles()
        self.numberOfEnsembles = numberOfEnsembles

        if iStart is not None:
            self.iStart = iStart
        self.iEnd = iEnd

        if self.numberOfEnsembles is None:
            # TODO: __readFromFileSingle doesn't work with self.iStart because skiprows doesn't skip commented lines
            # self.setArray(self.__readFromFileSingle(self.iStart, self.get_iLength(), columns=columns))
            self.numberOfEnsembles = 1
            self.setArray(self.__readFromFileEnsembles(self.numberOfEnsembles, columns=columns))
        else:
            self.setArray(self.__readFromFileEnsembles(self.numberOfEnsembles, columns=columns))
        if self.metadata is not None:
            self.metadata.numberOfEnsembles = self.numberOfEnsembles
        return self

    def readHeaderFromFile(self, folder, filename, columns=0, comment='#'):
        path = Path(folder) / filename
        if isinstance(columns, int):
            columns = [columns]
        header = []
        with open(path, 'r') as f:
            line = f.readline()
            while line:
                if line.startswith(comment):
                    header.append(line[1:])     # remove the first character (comment)
                    line = f.readline()
                else:
                    break
        self._readIndicesFromHeader(header, columns=columns)
        self._readNameFromHeader(header, columns=columns)
        self._readUnitFromHeader(header, self.name_abr)
        return self

    def _readIndicesFromHeader(self, header, columns):
        indices = [header[-1].split()[i] for i in columns]  # last line contains indices
        self.indices = []
        import re
        for index in indices:
            if "_" not in index and '(' not in index:
                index_clear = None
            else:
                # filter empty strings, split at either '_', '(' or ')'
                index_clear = list(filter(None, re.split('_|\(|\)', index)[1:]))
                if len(index_clear) == 1:
                    index_clear = index_clear[0]
            self.indices.append(index_clear)
        self.indices = np.array(self.indices)  # convert to numpy array
        return self.indices

    def _readNameFromHeader(self, header, columns):
        names_and_indices = [header[-1].split()[i] for i in columns]  # last line contains names + indices
        first = names_and_indices[0]
        import re
        name_abr = re.split('_|\(|\)', first)[0]
        name = None
        for line in header:
            if line.strip().startswith(name_abr):   # strip() removes leading and trailing whitespaces
                posColon = line.find(':')
                from analyzebd.helpers.strings import findFirstCharacter
                posEnd = findFirstCharacter(line[posColon+1:], ['(', '[', ',']) + posColon + 1
                name = (line[posColon + 1:posEnd]).lstrip().rstrip()
                break
        self.name = name
        self.name_abr = name_abr
        return self.name

    def _readUnitFromHeader(self, header, name_abr):
        unit = '1'
        for line in header:
            if line.lstrip().startswith(name_abr):  #lstrip removes whitespace/spaces on the left
                if '[' in line and ']' in line:
                    start = line.rfind('[') + 1     # find last occurrance
                    end = line.rfind(']')
                    unit = line[start:end]
                break
        self.setScale(unit)
        self.unit.conversion = 1    # TODO: this is a quick and dirty approach
        return self.unit

    def writeToFile(self, filename, header=None, fmt="% .8e", delimiter='\t', verbose=False, overwrite=True):
        array = self.getArray()
        if header is None:
            header = f"{self.name_abr}: {self.name} [{self.unit.name}]\n"
            fmt_h_length = len(f"{1.0:{fmt[1:]}}")  # test length of fmt with example float (1.0), remove first character '%'
            if self.indices is None:
                indices = [None]
            else:
                indices = self.indices
            for i, index in enumerate(indices):
                identifier = self.name_abr
                if index is not None:
                    identifier += "_" + self.index2string(index)
                if i == 0:
                    # default of np.savetxt(comment='# ') is 2 characters wide
                    header += f"{identifier:>{fmt_h_length - 2}s}"
                else:
                    header += f"{delimiter}{identifier:>{fmt_h_length}s}"
        if overwrite:
            try:
                Path(filename).unlink()
            except FileNotFoundError:
                pass
        with open(filename, "a") as f:
            if self.numberOfEnsembles == 1:
                array = np.reshape(array, (-1, len(indices)))
                np.savetxt(f, array, fmt=fmt, delimiter=delimiter, header=header)
            else:
                array = np.reshape(array, (self.numberOfEnsembles, -1, len(indices)))
                for i in range(self.numberOfEnsembles):
                    headerEns = f"Ensemble: {i}\n"
                    if i == 0:
                        headerEns = f"numberOfEnsembles: {self.numberOfEnsembles}\n" + headerEns
                    np.savetxt(f, array[i], fmt=fmt, delimiter=delimiter,
                               header=headerEns + header)
        if verbose:
            print(f"Saved {self.name} in {filename}")
        return self

    # TODO: def save_hdf5(self, filename, name: str, verbose: bool=False):
    # TODO: def load_hdf5
    # TODO: def load_from_dataset(self, dataset, verbose: bool=False)
    def save_as_dataset(self, f, name: str, verbose: bool=False):
        """
        Save quantity as dataset in an already opened h5py file or group.
        Args:
            f: h5py.File or h5py.Group
            name: str
            verbose: bool

        Returns:
            self: h5py._hl.dataset.Dataset
        """
        clock = Clock()
        if name in list(f.keys()):
            dataset = f[name]
            dataset[:] = self.array     # TODO: maybe needs to overwrite if not fitting
        else:
            dataset = f.create_dataset(name, data=self.array)
        f[name].attrs["dtype"] = "Quantity"
        f[name].attrs["name"] = self.name
        f[name].attrs["name_abr"] = self.name_abr
        f[name].attrs["name_latex"] = self.name_latex.name
        f[name].attrs["unit"] = self.unit.name
        f[name].attrs["unit_latex"] = self.unit.latex
        f[name].attrs["conversion"] = self.unit.conversion
        if verbose:
            print(f"Saved quantity '{self.name}' as dataset '{name}' in {f.filename} ... {clock()}")
        return dataset

    def index2string(self, index):
        s = ""
        for i in range(len(index)):
            s += str(index[i])
            if i < len(index) - 1:
                s += "_"
        return s

    def __readFromFileSingle(self, iStart, iLength, columns=0):
        if isinstance(columns, int):
            columns = [columns]
        # this doesn't work right now, because pd.read_csv() for some reason calls skiprows(x) multiple times per row
        # with tqdm(total=iLength, unit='lines') as bar:
        #     def skiprows(x):
        #         if x < iStart:
        #             return True
        #         else:
        #             bar.update(1)
        #             return False
        #     df = pd.read_csv(self.src, skiprows=skiprows, nrows=iLength, usecols=columns, comment='#', sep=r'\s+',
        #                  header=None)
        df = read_csv(self.src, skiprows=iStart, nrows=iLength, usecols=columns, comment='#', sep=r'\s+',
                         header=None)
        # skiprows counts commented lines as well (problematic?)
        array = df.to_numpy()
        array = np.squeeze(array)
        # if self.iEnd is None:
        #     self.iEnd = len(array)
        return array

    def __readFromFileEnsembles(self, numberOfEnsembles, columns=0):
        numberOfLines = self.__readNumberOfLines()
        if self.iEnd is not None:
            max_rows = self.iEnd
        else:
            max_rows = numberOfLines
        if isinstance(columns, int):
            numberOfColumns = 1
        elif isinstance(columns, list):
            numberOfColumns = len(columns)
        else:
            raise TypeError("Invalid type for columns. Use either int or list of int.")

        # read all at once and reshape (always the preferred way, at least with pd.read_csv())
        array = self.__readFromFileSingle(0, numberOfEnsembles * numberOfLines, columns=columns)

        # check if reshapable
        old_shape = array.shape
        new_shape = (numberOfEnsembles, numberOfLines, numberOfColumns)
        prod = lambda x: np.array(x).prod()
        if prod(old_shape) != prod(new_shape):
            raise ValueError(f"old_shape={old_shape} not reshapable into new_shape={new_shape}. The source file might be inhomogeneous/corrupt: {self.src}")
        array = np.reshape(array, new_shape)
        array = array[:, self.iStart:max_rows]
        array = np.squeeze(array)

        # read one at a time (with correct length) and stack afterwards (slow!)
        # arrays = []
        # for i in range(numberOfEnsembles):
        #     iStart = self.iStart + i * numberOfTimesteps
        #     arrays.append(self.__readFromFileSingle(iStart, max_rows, columns))
        # array = np.stack(arrays)
        # array = np.squeeze(array)
        return array

    def __readNumberOfEnsembles(self):
        """
        Needs self.src to be set.
        :return: N (int) or None (if only single file)
        """
        N = find_setting_in_file(self.src, "numberOfEnsembles", dtype=int, skipComment=False)
        if N is None:
            with open(self.src, "r") as f:
                data = f.read()
            N = data.count("Ensemble")
            if N == 0:
                N = None
        return N

    def __readNumberOfLines(self):
        """
        Needs self.src to be set.
        """
        # read only first ensemble (the rest is the same)
        N = 0
        ensemble_counter = 0
        with open(self.src, "r") as f:
            for line in f:
                if "Ensemble:" in line:
                    ensemble_counter += 1
                    if ensemble_counter > 1:
                        break
                if line.startswith("#"):
                    continue
                else:
                    N += 1
        return N

    def get_iLength(self):
        if self.iStart is not None and self.iEnd is not None:
            return self.iEnd - self.iStart
        else:
            return None

    def setScale(self, unit, **kwargs):
        if isinstance(unit, Unit):
            self.unit = unit
        elif isinstance(unit, str):
            self.unit.set(name=unit, **kwargs)
        else:
            raise ValueError(f"unit needs to be either an instance of Unit or str.")
        return self

    def crop(self, start=None, end=None, inPlace=False):
        from analyzebd.helpers.arrays import is_monotonously_increasing
        if not is_monotonously_increasing(self.getArray()):
            raise ValueError(f"This object of type {type(self)} is not monotonously increasing. Cropping would be ambiguous.\n"
                             f"len={len(self)}, min={self.min()}, max={self.max()}, src={self.src}")
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        if start is not None:   # else keep it as it is
            obj.iStart = obj._v2i(start)
        else:
            obj.iStart = None   # this might lead to errors in some cases
        if end is not None:     # else keep it as it is
            obj.iEnd = obj._v2i(end) + 1    #TODO: +1 might lead to errors sometimes?
        else:
            obj.iEnd = None     # this might lead to errors in some cases
        obj.array = obj.array[obj.iStart:obj.iEnd]  # dont need to call obj.slice() because we have a one-dimensional array
        return obj

    def rcrop(self, diff: float=None, inPlace=False):
        """
        Crop the quantity by a certain distance from the end.
        :param diff: float
        :param inPlace: bool
        :return: Quantity
        """
        end = max(self)
        if diff is not None:
            start = end - diff
        else:
            start = None
        obj = self.crop(start, end, inPlace=inPlace)
        return obj

    def slice(self, slc=None, start=None, stop=None, step=None, inPlace=False, keep_dims=False):
        """
        Slice the data array along multiple axes (slc!=None) or along its primary axis (slc=None). The primary axis is
        the axis, which contains the functional dependence.
        :param slc: tuple of slice
        :param start: int
        :param stop: int
        :param step: int
        :return: self
        """
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        if slc is None:
            if obj.numberOfEnsembles is not None and obj.numberOfEnsembles > 1:
                slc = (slice(None), slice(start, stop, step))
            else:
                slc = (slice(start, stop, step),)
        if not isinstance(slc, tuple):
            raise TypeError(f"Only tuples are allowed, e.g. (slice(0),)")
        # self.iStart += slc.start
        # self.iEnd -= slc.stop
        # self.iInterval *= slc.step
        # if keep_dims: # TODO: implement keep_dims
        #     new_slc = (,)
        #     for elem in slc:
        #         new_slc += (elem,)
        #         if elem
        obj.array = obj.array[slc]
        # check if slc cuts whole dimensions in the last relevant (index) axes
        # if Ellipsis in slc[:-1]:  # this would be nice
        if slc[0] == Ellipsis:  # only in case one of the last dimensions was sliced
            obj.indices = obj.indices[slc]  # apply the slicing to indices as well
            if obj.indices.size == 1:
                obj.name_latex.appendSubScript(str(obj.indices))
        # else:
        #     import warnings
        #     warnings.warn("Slicing without the Ellipsis (...) does not work yet for indices.")
        #     # TODO: nothing should happen if numberOfEnsembles or numberOfTimesteps are sliced

        # obj.array = np.squeeze(obj.array)
        if not isinstance(obj.array, np.ndarray):  # should always be a np.array with at least one axis
            obj.array = np.expand_dims(obj.array, axis=0)
        if not isinstance(obj.indices, np.ndarray) or len(obj.indices.shape) == 0:  # should always be a np.array with at least one axis
            obj.indices = np.expand_dims(obj.indices, axis=0)
        return obj

    def slice_ensembles(self, stop, start=None, step=None, inPlace=False):
        from analyzebd.helpers.copy import copy_inplace
        obj = copy_inplace(self, inPlace)
        if obj.numberOfEnsembles == 1:
            pass
        else:
            if start is None:
                slc = (stop, ...)
            else:
                slc = (slice(start, stop, step), ...)
            obj.slice(slc=slc, inPlace=True)
        return obj

    def extrapolate(self, start: float=None, stop: float=None, type='linear', inPlace=False):
        if len(self.array.shape) > 1:
            raise NotImplementedError(f"Not implemented for shape={self.array.shape} yet.")
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        if type != 'linear':
            raise NotImplementedError
        N = len(self)
        xMin = self[0]
        xMax = self[-1]
        xDiff = xMax - xMin
        dx = xDiff / (N - 1)
        newArray = self.getArray()
        if start is not None:
            prepend = np.arange(start, xMin, dx)
            # make sure the last value of prepend doesn't coincide with the first value of newArray within numerical accuracy
            if np.isclose(prepend[-1], newArray[0]):
                prepend = prepend[:-1]
            newArray = np.insert(newArray, 0, prepend)
        if stop is not None:
            append = np.arange(xMax + dx, stop, dx)
            newArray = np.append(newArray, append)
        obj.setArray(newArray)
        return obj


    def getLatexLabel(self, fft=False, fft_kind="abs"):
        if self.name_latex is None:
            name_latex = LatexLabel(name="\\mathrm{" + self.name.replace(' ', '\ ') + "}")
        elif isinstance(self.name_latex, str):
            name_latex = LatexLabel(name=self.name_latex)
        else:
            name_latex = self.name_latex
        # TODO: might need to the same with self.name_latex_fft
        unit_latex = self.unit.getLatex(fft)
        if unit_latex == "1":
            unit_latex = ""
            unit_delim = ""
        elif "^{-1}" in unit_latex:
            unit_latex = invertString(unit_latex, suffix="^{-1}")
            unit_delim = ""
        else:
            unit_delim = "/"

        # self.name_latex may not contain a subscript (might need more intelligent subscript function)
        if self.indices is not None and self.indices[0] is not None:
            name_latex.setSubScripts(self.indices.tolist())     # maybe unnecessary if name_latex.subscripts is already correct?
        if fft:
            name_latex.prependEnclosing(("\\mathcal{F}\\left[", "\\right]"))
            if fft_kind == "abs":
                name_latex.prependEnclosing(("", "^{\\mathrm{abs}}"))
            elif fft_kind == "cos":
                name_latex.prependEnclosing(("", "^{\\cos}"))
            elif fft_kind == "sin":
                name_latex.prependEnclosing(("", "^{\\sin}"))
        if name_latex.getModulo() is not None:
            modulo = f" {name_latex.getModulo()}"
        else:
            modulo = ""
        label = f"${name_latex.getLabel(with_dollars=False, with_modulo=False)}{unit_delim}{unit_latex}{modulo}$"
        return label

    def _getDirectionLabel(self, direction):
        label = ""
        if direction == 0:
            label = "_x"
        elif direction == 1:
            label = "_y"
        elif direction == 2:
            label = "_z"
        return label

    def getTotalAverage(self):
        if self.numberOfEnsembles == 1:
            return self[:].mean(axis=0)
        else:
            return self[:].mean(axis=1)

    def buildEnsembleAverage(self, inPlace=False):
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        if obj.numberOfEnsembles > 1:
            obj.array = obj.array.mean(axis=0)
        obj.numberOfEnsembles = 1
        obj.name = "average " + obj.name
        obj.name_latex.prependEnclosing(("\\left<", "\\right>"))
        return obj

    def buildEnsembleStd(self, inPlace=False, side: str=None):
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        if obj.numberOfEnsembles > 1:
            if side is None:
                # this is the long version of np.std(, axis=0)
                # a = obj.array
                # mean = a.mean(axis=0)
                # x = np.abs(a - mean) ** 2
                # var = np.mean(x, axis=0)
                # std = np.sqrt(var)
                # obj.array = std
                obj.array = obj.array.std(axis=0)
            elif side == "upper":
                a = obj.array
                mean = a.mean(axis=0)
                theta = (a >= mean)
                theta = np.logical_or(theta, np.isclose(a, mean))   # catches the case if a == mean (within numerical accuracy)
                N = theta.sum(axis=0)
                x = np.abs(a - mean) ** 2 * theta
                var = np.sum(x, axis=0) / N
                std = np.sqrt(var)
                obj.array = std
            elif side == "lower":
                a = obj.array
                mean = a.mean(axis=0)
                theta = (a <= mean)
                theta = np.logical_or(theta, np.isclose(a, mean))   # catches the case if a == mean (within numerical accuracy)
                N = theta.sum(axis=0)
                x = np.abs(a - mean) ** 2 * theta
                var = np.sum(x, axis=0) / N
                std = np.sqrt(var)
                obj.array = std
            else:
                raise ValueError(f"side={side} is not supported. Pick either 'upper', 'lower' or None!")
            obj.name = "Std[" + obj.name + "]"
            obj.name_latex.prependEnclosing(("\\mathrm{Std) \\left[", "\\right]"))
        else:
            obj.array.fill(0)  # there is no standard deviation of a single ensemble
        obj.numberOfEnsembles = 1
        return obj

    def buildEnsembleVar(self, inPlace=False):
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        if obj.numberOfEnsembles > 1:
            obj.array = obj.array.var(axis=0)
            obj.unit.name += "2"
            obj.unit.latex += "^2"
            obj.unit.conversion *= obj.unit.conversion
            obj.name = "Var[" + obj.name + "]"
            obj.name_latex.prependEnclosing(("\\mathrm{Var} \\left[", "\\right]"))
        else:
            obj.array.fill(0)  # there is no variance of a single ensemble
        obj.numberOfEnsembles = 1
        return obj

    def buildEnsemblePercentile(self, k, inPlace=False):
        """
        Build k-th percentile along the ensemble axis.
        :param k: float or np.array of floats
        :param inPlace: bool
        :return: Quantity
        """
        if isinstance(k, np.ndarray):
            if np.min(k) < 0 or np.max(k) > 1:
                raise ValueError(f"percentile needs to be between 0 and 1.")
        elif isinstance(k, float):
            if k < 0. or k > 1.:
                raise ValueError(f"k={k}-th percentile needs to be between 0 and 1.")
        else:
            raise TypeError
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        if obj.numberOfEnsembles > 1:
            obj.array = np.percentile(obj.array, 100*k, axis=0)     #numpy requires numbers between 0 and 100
        obj.numberOfEnsembles = 1
        return obj

    def buildDistribution(self, bins=10, limits=None):
        if self.numberOfEnsembles == 1:
            raise ValueError("Not enough ensembles to build a distribution.")
        if limits is None:
            limits = self.min(), self.max()
        numberOfTimesteps = len(self[0])
        psi = np.zeros((bins, numberOfTimesteps))
        bin_edges = None
        for i in range(numberOfTimesteps):
            psi[:, i], bin_edges = np.histogram(self[:, i], range=limits, bins=bins, density=True)
        # eventually manipulate bin_edges
        bin_centers = bin_edges[:-1] + (bin_edges[1:] - bin_edges[:-1]) / 2
        return psi, bin_centers

    def map_periodic(self, period: float, inPlace=False):
        """
        Map values to interval [0, period].
        :param period: float
        :param inPlace: bool
        :return: Quantity
        """
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        a = obj.getArray()
        a = np.fmod(a, period)
        obj.setArray(a)
        obj.name_latex.setModulo(period)
        return obj

    def tracePeak(self, bins=1000, limits=None, inPlace=False):
        if len(self.array.shape) != 2:
            raise NotImplementedError
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        psi, bin_centers = obj.buildDistribution(bins=bins, limits=limits)
        _, numberOfTimesteps = psi.shape
        from scipy.signal import peak_widths, find_peaks
        peaks = np.zeros(numberOfTimesteps)
        #find highest peak
        for i in range(numberOfTimesteps):
            tmp_peaks, _ = find_peaks(psi[:, i])
            max_psi = 0
            argmax = 0
            for j in tmp_peaks:
                if psi[j, i] > max_psi:
                    max_psi = psi[j, i]
                    argmax = j
            # print(f"i={i}; tmp_peaks={tmp_peaks}; argmax={argmax}")
            peaks[i] = bin_centers[argmax]  # TODO: interpolate between bin_centers
        obj.array = peaks
        return peaks
        # widths, width_heights, left_ips, right_ips = peak_widths(psi[:, 100], peaks)
        # TODO: optionally return left_ips and right_ips as well

    # filter-option?
    def get_lim(self, slice=None, symmetric=False, buffer=None):
        # if filter is not None:
        #     y = self.filterNoise(filter=filter)[:]
        # else:
        #     y = self[:]
        y = self[:]
        if slice is not None:
            y = y[slice]
        ymin = 0
        ymax = 0
        if symmetric:
            yAbs = max(abs(np.nanmin(y)), abs(np.nanmax(y)))
            # yAbs = max(abs(y.min()), abs(y.max()))
            ymin = -yAbs
            ymax = yAbs
        else:
            ymin = np.nanmin(y)
            # ymin = y.min()
            ymax = np.nanmax(y)
            # ymax = y.max()
        if buffer is not None:
            ydiff = ymax - ymin
            ymin -= buffer * ydiff
            ymax += buffer * ydiff
        return [ymin, ymax]

    def getFFT(self, kind="raw", axis=None):
        if axis is None:
            if self.numberOfEnsembles == 1:
                axis = 0
            else:
                axis = 1
                # TODO: algorithm needs to be adapted
                raise NotImplementedError(f"not yet implemented for axis={axis}")
        N = len(self)
        y = self[:]
        yf = fft(y, axis=axis) / N
        if (N % 2 == 0):  # even
            delim = N // 2
        else:  # uneven
            delim = (N + 1) // 2
        # for real x, abs = sqrt(cos^2 + sin^2)
        if(kind == "abs"):
            yf_pos = yf[0:delim]
            yf_neg = np.flip(yf[delim:])  # yf_neg needs same order as yf_pos
            # insert 0 at beginning and remove last element if necessary
            yf_neg = np.insert(yf_neg, 0, 0, axis=axis)[:len(yf_pos)]
            # based on cos- and sin-definitions
            yf_cos = yf_pos + yf_neg
            yf_sin = 1j * (yf_pos - yf_neg)
            # need take cartesian norm for real and imaginary part separately
            yf_real = np.sqrt(np.real(yf_cos) ** 2 + np.real(yf_sin) ** 2)
            yf_imag = np.sqrt(np.imag(yf_cos) ** 2 + np.imag(yf_sin) ** 2)
            yf = yf_real + 1j * yf_imag     # put them back together
        elif(kind == "cos"):
            yf_pos = yf[0:delim]
            yf_neg = np.flip(yf[delim:])    # yf_neg needs same order as yf_pos
            # insert 0 at beginning and remove last element if necessary
            yf_neg = np.insert(yf_neg, 0, 0)[:len(yf_pos)]
            yf = yf_pos + yf_neg
        elif(kind == "sin"):
            yf_pos = yf[0:delim]
            yf_neg = np.flip(yf[delim:])  # yf_neg needs same order as yf_pos
            # insert 0 at beginning and remove last element if necessary
            yf_neg = np.insert(yf_neg, 0, 0)[:len(yf_pos)]
            yf = 1j * (yf_pos - yf_neg)
        else:
            # order from - to +
            yf = np.concatenate((yf[delim:], yf[0:delim]))
        return yf

    def filterNoise(self, filter=FrequencyFilter(), inPlace=False, axis=None):
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        if axis is None:
            if obj.numberOfEnsembles == 1:
                axis = 0
            else:
                axis = 1
                # TODO: algorithm needs to be adapted
                raise NotImplementedError(f"not yet implemented for axis={axis}")
        y = obj.array
        if filter is None:
            print("No filter given! Was this intended?")
            return y
        N = len(y)
        yf = fft(y, axis=axis) / N
        if filter.type == "abs":
            criterium = (np.abs(yf) < filter.threshold)
        elif filter.type == "rel":
            max_value = np.max(np.abs(yf), axis=axis)
            criterium = (np.abs(yf) / max_value < filter.threshold)
        elif filter.type == "avg":
            avg = np.mean(np.abs(yf), axis=axis)
            criterium = (np.abs(yf) / avg < filter.threshold)
        else:
            raise ValueError(f"type={filter.type} not supported!")
        filtered_yf = yf.copy()
        filtered_yf[criterium] = 0
        filtered_y = N * ifft(filtered_yf, axis=axis)
        filtered_y = np.real(filtered_y)
        obj.array = filtered_y
        return obj

    def count_unique(self, translate=None, as_dict=True, relative=False, **kwargs):
        """
        Count the quantity's unique elements and return them as a dictionary.
        Args:
            translate (func): function that takes an array of the unique values as only positional argument
            as_dict (bool): return unique_counts (dict) instead
            relative (bool): return relative counts instead (normalized by total counts)
            **kwargs: kwargs passed to translate
        Returns:
            unique (np.ndarray), counts (np.ndarray)
        """
        unique, counts = np.unique(self.getArray(), return_counts=True)
        # remove nan-values
        is_nan = np.isnan(unique)
        unique = unique[~is_nan]
        counts = counts[~is_nan]
        if relative:
            counts = counts / counts.sum()
        if translate is not None:
            unique_translated = translate(unique, **kwargs)
        else:
            unique_translated = unique
        if as_dict:
            unique_counts = dict(zip(unique_translated, counts))
            return unique_counts  # TODO: make this a Function instead of a dict
        else:
            return unique_translated, counts

import contextlib

@contextlib.contextmanager
def scaleAs(quantity, unit, **kwargs):
    prev_unit = deepcopy(quantity.unit)
    quantity.setScale(unit=unit, **kwargs)
    yield
    quantity.setScale(prev_unit)
    return True
