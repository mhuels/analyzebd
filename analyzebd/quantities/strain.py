from analyzebd.quantities.quantity import *
from analyzebd.metadata.shearedSlitporeBD import MetadataShearedSlitporeBD as Metadata

class Strain(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "strain"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "y"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "\\gamma"
        super().setup(**kwargs)
        self.setScale()
        return self

    def readFromFile(self, folder, filename="positions.out", **kwargs):
        if 'columns' not in kwargs:
            kwargs['columns'] = 1
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def setScale(self, unit="1", **kwargs):
        # if unit == "1":
        #     kwargs.setdefault("conversion", 1)
        #     kwargs.setdefault("latex", "1")
        #     kwargs.setdefault("latex_fft", "1")
        return super().setScale(unit, **kwargs)

    def generate(self, time=None):
        if time is None:
            N = self.metadata.numberOfTimesteps
            dt = self.metadata.dt
            time = np.arange(0, N * dt, dt)
        period = self.metadata.oscillationPeriod
        gamma0 = self.metadata.amplitude * period / (2 * np.pi)
        phi = self.metadata.phaseOffset * np.pi
        self.array = gamma0 * np.sin(2 * np.pi * time / period + phi)
        return self