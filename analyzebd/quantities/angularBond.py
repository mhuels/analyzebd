from analyzebd.quantities.quantity import *
from analyzebd.metadata.shearedSlitporeBD import MetadataShearedSlitporeBD as Metadata

class AngularBond(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "angular bond"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "psi"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "\\psi"
        super().setup(**kwargs)
        self.setScale()
        return self

    def readFromFile(self, folder, filename="angularBond.out", **kwargs):
        if 'columns' not in kwargs:
            kwargs['columns'] = [1, 2]
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def slice(self, slc=None, **kwargs):
        obj = super().slice(slc=slc, **kwargs)
        # change name_latex if the last index is sliced
        # TODO: this should be deprecated if self.indices is implemented completely
        # if slc is not None and len(slc) > 1 and isinstance(slc[-1], int):
        #     last_index = slc[-1]
        #     if last_index == 0:
        #         obj.name_latex = "\\psi_4"
        #     elif last_index == 1:
        #         obj.name_latex = "\\psi_6"
        return obj

    def setScale(self, unit="1", **kwargs):
        # if unit == "1":
        #     kwargs.setdefault("conversion", 1)
        #     kwargs.setdefault("latex", "1")
        #     kwargs.setdefault("latex_fft", "1")
        return super().setScale(unit, **kwargs)

    def getRelative(self, inPlace=False):
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        splits = np.split(obj.array, 2, axis=-1)  # splits in two parts along the last axis
        if obj.array.min() == 0:
            raise ZeroDivisionError(f"One of the angular bonds is zero. Please provide a fix in this algorithm.")
        psi4 = splits[0]
        psi6 = splits[1]
        psi_rel = psi6 / (psi4 + psi6)
        # if psi4[i] == 0 and psi6[i] == 0: psi_rel[i] = 0.5
        obj.array = psi_rel.squeeze()
        # correct name
        obj.name_latex = "\\psi_{rel}"
        obj.name = "relative " + obj.name
        # correct indices
        obj.indices = np.array([None])
        return obj
