from analyzebd.quantities.quantity import *
from analyzebd.metadata.drivenSingleParticle import MetadataDrivenSingleParticle as Metadata

class Position(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "position"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "x"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "x"
        if 'name_latex_fft' not in kwargs:
            kwargs['name_latex_fft'] = "k"  # maybe 2pi
        super().setup(**kwargs)
        self.setScale()
        return self

    def readFromFile(self, folder, filename="positions.out", **kwargs):
        if 'columns' not in kwargs:
            kwargs['columns'] = 1
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def setScale(self, unit="d", **kwargs):
        if unit == "d":
            kwargs.setdefault("conversion", 1)
            kwargs.setdefault("latex", "d")
        elif unit == "a":
            kwargs.setdefault("conversion", 1 / self.metadata.substratePeriod)
            kwargs.setdefault("latex", "a")
        return super().setScale(unit, **kwargs)