from analyzebd.quantities.quantity import *
from analyzebd.metadata.shearedSlitporeBD import MetadataShearedSlitporeBD as Metadata
from analyzebd.helpers.file_reader import find_setting_in_file

class LayerPosition(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "layer position"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "R"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "R"
        super().setup(**kwargs)
        self.setScale()
        return self

    # TODO: redefine this function as well
    # def __readFromFileSingle(self, iStart, iLength, columns=0):
    #     array = np.loadtxt(skipper(self.src), skiprows=iStart, max_rows=iLength, usecols=columns)
    #     return array

    def readFromFile(self, folder, filename="layerPositions.out", **kwargs):
        numberOfLayers = 2
        numberOfDirections = 3
        if 'columns' not in kwargs:
            # TODO: infer number of layers from layerPositions.out
            kwargs['columns'] = [i for i in range(1, 1 + numberOfLayers * numberOfDirections)]
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        if "position of layer" in self.name:
            self.name = "layer position"
        if self.numberOfEnsembles == 1:
            numberOfTimesteps, _ = self.array.shape
            self.array = np.reshape(self.array, (numberOfTimesteps, numberOfLayers, numberOfDirections))
        else:
            numberOfEnsembles, numberOfTimesteps, _ = self.array.shape
            self.array = np.reshape(self.array,
                                    (numberOfEnsembles, numberOfTimesteps, numberOfLayers, numberOfDirections))
        return self

    def setScale(self, unit="d", **kwargs):
        if unit == "d":
            kwargs.setdefault("conversion", 1)
            kwargs.setdefault("latex", "d")
        elif unit == "a":
            kwargs.setdefault("conversion", 1 / self.metadata.latticeConstant)
            kwargs.setdefault("latex", "a")
        return super().setScale(unit, **kwargs)

    def getRelative(self, inPlace=False):
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        #TODO: make these more intelligent (with indices)
        if obj.numberOfEnsembles == 1:
            numberOfTimepoints, numberOfLayers, dimensions = obj.array.shape
        else:
            numberOfEnsembles, numberOfTimepoints, numberOfLayers, dimensions = obj.array.shape
        if (numberOfLayers != 2):
            print(f"numberOfLayers (={numberOfLayers}) != 2. getRelative() is only designed for two layers.")
        # layer numbers counted from -infty to +infty
        if obj.numberOfEnsembles == 1:
            obj.array = obj.array[:, 1] - obj.array[:, 0]
        else:
            obj.array = obj.array[:, :, 1] - obj.array[:, :, 0]
        # correct name
        obj.name_latex.prependName("\\Delta ")
        obj.name = "relative " + obj.name
        obj.name_abr = "d" + obj.name_abr
        # correct indices
        obj.indices = obj.indices[:dimensions, 0]
        if obj.indices.size == 1:
            obj.name_latex.appendSubScript(str(obj.indices))
        return obj

    # def slice(self, slc=None, **kwargs):
    #     obj = super().slice(slc=slc, **kwargs)
    #     # change name_latex if the last index is sliced
    #     if slc is not None and len(slc) > 1 and isinstance(slc[-1], int):
    #         last_index = slc[-1]
    #         obj.name_latex += self._getDirectionLabel(last_index)
    #     return obj
