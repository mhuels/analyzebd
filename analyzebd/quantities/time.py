from analyzebd.quantities.quantity import *
from analyzebd.metadata.metadata import Metadata
from analyzebd.helpers.file_reader import find_setting_in_file

class Time(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "time"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "t"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "t"
        if 'name_latex_fft' not in kwargs:
            kwargs['name_latex_fft'] = "f"
        super().setup(**kwargs)
        self.setScale()
        return self

    def readMetadata(self, folder, **kwargs):
        if np.isnan(self.metadata.dt) or np.isnan(self.metadata.period):
            raise AttributeError("Metadata needs to be set manually for Time().")
        return self

    def readInterval(self, filename):
        filename = Path(filename)
        if(self.metadata.dt is None):
            raise ValueError("dt is not set!")
        section = np.loadtxt(filename, max_rows=2, usecols=0, dtype=int)
        stepInterval = (section[1] - section[0])
        interval = stepInterval * self.metadata.dt
        return interval

    # might want to need support for iInterval (involved task though)
    def readFromFile(self, folder, filename, **kwargs):
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        if self.name == "timestep":
            self.array *= self.metadata.dt  #might be better to create an extra class Timestep
            self.name = "time"
            self.name_abr = "t"
            self.setScale("tB")
        return self

    def setScale(self, unit="tB", **kwargs):
        if unit == "tB":
            kwargs.setdefault("conversion", 1)
            kwargs.setdefault("latex", "\\tau_\\mathrm{B}")
            kwargs.setdefault("latex_fft", "\\tau_\\mathrm{B}^{-1}")
        elif unit == "tw":
            kwargs.setdefault("conversion", 1 / self.metadata.period)
            kwargs.setdefault("latex", "\\tau_\\omega")
            kwargs.setdefault("latex_fft", "\\tau_\\omega^{-1}")
        elif unit == "tSub":
            kwargs.setdefault("conversion", 1 / self.metadata.tauSub)
            kwargs.setdefault("latex", "\\tau_\\mathrm{sub}")
            kwargs.setdefault("latex_fft", "\\tau_\\mathrm{sub}^{-1}")
        return super().setScale(unit, **kwargs)

    # inPlace-option not possible
    def convert2Frequency(self):
        from analyzebd.quantities.frequency import Frequency
        obj = Frequency()
        obj.metadata = self.metadata
        obj.array = 2 * np.pi / self.array
        obj.name = "frequency"
        obj.name_latex = "\\omega"
        obj.unit = deepcopy(self.unit).invert()
        return obj

