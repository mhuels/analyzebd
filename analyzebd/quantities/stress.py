from analyzebd.quantities.quantity import *
from analyzebd.metadata.shearedSlitporeBD import MetadataShearedSlitporeBD as Metadata
from analyzebd.helpers.file_reader import find_setting_in_file

class Stress(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "stress"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "sigma"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "\\sigma_{ij}"
        super().setup(**kwargs)
        self.setScale()
        return self

    def readFromFile(self, folder, filename="angularBond.out", **kwargs):
        if 'columns' not in kwargs:
            kwargs['columns'] = 6
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        # TODO: something similar to layerVelocity.py
        return self

    def setScale(self, unit="kTd-3", **kwargs):
        if unit == "kTd-3":
            kwargs.setdefault("conversion", 1)
            kwargs.setdefault("latex", "k_B T d^{-3}")
            kwargs.setdefault("latex_fft", "d^3 (k_B T)^{-1}")
        return super().setScale(unit, **kwargs)

    def readModuli(self, filename):
        if (isinstance(filename, str)):
            filename = Path(filename)
        self.storageModulus = float(find_setting_in_file(filename, "Storage modulus", 3))
        self.lossModulus = float(find_setting_in_file(filename, "Loss modulus", 3))
        return self

    """should be moved to function StressStrain/StressStrainRate"""
    # import matplotlib.pyplot as plt
    # from matplotlib import cm
    # from matplotlib.colors import Normalize
    # from mpl_toolkits.axes_grid1 import make_axes_locatable
    # from matplotlib.collections import LineCollection
    # def plotLB(self, ax=None, kind="viscous", normalize=False, withColor=True, withColorBar=False, colormap="viridis", **kwargs):
    #     if(ax is None):
    #         ax = plt.subplot()
    #     if(kind == "viscous"):
    #         x = self.shearRate
    #         ax.set_xlabel("$\\dot{\\gamma}(t)/\\tau_B^{-1}$")
    #         ax.set_title("Viscous Lissajou-Bowditch figure")
    #     elif(kind == "elastic"):
    #         x = np.array([self.A * self.T / self.timeConversion / (2 * math.pi) * math.sin(2*math.pi * t / self.T * self.timeConversion) for t in self.time])
    #         ax.set_xlabel("$\\gamma(t)$")
    #         ax.set_title("Elastic Lissajou-Bowditch figure")
    #     else:
    #         raise AttributeError(f"kind='{kind}' not supported. Choose either 'viscous' or 'elastic'.")
    #     y = self.stress
    #     ax.set_ylabel("$\\sigma_{xz}(t)/\\tilde{\\epsilon}d^{-3}$")
    #     if(normalize):
    #         x /= np.absolute(x).max()
    #         y /= np.absolute(y).max()
    #         if(kind == "viscous"):
    #             ax.set_xlabel("$\\dot{\\gamma}(t)/\\dot{\\gamma}_0$")
    #         else:
    #             ax.set_xlabel("$\\gamma(t)/\\gamma_0$")
    #         ax.set_ylabel("$\\sigma_{xz}(t)/\\sigma_{xz}^{max}$")
    #     else:
    #         x = self.shearRate
    #         y = self.stress
    #         ax.set_xlabel("$\\dot{\\gamma}/\\tau_B^{-1}$")
    #         ax.set_ylabel("$\\sigma_{xz}/\\tilde{\\epsilon}d^{-3}$")
    #     # connect start with end
    #     x = np.append(x, x[0])
    #     y = np.append(y, y[0])
    #     ax.set_title("Viscous Lissajou-Bowditch figure")
    #     if(withColor):
    #         cmap = cm.get_cmap(colormap)
    #         norm = Normalize(0, 1)
    #         if(withColorBar):
    #             divider = make_axes_locatable(ax)
    #             cax = divider.append_axes("right", size="5%", pad=0.05)
    #             cbar = plt.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax, cax=cax)
    #             cbar.ax.set_xlabel("$t/T$", labelpad=25)
    #             # cbar.set_ticks(np.arange(self.time.min(), self.time.max()*1.01, (self.time.max() - self.time.min())/5))
    #         #idea from https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/multicolored_line.html
    #         points = np.array([x, y]).T.reshape(-1, 1, 2)
    #         segments = np.concatenate([points[:-1], points[1:]], axis=1)
    #         lc = LineCollection(segments, cmap=cmap, norm=norm)
    #         lc.set_array(self.time % (self.T / self.timeConversion))
    #         linewidth = 1 if "linewidth" not in kwargs.keys() else kwargs["linewidth"]
    #         lc.set_linewidth(linewidth)
    #         ax.add_collection(lc)
    #     else:
    #         ax.plot(x, y, **kwargs)
    #     if(withColorBar):
    #         return ax, cax, cbar
    #     else:
    #         return ax

    """should be moved to function StressTime"""
    # def highlightTimestep(self, ax, timepoint, **kwargs):
    #     i = int(timepoint * self.timeConversion / self.dt_stress)
    #     ax.plot(self.shearRate[i], self.stress[i], marker='o', markersize=10, color='red',
    #             label="$t=$" f"{timepoint:.3f}" "$\\tau_B$", linestyle='None')
    #     ax.legend(loc='lower right', handlelength=1)
    #     return ax