from analyzebd.quantities.quantity import *
from analyzebd.metadata.shearedSlitporeBD import MetadataShearedSlitporeBD as Metadata
from analyzebd.helpers.file_reader import find_setting_in_file

class LayerVelocity(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "layerVelocity"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "V"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "V"
        super().setup(**kwargs)
        self.setScale()
        return self

    def readFromFile(self, folder, filename="velocities.out", **kwargs):
        if 'columns' not in kwargs:
            # TODO: infer number of layers from velocities.out
            kwargs['columns'] = [i for i in range(5, 5 + 6)]
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        size = len(self.array)
        # [l, i]: l=layer number, i=x,y,z
        self.array = np.reshape(self.array, (size, 2, 3))
        return self

    def setScale(self, unit="d/tB", **kwargs):
        if unit == "d/tB":
            kwargs.setdefault("conversion", 1)
            kwargs.setdefault("latex", "d\\tau_B^{-1}")
            kwargs.setdefault("latex_fft", "d^{-1}\\tau_B")
        elif unit == "d/tw":
            kwargs.setdefault("conversion", self.metadata.oscillationPeriod)
            kwargs.setdefault("latex", "d\\tau_\\omega^{-1}")
            kwargs.setdefault("latex_fft", "d^{-1}\\tau_\\omega")
        elif unit == "y0Lz":
            A = self.metadata.amplitude
            zMin = find_setting_in_file(self.src.parent / "configuration.in", "ITEM: BOX BOUNDS",
                                        next_line=3, posStart=0, posEnd=1, dtype=float)
            zMax = find_setting_in_file(self.src.parent / "configuration.in", "ITEM: BOX BOUNDS",
                                        next_line=3, posStart=1, posEnd=2, dtype=float)
            Lz = zMax - zMin
            kwargs.setdefault("conversion", 1 / (A * Lz))
            kwargs.setdefault("latex", "\\gamma_0 L_z")
        return super().setScale(unit, **kwargs)

    def getRelative(self, inPlace=False):
        numberOfTimepoints, numberOfLayers, dimensions = self.array.shape
        if (numberOfLayers != 2):
            print(f"numberOfLayers (={numberOfLayers}) != 2. getRelativeVelocity() is only designed for two layers.")
        # layer numbers counted from -infty to +infty
        y = self.array[:, 1] - self.array[:, 0]
        if inPlace:
            self.array = y
            # self.name_latex = "v_i"
        return self._a2v(y)