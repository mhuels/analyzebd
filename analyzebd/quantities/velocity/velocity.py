from analyzebd.quantities.quantity import *
from analyzebd.metadata.drivenSingleParticle import MetadataDrivenSingleParticle as Metadata

class Velocity(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "velocity"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "v"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "v"
        super().setup(**kwargs)
        self.setScale()
        return self

    def readFromFile(self, folder, filename="velocities.out", **kwargs):
        if 'columns' not in kwargs:
            kwargs['columns'] = 1
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def setScale(self, unit="d/tB", **kwargs):
        if unit == "d/tB":
            kwargs.setdefault("conversion", 1)
            kwargs.setdefault("latex", "d\\tau_B^{-1}")
            kwargs.setdefault("latex_fft", "d^{-1}\\tau_B")
        elif unit == "d/tw":
            kwargs.setdefault("conversion", self.metadata.drivingPeriod)
            kwargs.setdefault("latex", "d/\\tau_\\omega^{-1}")
            kwargs.setdefault("latex_fft", "d^{-1}\\tau_\\omega")
        return super().setScale(unit, **kwargs)