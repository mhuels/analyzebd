from analyzebd.quantities.quantity import *
from analyzebd.metadata.shearedSlitporeBD import MetadataShearedSlitporeBD as Metadata

class StrainRate(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "strainRate"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "dy/dt"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "\\dot{\\gamma}"
        super().setup(**kwargs)
        self.setScale()
        return self

    def readFromFile(self, folder, filename, **kwargs):
        if 'columns' not in kwargs:
            kwargs['columns'] = 1
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def setScale(self, unit="tB-1", **kwargs):
        if unit == "tB-1":
            kwargs.setdefault("conversion", 1)
            kwargs.setdefault("latex", "\\tau_\\mathrm{B}^{-1}")
            kwargs.setdefault("latex_fft", "\\tau_\\mathrm{B}")
        elif unit == "tw-1":
            kwargs.setdefault("conversion", self.metadata.oscillationPeriod)
            kwargs.setdefault("latex", "\\tau_\\omega^{-1}")
            kwargs.setdefault("latex_fft", "\\tau_\\mathrm{B}")
        elif unit == "tSub-1":
            kwargs.setdefault("conversion", self.metadata.tauSub)
            kwargs.setdefault("latex", "\\tau_\\mathrm{sub}^{-1}")
            kwargs.setdefault("latex_fft", "\\tau_\\mathrm{sub}")
        elif unit == "aCrit":
            kwargs.setdefault("conversion", 1 / self.metadata.amplitudeCrit)
            kwargs.setdefault("latex", "\\dot{\\gamma}_\\mathrm{crit}")
            kwargs.setdefault("latex_fft", "\\dot{\\gamma}_\\mathrm{crit}^{-1}")
        return super().setScale(unit, **kwargs)

    def generate(self, time=None):
        if time is None:
            N = self.metadata.numberOfTimesteps
            dt = self.metadata.dt
            time = np.arange(0, N * dt, dt)
        period = self.metadata.oscillationPeriod
        amplitude = self.metadata.amplitude
        phi = self.metadata.phaseOffset * np.pi
        self.array = amplitude * np.cos(2 * np.pi * time / period + phi)
        return self