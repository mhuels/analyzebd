from analyzebd.quantities.quantity import *
from analyzebd.metadata.drivenSingleParticle import MetadataDrivenSingleParticle as Metadata


class Force(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "force"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "F"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "F"
        super().setup(**kwargs)
        self.setScale()
        return self

    def readFromFile(self, folder, filename="forces.out", **kwargs):
        if 'columns' not in kwargs:
            kwargs['columns'] = 1   # TODO: questionable
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def setScale(self, unit="kTd-1", **kwargs):
        if unit == "kTd-1":
            kwargs.setdefault("conversion", 1)
            kwargs.setdefault("latex", "k_B T d^{-1}")
            kwargs.setdefault("latex_fft", "(k_B T)^{-1} d")
        elif unit == "Fsub0":
            kwargs.setdefault("conversion", 1 / self.metadata.substrateAmplitude)
            kwargs.setdefault("latex", "F_{\\mathrm{sub},0}")
            kwargs.setdefault("latex_fft", "F_{\\mathrm{sub},0}^{-1}")
        return super().setScale(unit, **kwargs)
