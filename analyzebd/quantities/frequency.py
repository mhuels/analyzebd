from analyzebd.quantities.quantity import *
from analyzebd.metadata.metadata import Metadata


class Frequency(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "frequency"
        if 'metadata' not in kwargs:
            kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "w"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "\\omega"
        super().setup(**kwargs)
        self.setScale()
        return self

    def setScale(self, unit="tB-1", **kwargs):
        if unit == "tB-1":
            kwargs.setdefault("conversion", 1)
            kwargs.setdefault("latex", "\\tau_B^{-1}")
            kwargs.setdefault("latex_fft", "\\tau_B")
        return super().setScale(unit, **kwargs)

    def convert2Period(self):
        from analyzebd.quantities.time import Time
        obj = Time()
        obj.metadata = self.metadata  # deepcopy?
        obj.array = 2 * np.pi / self.array
        obj.name = "period"
        obj.name_latex = "\\tau_\\omega"
        obj.unit = deepcopy(self.unit).invert()
        return obj
