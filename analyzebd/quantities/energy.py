from analyzebd.quantities.quantity import *
# from analyzebd.metadata.drivenSingleParticle import MetadataDrivenSingleParticle as Metadata

class Energy(Quantity):
    def setup(self, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "energy"
        # if 'metadata' not in kwargs:
        #     kwargs['metadata'] = Metadata()
        if 'name_abr' not in kwargs:
            kwargs['name_abr'] = "E"
        if 'name_latex' not in kwargs:
            kwargs['name_latex'] = "E"
        super().setup(**kwargs)
        self.setScale()
        return self

    def readFromFile(self, folder, filename="energies.out", **kwargs):
        if 'columns' not in kwargs:
            kwargs['columns'] = 1
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def setScale(self, unit="kT", **kwargs):
        if unit == "kT":
            kwargs.setdefault("conversion", 1)
            kwargs.setdefault("latex", "k_B T")
            kwargs.setdefault("latex_fft", "(k_B T)^{-1}")
        return super().setScale(unit, **kwargs)
