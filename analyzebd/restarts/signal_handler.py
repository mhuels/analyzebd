import signal
import analyzebd.restarts.globals as globals

def signal_handler(sig, frame):
    globals.ret_code = sig
    return sig

def apply_signal_handlers(signals=None):
    """
    Apply signal handlers for given signals
    Args:
        signals (list): list of signals (e.g. signal.SIGTERM)
    Returns:
        ret_code (int)
    """
    if signals is None:
        signals = [signal.SIGINT, signal.SIGTERM, signal.SIGUSR1, signal.SIGUSR2]
    for sig in signals:
        signal.signal(sig, signal_handler)
    return None
