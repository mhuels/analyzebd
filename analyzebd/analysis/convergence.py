import numpy as np
from analyzebd.functions.function import Function

def running_convergence(f: Function, x_min: float, eps: float=5e-2, min_conv_length: float=1.5,
                        range_before: float=1., range_after: float=None, max_steps: int=1000):
    """
    Search for x that fulfills delta = |f(x) - f(x')|/|f(x)| < eps for all x' in [x, x_max].
    :param f: Function
           function f(x)
    :param x_min: float
           only x with x > x_min are considered
    :param eps: float
           convergence threshold
    :param min_conv_length: float
           minimal convergence length: x * min_conv_length < x_max must be fulfilled to award convergence
    :param range_before: float (default=1.)
           checks x' in [range_before * x, x_max]
    :param range_after: float or None
           checks x' only in [x, range_after * x]
    :param max_steps: int or None
           leave out steps if function contains more than max_steps data points by increasing step size
    :return: x (float or np.inf), smallest_delta (float)
           np.inf if not converged, smallest delta that was found
    """
    x = f.x
    y = f.y
    N = len(x)
    x_converged = np.inf
    min_i = x(v=x_min)
    smallest_delta = np.inf
    if max_steps is not None and N > max_steps:
        stepsize = N // max_steps
    else:
        stepsize = 1
    for i in range(min_i, N, stepsize):
        j_min = int(i * range_before)   # only works for equally spaced x
        if range_after is not None:
            j_max = int(i * range_after)    # check only a certain range
        else:
            j_max = N   # check to the end
        if int(i * min_conv_length) > N:
            break   # dataset is not large enough; minimal length needs to be checked before awarding convergence
        numerator = np.abs(y[i] - y[j_min:j_max])
        denominator = np.abs(y[i])
        if denominator == 0:
            continue    # can happen sometimes in the beginning
        delta_array = numerator / denominator
        if np.max(delta_array) < smallest_delta:
            smallest_delta = np.max(delta_array)
        if np.min(delta_array < eps):
            x_converged = x[i]
            break
    return x_converged, smallest_delta