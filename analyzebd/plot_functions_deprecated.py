import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import Normalize
from mpl_toolkits.axes_grid1 import make_axes_locatable

def plotPairCorrelation(ax, filename, **kwargs):
    data = np.loadtxt(filename, usecols=(0, 1))
    r = data[:, 0]
    g = data[:, 1]
    ax.plot(r, g, **kwargs)
    ax.set_xlim([0, None])
    ax.set_ylim([0, None])
    ax.set_xlabel("$r/d$")
    ax.set_ylabel("$g(r)/d^{-1}$")
    ax.set_title("Intra-layer pair correlation")
    return ax

def plotConfiguration(ax, filename):
    configuration = plt.imread(filename, format="png")
    im = ax.imshow(configuration)
    pixelNumberX, pixelNumberY, _ = configuration.shape
    ###dirty part###
    L = 23.8
    shown_L = 16.0
    xStart = - shown_L / 2
    xEnd = shown_L / 2
    xtick_labels = np.arange(xStart, xEnd*1.01, round((xEnd - xStart) / 10))
    ytick_labels = xtick_labels
    xtick_labels = [f"{i:.0f}" for i in xtick_labels]
    ytick_labels = [f"{-i:.0f}" for i in ytick_labels]
    xticks = np.arange(0, pixelNumberX+1, pixelNumberX / (len(xtick_labels) - 1))
    yticks = np.arange(0, pixelNumberY+1, pixelNumberY / (len(ytick_labels) - 1))
    ax.set_xticks(xticks)
    ax.set_yticks(yticks)
    ax.axes.xaxis.set_ticklabels(xtick_labels)
    ax.axes.yaxis.set_ticklabels(ytick_labels)
    ################
    ax.set_xlabel("$x/d$")
    ax.set_ylabel("$y/d$")
    ax.set_title("Configuration")
    cmap = cm.get_cmap('rainbow')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(cm.ScalarMappable(norm=Normalize(0.5, -0.5), cmap=cmap), ax=ax, cax=cax)
    cbar.ax.set_xlabel("$z/d$", labelpad=25)
    return ax, cax
