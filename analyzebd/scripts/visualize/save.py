dirs = {0: 'x', 1: 'y', 2: 'z'}

def add_outname_specifics(outname, args):
    outname = outname.parent / (outname.name + f"_{dirs[args.direction]}")
    if args.withDistribution:
        outname = outname.parent / (outname.name + "_withDistribution")
        try:
            args.formats.remove('.pdf')  # remove vector graphic formats
        except ValueError:
            pass
    return outname
