import numpy as np
from copy import deepcopy

class ProcessingTimes:
    def __init__(self):
        self.reset()

    def reset(self):
        self.data = {}

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        self.data[key] = np.array(value, ndmin=1)    # dict-entries should only be of type np.ndarray and should be iterable
        return self

    def __str__(self):
        return self.get_string()

    def append(self, other):
        for key in other.data.keys():
            if key in self.data:
                self[key] = np.append(self[key], other[key])
            else:
                self[key] = other[key]
        return self

    def get_string(self, sep=", ", with_total=True, tabulate=True):
        s = ""
        if tabulate:
            key_length = 1
            for key in self.data.keys():
                if len(key) > key_length:
                    key_length = len(key)
        else:
            key_length = ""
        for key in self.data.keys():
            s += f"{key:{key_length}s} [s]: "
            cur_sep = sep
            sum = 0
            for i, v in enumerate(self.data[key]):
                v_sec = v.total_seconds()
                sum += v_sec
                if i == len(self[key]) - 1: cur_sep = ''
                s += f"{v_sec:.2f}{cur_sep}"
            if with_total:
                s += f" [{sum:.2f}]"
            s += '\n'
        return s

    # def total(self, inPlace=False):
    #     if inPlace:
    #         obj = self
    #     else:
    #         obj = deepcopy(self)
    #     for key in obj.data.keys():
    #         sum = 0
    #         for value in obj[key]:
    #             sum += value.total_seconds()
    #         obj[key] = sum
    #     return obj
