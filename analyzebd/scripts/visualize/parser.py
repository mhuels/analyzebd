from argparse import ArgumentParser
from analyzebd.helpers.modes import print_supported_modes
from analyzebd.scripts.visualize.plots import supported_modes
from analyzebd.parser.classification import addargs_struc_classification
from analyzebd.parser.settling_time import addargs_settling_time
from analyzebd.plt import plt_global

def add_main_group(parser):
    parser_main = parser.add_argument_group("folder-related options")
    parser_main.add_argument("folders", type=str, nargs='*', help="folders to be plotted")
    parser_main.add_argument("--amplitude", "-a", type=float, nargs="+", help="amplitudes to be added")
    parser_main.add_argument("--period", "-p", type=float, nargs="+", help="periods to be added")
    parser_main.add_argument("--a_fmt", type=str, default=".6e", help="format specifier for amplitudes")
    parser_main.add_argument("--p_fmt", type=str, default=".6e", help="format specifier for periods")
    parser_main.add_argument("--workdir", type=str, help="location of -a/-p folders, defaults to current location")
    return parser_main

def add_modes_group(parser):
    parser_modes = parser.add_argument_group("mode-related options")
    parser_modes.add_argument("--modes", "-m", type=str, nargs='+', default=["all"],
                              help=f"plots to be considered, default='all'\n{print_supported_modes(supported_modes)}")
    parser_modes.add_argument("--ps", type=str, help="plot parameter space plot for this mode instead")
    parser_modes.add_argument("--withDistribution", action='store_true', help="also plot distribution as grayscale")
    parser_modes.add_argument("--bins", type=int, default=100, help="number of bins if --withDistribution")
    parser_modes.add_argument("--maxAll", type=int, default=10, help="maximal number of lines drawn with 'All'-modes ")
    # parser_modes.add_argument("--shuffleAll", action='store_true', help="randomize which trajectories are shown in 'All'-modes")
    parser_modes.add_argument("--single_ensembles", type=int, nargs='+', help="plot these ensemble numbers as well")
    parser_modes.add_argument("--symmetric_errors", action='store_true',
                              help="don't distinguish between upper and lower error")
    return parser_modes

def add_data_group(parser):
    parser_data = parser.add_argument_group("data-related options")
    # TODO: move the first three to analyzebd.parser.read_data
    parser_data.add_argument("--numberOfEnsembles", "-e", type=int, help="number of ensembles to read")
    parser_data.add_argument("--xStart", type=float, help="start of x-axis")
    parser_data.add_argument("--xEnd", type=float, help="end of x-axis")
    parser_data.add_argument("--xUnit", type=str, default="tw", help="unit of x-axis (time)")
    parser_data.add_argument("--direction", type=int, default=0, help="direction: 0=x, 1=y, 2=z")
    return parser_data

def add_plot_group(parser):
    parser_plt = parser.add_argument_group("plot-related options")
    # TODO: move this to analyzebd.parser
    parser_plt.add_argument("--notitles", action='store_true', help="remove titles and suptitles")
    parser_plt.add_argument("--titles", type=str, nargs='+', help="titles for subplots")
    parser_plt.add_argument("--hide_ts", action='store_true', help="hide settling time lines and annotates")
    parser_plt.add_argument("--hide_colorbars", action='store_true', help="hide colorbars")
    parser_plt.add_argument("--noshow", action='store_true', help="don't show interactive panels")
    parser_plt.add_argument("--fullscreen", action='store_true', help="show interactive panels on fullscreen")
    parser_plt.add_argument("--save", type=str, nargs='?', const='auto',
                            help="save plots")  # default=None, --save -> 'auto'; --save outname -> 'outname'
    parser_plt.add_argument("--formats", type=str, nargs='+', default=[".pdf", ".png"],
                            help="save plot file formats (e.g. '.pdf')")
    parser_plt.add_argument("--dpi", type=float, default=600, help="resolution of saved figure")
    parser_plt.add_argument("--spw", type=float, help=f"subplot width, default={plt_global['subplot_width']:.1f}")
    parser_plt.add_argument("--spr", type=float, help=f"subplot ratio, default={plt_global['ratio']:.2f}")
    parser_plt.add_argument("--fontsize", type=float, help=f"fontsize of labels, annotates, etc.")
    parser_plt.add_argument("--lw", type=float, help=f"linewidth (and marker size) of plots")
    parser_plt.add_argument("--locl", type=str, help=f"location of legend")
    parser_plt.add_argument("--markers", type=str, help="json-file with markers, their location and corresponding mode")
    parser_plt.add_argument("--ymin", type=float, help="lower limit of y-axis (in all plots)")
    parser_plt.add_argument("--ymax", type=float, help="upper limit of y-axis (in all plots)")
    return parser_plt

def add_analysis_group(parser):
    parser_analysis = parser.add_argument_group("analysis-related options")
    addargs_settling_time(parser_analysis, inPlace=True)
    addargs_struc_classification(parser_analysis, inPlace=True)
    return parser_analysis

def get_parser():
    parser = ArgumentParser(description="Plot amplitude development/convergence")
    parser_main = add_main_group(parser)
    parser_modes = add_modes_group(parser)
    parser_data = add_data_group(parser)
    parser_plt = add_plot_group(parser)
    parser_analysis = add_analysis_group(parser)
    return parser
