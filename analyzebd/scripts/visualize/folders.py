def parse_folders(args):
    folders = args.folders
    if (args.period is None) != (args.amplitude is None):
        exit("Please use --amplitude/-a only together with --period/-p!")
    elif (args.period is None) and (args.amplitude is None):
        pass
    else:
        for a in args.amplitude:
            for p in args.period:
                folder = f"amplitude_{a:{args.a_fmt}}_period_{p:{args.p_fmt}}"
                folders.append(folder)
    # TODO: remove duplicates
    if len(folders) == 0:
        exit("Nothing to plot!")
    return folders
