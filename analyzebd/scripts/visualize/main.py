import numpy as np
from pathlib import Path
from copy import deepcopy
from math import ceil
from analyzebd.helpers.file_writer import savefig
from analyzebd.helpers.clock import Clock, stop_time, pause_clock
from analyzebd.plt import plt, plt_global
from analyzebd.helpers.modes import parse_modes
from analyzebd.quantities import Time, StrainRate
from analyzebd.functions import LayerPositionTime, AngularBondTime
from analyzebd.parameter_space.period_amplitude import PeriodAmplitudeParameterSpace
from analyzebd.helpers.file_reader import load_json
from analyzebd.plt.markers import add_markers

from analyzebd.scripts.visualize.parser import get_parser
from analyzebd.scripts.visualize.plots import supported_modes
from analyzebd.scripts.visualize.folders import parse_folders
from analyzebd.scripts.visualize.read import read_data
from analyzebd.scripts.visualize.analysis import analyze_settling_time_depinning, analyze_settling_time_structure
from analyzebd.scripts.visualize.analysis import analyze_amplitudes
from analyzebd.scripts.visualize.analysis import get_period_average
from analyzebd.scripts.visualize.analysis import analyze_transition_state, format_state_ratios
from analyzebd.scripts.visualize.plots import plot_mode, plot_ts_depinning, plot_ts_structure, plot_state_classification
from analyzebd.scripts.visualize.plots import modes_with_time, modes_with_LB
from analyzebd.scripts.visualize.processing_times import ProcessingTimes
from analyzebd.scripts.visualize.save import add_outname_specifics
plt.rcParams.update({'font.size': 8})
from tqdm import tqdm

def plot_parameter_space(mode, args, clock=None, verbose=True):
    periods = Time(name="period")
    amplitudes = StrainRate(name="amplitude", name_latex="\\dot{\\gamma}_0")
    if args.period is None or args.amplitude is None:
        print(f"Please provide periods and amplitudes via -p/-a.")
        exit(1)
    periods.setArray(np.array(args.period))
    periods.array = np.flip(np.sort(periods.array))     # sort reversed
    amplitudes.setArray(np.array(args.amplitude))
    amplitudes.array = np.sort(amplitudes.array)    # sort
    ps = PeriodAmplitudeParameterSpace(periods=periods, amplitudes=amplitudes, property=None)
    ps.setupPlot(Xsize=plt_global['subplot_width'], Ysize=plt_global['subplot_height'],
                 kw_label_outer={'xtick_fmt': '.6e', 'ytick_fmt': '.6e', 'ytick_pad': 50})
    N = len(ps.X) * len(ps.Y)
    if verbose:
        print(f"mode: {mode}")
        print(f"Reading and plotting data ...")
    processing_times = ProcessingTimes()
    with tqdm(range(N)) as counter:
        for i, p in enumerate(ps.X):
            for j, a in enumerate(ps.Y):
                folder = Path(f"amplitude_{a:{args.a_fmt}}_period_{p:{args.p_fmt}}")  # TODO: this has already been done in parse_folders
                # use workdir if relative folder paths
                if args.workdir is not None and not folder.is_absolute():
                    folder = Path(args.workdir) / folder
                try:
                    data, metadata = read_data(folder, args, clock=clock)
                    # analysis
                    data['ts_depinning'], data['delta'] = analyze_settling_time_depinning(a_avg=data['a_avg'],
                                                                                          args=args, verbose=False,
                                                                                          processing_times=processing_times)
                except FileNotFoundError:
                    data = None

                ax = ps.axes[i][j]
                if data is not None:
                    plot_mode(mode, ax=ax, args=args, data=data, title=None)
                    if 'LB' not in mode:
                        if mode == "ampAvg": label_ax = ax
                        else: label_ax = None
                        plot_ts_depinning(axes=[ax], t_settling=data['ts_depinning'], label_ax=label_ax,
                                          unit=data['a_avg'].x.unit.getLatex())
                        analyze_amplitudes(f_avg=data['f_avg'], a_avg=data['a_avg'], t_settling=data['ts_depinning'],
                                           delta=data['delta'], args=args, ax=label_ax, verbose=False)
                else:
                    ps.plot_none(ax=ax)
                if data is not None:
                    processing_times.append(metadata["processing times"])
                counter.update()
    print(processing_times)
    plt.suptitle("")
    plt.subplots_adjust(hspace=0.4, wspace=0.4, top=0.8)

    # save figure
    if args.save is not None:
        if args.save == 'auto':
            outname = Path(mode)
            outname = add_outname_specifics(outname, args)
        else:
            outname = Path(args.save)
        savefig(outname, formats=args.formats, verbose=verbose, dpi=args.dpi)
    print(f"Total runtime: {clock()}")
    if not args.noshow:
        if args.fullscreen:
            plt.get_current_fig_manager().full_screen_toggle()  # toggle fullscreen mode
        with pause_clock(clock):
            plt.show()
    plt.close()
    return 0

def plot_single(folders: list, modes, args, clock=None, verbose=True):
    for folder in folders:
        start = clock.lap()
        folder = Path(folder)
        # use workdir if relative folder paths
        if args.workdir is not None and not folder.is_absolute():
            folder = Path(args.workdir) / folder
        if verbose:
            print(f"folder: {folder.name}")

        # skip if not existing
        if not folder.exists():
            print(f"{folder} doesn't exist. Skipping...")
            continue

        # read data
        data, metadata = read_data(folder, args, clock=clock, verbose=verbose)
        if verbose:
            print(f"numberOfEnsembles: {metadata['numberOfEnsembles']}")
            print(f"numberOfPeriods: {metadata['numberOfPeriods']:.1f}")

        # analysis
        data['ts_depinning'], data['delta'] = analyze_settling_time_depinning(a_avg=data['a_avg'], args=args, verbose=verbose)   # TODO: need a t_settling_structure and t_settling_depinning
        data['ts_structure'] = analyze_settling_time_structure(psi_avg=data['psi_avg'], args=args, verbose=verbose)   # TODO: need a t_settling_structure and t_settling_depinning
        data['transition_state'], data['state_ratios'] = analyze_transition_state(states=data['states'], struc_classifier=metadata['struc_classifier'],
                                                            t_settling=data["ts_structure"],
                                                            args=args, verbose=verbose)
        data['transition_state_dict'] = {'angBondLBAllSettled': data['transition_state']}
        # remove this later START
        data['state_ratios_dict'] = {'angBondLBAllSettled': data['state_ratios']}
        data['transition_state_dict']['angBondLBAvgSettled'], data['state_ratios_dict']['angBondLBAvgSettled'] = \
            analyze_transition_state(states=data['states_avg'], struc_classifier=metadata['struc_classifier'],
                                     t_settling=data["ts_structure"],
                                     args=args, verbose=verbose)
        data['psi_avg_avg'] = get_period_average(data['psi_avg'], t_settling=data['ts_structure'])
        data['states_avg_avg'] = metadata['struc_classifier'].classify(data['psi_avg_avg'])
        data['transition_state_dict']['angBondLBAvgSettledAvg'], data['state_ratios_dict']['angBondLBAvgSettledAvg'] = \
            analyze_transition_state(states=data['states_avg_avg'], struc_classifier=metadata['struc_classifier'],
                                     t_settling=data["ts_structure"],
                                     args=args, verbose=verbose)
        # remote this later END

        # setup subplots according to number of needed graphs
        num = len(modes)
        nrows = int(np.sqrt(num))  # rather less rows than columns
        ncols = int(ceil(num / nrows))
        if args.spw is None:
            subplot_width = plt_global['subplot_width']
        else:
            subplot_width = args.spw
        if args.spr is None:
            ratio = plt_global['ratio']
        else:
            ratio = args.spr
        subplot_height = subplot_width / ratio
        figsize = (subplot_width * ncols, subplot_height * nrows)
        fig, axes = plt.subplots(nrows, ncols,
                                 figsize=figsize)
        if verbose: print(f"len(fig.axes)={len(fig.axes)}")
        plt.subplots_adjust(hspace=0.4, wspace=0.3, top=0.8)  # TODO: adjust top and wspace according to nrows and ncols

        # plot
        ax_mode_map = {}
        for i, mode in enumerate(modes):
            with stop_time(f"Plotting {mode} ({i + 1}/{len(modes)})", clock, verbose=verbose):
                ax_mode_map[mode] = plot_mode(mode, ax=fig.axes[i], args=args, data=data)
        # hide remaining axes
        for i in range(len(modes), nrows * ncols):
            fig.axes[i].axis('off')

        # analysis
        if verbose:
            print(f"settling time (depinning): {data['ts_depinning']:.2f} [{data['a_avg'].x.unit}]")
            print(f"smallest delta: {data['delta']:.2e}")
            print(f"settling time (structure): {data['ts_structure']:.2f} [{data['psi_avg'].x.unit}]")
            print(f"psi4_thr: {args.thr4}")
            print(f"psi6_thr: {args.thr6}")
            print(f"state ratios: {format_state_ratios(data['state_ratios'], translate=metadata['struc_classifier'].translate)}")
            print(f"transition_state: {data['transition_state']}")
            # remove this later START
            for mode in ["angBondLBAllSettled", "angBondLBAvgSettled", "angBondLBAvgSettledAvg"]:
                print(f"state ratios['{mode}']: {format_state_ratios(data['state_ratios_dict'][mode], translate=metadata['struc_classifier'].translate)}")
                print(f"transition_state['{mode}']: {data['transition_state_dict'][mode]}")
            # remove this later END

        modes_time = modes_with_time(modes)
        if not args.hide_ts and len(modes_time) > 0:
            axes = [ax_mode_map[mode] for mode in modes_time]  # fig.axes would include color axes as well
            if "ampAvg" in modes_time: label_ax = ax_mode_map["ampAvg"]
            else: label_ax = None
            plot_ts_depinning(axes, data['ts_depinning'], label_ax=label_ax,
                              unit=data['a_avg'].x.unit.getLatex())
            analyze_amplitudes(f_avg=data['f_avg'], a_avg=data['a_avg'], t_settling=data['ts_depinning'],
                               delta=data['delta'], args=args,
                               ax=label_ax,
                               verbose=verbose)  # TODO: maybe move this above plots, needs to split prints and plot-annotates into different function
        if not args.hide_ts:
            if "angBondAvg" in modes:
                axes = [ax_mode_map[mode] for mode in modes_with_time(modes)]
                plot_ts_structure(axes, data['ts_structure'], label_ax=ax_mode_map['angBondAvg'], unit=data['psi_avg'].x.unit.getLatex())
        modes_with_LBSettled = modes_with_LB(modes, settled=True)
        for mode in modes_with_LBSettled:
            plot_state_classification([ax_mode_map[mode]], args=args, state_ratios=data['state_ratios_dict'][mode],
                                      translate=metadata["struc_classifier"].translate, kw_translate={'long': False},
                                      bgcolor="auto", show_missing=False)

        # axis limits
        for mode in modes:
            ax = ax_mode_map[mode]
            ax.set_ylim([args.ymin, args.ymax])

        # markers
        # TODO json-file with markers and corresponding 'loc', 'label' and 'mode'
        if args.markers is not None:
            try:
                markers = load_json(args.markers, verbose=True)
                add_markers(ax_mode_map=ax_mode_map, markers=markers, zorder=4)
            except FileNotFoundError:
                print(f"Markers from {args.markers} could not be loaded.")

        # title(s)
        if args.notitles:
            plt.suptitle("")
            for mode in modes:
                ax_mode_map[mode].set_title("")
        if args.titles is not None:
            if len(args.titles) != len(modes):
                from warnings import warn
                warn(f"Not every mode was given a title. Skipping title assignment.")
            else:
                for i, mode in enumerate(modes):
                    ax_mode_map[mode].set_title(args.titles[i])

        # save figure
        if args.save is not None:
            if args.save == 'auto':
                outname = Path(folder.name)
                outname = add_outname_specifics(outname, args)
            else:
                outname = Path(args.save)
                if len(folders) > 1:
                    outname = outname.parent / (outname.name + "_" + folder.name)
            savefig(outname, formats=args.formats, verbose=verbose, dpi=args.dpi)
        stop = clock.lap()
        if verbose: print(f"Current folder runtime: {clock(start, stop)}")
        if not args.noshow:
            if args.fullscreen:
                plt.get_current_fig_manager().full_screen_toggle()  # toggle fullscreen mode
            with pause_clock(clock):
                plt.show()
        plt.close()
        print("\n")
    print(f"Total runtime: {clock()}")
    return 0

def main():
    clock = Clock()
    parser = get_parser()
    args = parser.parse_args()
    if args.ps is None:
        modes = parse_modes(args.modes, supported_modes)
    else:
        modes = parse_modes([args.ps], supported_modes)

    # parse folders
    folders = parse_folders(args)

    # global settings
    if args.fontsize is not None:
        plt.rcParams.update({'font.size': args.fontsize})

    # parameter space if called for
    clock.lap()
    if args.ps is not None:
        ret = plot_parameter_space(modes[0], args, clock=clock)
    else:
        ret = plot_single(folders, modes, args, clock=clock)
    return ret


if __name__ == "__main__":
    main()
