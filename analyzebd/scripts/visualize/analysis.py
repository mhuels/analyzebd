import numpy as np
from analyzebd.helpers.clock import stop_time
from analyzebd.analysis.convergence import running_convergence
from analyzebd import Function
from analyzebd.classification import Structure_Classifier, Transition_Classifier
from analyzebd.functions import AngularBondTime

def analyze_settling_time_depinning(a_avg, args, verbose=False, processing_times=None, clock=None):
    with stop_time("calculating settling time", clock=clock, verbose=verbose):
        t_settling, delta = running_convergence(a_avg, x_min=args.t_min, eps=args.eps, min_conv_length=1.5)    # earliest after one period
    if processing_times is not None:
        processing_times["calculate depinning settling time"] = clock(-2, -1)
    return t_settling, delta

def analyze_settling_time_structure(psi_avg, args, verbose=False, processing_times=None, clock=None):
    with stop_time("calculating structure settling time", clock=clock, verbose=verbose):
        # t_settling, delta = running_convergence(a_avg, x_min=args.t_min, eps=args.eps, min_conv_length=1.5)    # earliest after one period
        xmin = psi_avg.x.min()
        xmax = psi_avg.x.max()
        rel = 2./3.
        t_settling = xmin + (xmax - xmin) * rel
    if processing_times is not None:
        processing_times["calculate structure settling time"] = clock(-2, -1)
    return t_settling

def analyze_amplitudes(f_avg, a_avg, t_settling: float, delta: float, args, ax=None, verbose=False):
    # calculate amplitude at settling time
    if not np.isinf(t_settling):
        xmax = a_avg(t_settling)
    else:
        xmax = np.nan  # last available value
    if verbose: print(f"xmax(ts_d): {xmax:.2f} [{a_avg.y.unit}]")

    # calculate amplitude at settling time when taking the ensemble average first
    if not np.isinf(t_settling):
        xmax2 = f_avg.getWindowedAmplitude(t_settling, mode=args.amplitude_mode)
    else:
        xmax2 = np.nan
    if verbose: print(f"xmax(ts_d) (ens-avg first): {xmax2:.2f} [{f_avg.y.unit}]")

    # calculate amplitude at maximum available value
    tmax_available = a_avg.x.max() * 2 / 3
    xmax_available = a_avg(tmax_available)
    if verbose: print(f"xmax(0.66*t_max): {xmax_available:.2f} [{a_avg.y.unit}]")

    # calculate amplitude at maximum available value when taking the ensemble average first
    xmax2_available = f_avg.getWindowedAmplitude(tmax_available, mode=args.amplitude_mode)
    if verbose: print(f"xmax(0.66*t_max) (ens-avg first): {xmax2_available:.2f} [{f_avg.y.unit}]")

    # calculate amplitude at maximum possible value
    xmax_end = a_avg.y[-1]
    if verbose: print(f"xmax(t_max): {xmax_end:.2f} [{a_avg.y.unit}]")

    # calculate amplitude at maximum possible value when taking the ensemble average first
    xmax2_end = f_avg.getWindowedAmplitude(f_avg.x.max() - 1., mode=args.amplitude_mode)
    if verbose: print(f"xmax(t_max) (ens-avg first): {xmax2_end:.2f} [{f_avg.y.unit}]")

    # calculate time average of amplitude after settling time (or 2/3 tmax)
    if np.isinf(t_settling):
        t_s = tmax_available
    else:
        t_s = t_settling
    xmax_avg = a_avg.crop(xStart=t_s).y.getTotalAverage()
    if verbose: print(f"avg[xmax([ts_d, t_max])]: {xmax_avg:.2f} [{a_avg.y.unit}]")

    if ax is not None:
        # plot horizontal line with xmax label
        if not np.isnan(xmax):
            xmax_center = xmax
            # t_label = "t_s"
        else:
            xmax_center = xmax_available
            # t_label = "\\frac{2}{3} t_{\max}"
        xmax_line = xmax_avg
        ax.axhline(xmax_line, color='black', ls='--')

        # plot enclosing lines +/- delta
        abs_dist = args.eps * xmax_line
        if np.isinf(t_settling):
            t_start = a_avg.x.max() * 2 / 3
        else:
            t_start = t_settling
        t_end = a_avg.x.max()
        hline_kw = {'color': 'black', 'ls': '--', 'lw': 0.5}
        upper_line = xmax_center + abs_dist
        lower_line = xmax_center - abs_dist
        ax.plot((t_start, t_end), (upper_line,) * 2, **hline_kw)
        ax.plot((t_start, t_end), (lower_line,) * 2, **hline_kw)

        # xmax label
        # xmax_label = f"${a_avg.y.name_latex.getLabel(with_dollars=False)}\\left({t_label}\\right) = {xmax_line:.2f} {a_avg.y.unit.getLatex()}$"
        xmax_label = f"${a_avg.y.name_latex.getLabel(with_dollars=False)} = {xmax_line:.2f} {a_avg.y.unit.getLatex()}$"
        # ax.annotate(xmax_label, (a_avg.x.max() * 0.98, (xmax_line - abs_dist) * 0.95), ha='right', va='top')
        ax.annotate(xmax_label, (a_avg.x.min(), upper_line), ha='left', va='bottom')
    return xmax, xmax2, xmax_available, xmax2_available, xmax_end, xmax2_end, xmax_avg

def analyze_transition_state(states: Function, struc_classifier: Structure_Classifier, t_settling: float, args, verbose=False,
                             processing_times=None, clock=None):
    states_settled = states.crop(xStart=t_settling)
    # state_ratios = states_settled.y.count_unique(translate=struc_classifier.translate, relative=True, as_dict=True)
    state_ratios = states_settled.y.count_unique(relative=True, as_dict=True)
    with stop_time("classifying transition state", clock=clock, verbose=verbose):
        transition_classifier = Transition_Classifier(struc_classifier)
        transition_state_num = transition_classifier.classify(states_settled, Pmin=args.Pmin)
        transition_state = transition_classifier.translate(transition_state_num, long=True)
    if processing_times is not None:
        processing_times["classify transition state"] = clock(-2, -1)
    return transition_state, state_ratios

def format_state_ratios(state_ratios: dict, fmt=".2f", translate=None):
    if translate is None:
        translate = lambda x: x
    s = "{"
    L = len(state_ratios.keys())
    for i, key in enumerate(state_ratios.keys()):
        s += f"'{translate(key)}': {state_ratios[key]:{fmt}}"
        if i < L - 1:
            s += ", "
    s += "}"
    return s

def get_period_average(psi_avg: AngularBondTime, t_settling=None, period=0.5):
    if t_settling is not None:
        cropped = psi_avg.crop(xStart=t_settling)
    else:
        cropped = psi_avg
    psi_avg_avg = cropped.build_periodic_avg(period=period)
    return psi_avg_avg
