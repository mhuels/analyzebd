import numpy as np
from copy import deepcopy
from matplotlib.colors import Normalize

supported_modes = ["statesAvg",
                   "combinedAvg",
                   "layerPosAvg",
                   "layerPosAll",
                   "ampAvg",
                   "ampAll",
                   # "angBondAvg",
                   "angBondAll",
                   "angBondLBAvgSettledAvg",
                   "angBondLBAvgSettled",
                   "angBondLBAllSettled",
                   "angBondLBAvg",
                   "angBondLBAll",
                  ]

angBondColors = {0: 'blue', 1: 'orange'}
state_colors = {"s": "blue", "d": "lime", "h": "red", "f": "gray"}

def plot_mode(mode, ax, data: dict, args, **kwargs):
    if mode == "statesAvg":
        ret = plot_statesAvg(ax=ax, f_avg=data["f_avg"], states_avg=data["states_avg"], args=args, **kwargs)
    elif mode == "combinedAvg":
        ret = plot_combinedAvg(ax=ax, f=data['layerPosition'], psi=data['angularBond'], args=args, **kwargs)
    elif mode == "layerPosAvg":
        ret = plot_layerPosAvg(ax=ax, f=data['layerPosition'], args=args, **kwargs)
    elif mode == "ampAvg":
        ret = plot_ampAvg(ax=ax, a=data['amplitude'], args=args, **kwargs)
    elif mode == "angBondAvg":
        ret = plot_angBondAvg(ax=ax, psi=data['angularBond'], args=args, **kwargs)
    elif mode == "angBondAll":
        ret = plot_angBondAll(ax=ax, psi=data['angularBond'], args=args, scatter=True, with_avg=True, **kwargs)
    elif mode == "layerPosAll":
        ret = plot_layerPosAll(ax=ax, f=data['layerPosition'], args=args, **kwargs)
    elif mode == "ampAll":
        ret = plot_ampAll(ax=ax, a=data['amplitude'], args=args, **kwargs)
    elif mode == "angBondLBAvg":
        ret = plot_angBondLBAvg(ax=ax, psi=data['angularBond'], args=args, **kwargs)
    elif mode == "angBondLBAvgSettled":
        ret = plot_angBondLBAvg(ax=ax, psi=data['angularBond'], args=args, t_settling=data['ts_structure'], **kwargs)
    elif mode == "angBondLBAvgSettledAvg":
        ret = plot_angBondLBAvg(ax=ax, psi=data['angularBond'], args=args, t_settling=data['ts_structure'], avg_periods=True, **kwargs)
    elif mode == "angBondLBAll":
        ret = plot_angBondLBAll(ax=ax, psi=data['angularBond'], args=args, **kwargs)
    elif mode == "angBondLBAllSettled":
        ret = plot_angBondLBAll(ax=ax, psi=data['angularBond'], args=args, t_settling=data['ts_structure'], time_as_color=True, **kwargs)
    else:
        ret = None
    return ret

def plot_combinedAvg(ax, f, psi, args, title="combined (ens-avg)"):
    f_avg = f.buildEnsembleAverage()
    psi_avg = psi.buildEnsembleAverage()
    psi_avg.y.slice((..., 1), inPlace=True)
    f_avg.plot(ax=ax, colors=psi_avg.y, colormap="Spectral_r", withColorBar=not args.hide_colorbars,
               cnorm=Normalize(0, 1),
            #kw_colorbar={'labelPos': 'x', 'labelpad': 7}
            )
    ax.set_ylim(f.y.get_lim())
    ax.set_title(title)
    return ax

def plot_statesAvg(ax, f_avg, states_avg, args, title="states (ens-avg)"):
    f_avg.plot(ax=ax, color="black")
    # TODO: use state_colors better
    special_colors = {0: state_colors["s"],  # s
                      1: state_colors["d"],  # d
                      2: state_colors["h"],  # h
                      3: state_colors["f"],  # f
                      }
    # find indices, where states swap
    state_swaps = (states_avg.y[:-1] != states_avg.y[1:])
    swap_indices = np.where(state_swaps)[0] + 1
    # add first and last index
    swap_indices = np.concatenate(([0], swap_indices, [len(states_avg.x) - 1]))
    # get y-limit of axis TODO: ideally from -inf to +inf
    ymin, ymax = f_avg.y.get_lim(buffer=0.2)   # larger than the actual limits
    if args.ymin is not None:
        ymin = args.ymin
    if args.ymax is not None:
        ymax = args.ymax
    for i, _ in enumerate(swap_indices[:-1]):
        x = states_avg.x[[swap_indices[i], swap_indices[i+1]]]
        ax.fill_between(x=x, y1=[ymin, ymin], y2=[ymax, ymax],
                        color=special_colors[states_avg.y[swap_indices[i]]], alpha=0.2)
    #TODO: maybe use this (https://stackoverflow.com/questions/69100231/matplotlib-fill-area-with-different-colors-based-on-a-value)
    #TODO: to avoid the for-loop
    ax.set_xlim(f_avg.x.get_lim())
    ax.set_ylim(f_avg.y.get_lim(buffer=0.1))
    ax.set_title(title)
    return ax

def plot_layerPosAvg(ax, f, args, title="layer position (distr)"):
    if args.withDistribution:   #this first because it overrides the error bars
        f.plotDistribution(ax=ax, bins=args.bins, withColorBar=not args.hide_colorbars, colorbar_kwargs={'labelPos': 'y'})
    if args.single_ensembles is not None:
        single_ens_kw = {'lw': 1}
        for ens in args.single_ensembles:
            f.plot(ax=ax, slice=(ens, ...), label=f"ens={ens}", **single_ens_kw)
    f.plotAverageWithError(ax=ax, color='red', with_label=True, lw=2, asymmetric=not args.symmetric_errors)
    # if args.withDistribution: loc = 'upper left'     # speeds up process
    # else: loc = None
    ax.legend(handlelength=1, loc='upper left')
    ax.set_title(title)
    return ax

def plot_layerPosAll(ax, f, args, title="layer position (all)"):
    numberOfEnsembles = f.y.numberOfEnsembles
    if args.maxAll < numberOfEnsembles:
        numberOfEnsembles = args.maxAll
    for i in range(numberOfEnsembles):
        f.slice_ensembles(i).plot(ax=ax, alpha=0.8, lw=0.5)
    ax.set_xlim(f.x.get_lim())
    ax.set_ylim(f.y.get_lim())
    ax.set_title(title)
    return ax

def plot_ampAvg(ax, a, args, title="layer position amplitude (distr)"):
    #if args.withDistribution:   #this first because it overrides the error bars
        #a.plotDistribution(ax=ax, bins=args.bins, withColorBar=not args.hide_colorbars, colorbar_kwargs={'labelPos': 'y'})
    if args.single_ensembles is not None:
        single_ens_kw = {'lw': 1}
        for ens in args.single_ensembles:
            a.plot(ax=ax, slice=(ens, ...), label=f"ens={ens}", **single_ens_kw)
    a.plotAverageWithError(ax=ax, color='red', with_label=True, lw=2, asymmetric=not args.symmetric_errors)
    ax.legend(handlelength=1, loc='lower right')
    ax.set_ylim(0, a.y.max() * 1.1)
    ax.set_title(title)
    return ax

def plot_ampAll(ax, a, args, title="layer position amplitude (all)"):
    numberOfEnsembles = a.y.numberOfEnsembles
    if args.maxAll < numberOfEnsembles:
        numberOfEnsembles = args.maxAll
    for i in range(numberOfEnsembles):
        a.slice_ensembles(i).plot(ax=ax, alpha=0.8, lw=0.5)
    ax.set_xlim(a.x.get_lim())
    ax.set_ylim(0, a.y.max() * 1.1)
    ax.set_title(title)
    return ax

def plot_angBondAvg(ax, psi, args, title="angular bond parameter (distr)"):
    for i in [0, 1]:
        psi_tmp = deepcopy(psi)
        psi_tmp.y.slice((..., i), inPlace=True)
        if args.single_ensembles is not None:
            single_ens_kw = {'lw': 1}
            for ens in args.single_ensembles:
                psi_tmp.plot(ax=ax, slice=(ens, ...), label=f"ens={ens}", **single_ens_kw)
        psi_tmp.plotAverageWithError(ax=ax, color=angBondColors[i], with_label=True, lw=2, asymmetric=not args.symmetric_errors)
    ax.legend(handlelength=1, loc='upper right')
    ax.set_ylim(0, 1)
    ax.set_ylabel("$\\psi_n$")
    ax.set_title(title)
    return ax

def plot_angBondAll(ax, psi, args, scatter=True, with_avg=False, title="angular bond parameter (all)"):
    numberOfEnsembles = psi.y.numberOfEnsembles
    if args.maxAll < numberOfEnsembles:
        numberOfEnsembles = args.maxAll
    label = {0: "$\\psi_4$", 1: "$\\psi_6$"}
    if args.lw is None:
        lw = 2
    else:
        lw = args.lw
    for i in range(numberOfEnsembles):
        kwargs = {}
        if scatter:
            kwargs['s'] = lw ** 2
        else:
            kwargs['lw'] = lw
        kwargs.setdefault('alpha', 1 / numberOfEnsembles)
        if not with_avg:
            kwargs["alpha"] *= 2
        psi_single = psi.slice_ensembles(i)
        for p in [0, 1]:
            psi_single.plot(ax=ax, slice=(..., p), c=angBondColors[p], label=label[p], scatter=scatter, **kwargs)
        label = {0: None, 1: None}
    if with_avg:
        psi_avg = psi.buildEnsembleAverage()
        label_avg = {0: "$\\left< \\psi_4 \\right>$", 1: "$\\left< \\psi_6 \\right>$"}
        for p in [0, 1]:
            psi_avg.plot(ax=ax, slice=(..., p), c=angBondColors[p], label=label_avg[p], lw=lw)
    if args.locl is None:
        loc = "upper right"
    else:
        loc = args.locl
    legend = ax.legend(handlelength=1, loc=loc)
    for i in range(2):
        legend.legendHandles[i].set_alpha(1.)
    ax.set_xlim(psi.x.get_lim())
    ax.set_ylim(0, 1)
    ax.set_ylabel("$\\psi_4, \\psi_6$")
    ax.set_title(title)
    return ax

def plot_angBondLBAvg(ax, psi, args, t_settling=None, scatter=True, avg_periods=False, title="angular bond Lissajou (ens-avg)"):
    if t_settling is not None:
        #if np.isinf(t_settling):
        if True:    # TODO: debug line
            xmin, xmax = psi.x.get_lim()
            t_settling = xmin + (xmax - xmin) * 2/3
        psi2 = psi.crop(xStart=t_settling)
    else:
        psi2 = psi
    psi_avg = psi2.buildEnsembleAverage()
    if args.single_ensembles is not None:   # needed before the first plot command
        with_label = True
    else:
        with_label = False
    if t_settling is not None:
        cperiod = 0.5
        colormap = None
        if avg_periods:
            psi_avg.build_periodic_avg(period=cperiod, inPlace=True)
    else:
        cperiod = None
        colormap = "viridis_r"
    if scatter:
        kw_plot = {'s': 1}
        if avg_periods:
            kw_plot['s'] = 10
    else:
        kw_plot = {}
    psi_avg.plotLissajou(ax=ax, colors=psi_avg.x, cperiod=cperiod, colormap=colormap,
                         with_label=with_label, withColorBar=not args.hide_colorbars, scatter=scatter, **kw_plot)
    if args.single_ensembles is not None:
        kw_plot_single_ens = deepcopy(kw_plot)
        if not scatter:
            kw_plot_single_ens['lw'] = 1
        for ens in args.single_ensembles:
            psi_tmp = psi2.slice((ens, ...))
            psi_tmp.plotLissajou(ax=ax, label=f"ens={ens}", **kw_plot_single_ens)
        ax.legend(handlelength=1, loc='upper right')
    if title is not None and t_settling is not None:
        title = title[:-1] + ", settled" + title[-1]
    if title is not None and avg_periods:
        title = title[:-1] + ", period-avg" + title[-1]
    ax.set_title(title)
    return ax

def plot_angBondLBAll(ax, psi, args, time_as_color=False, t_settling=None, scatter=True,
                      title="angular bond Lissajou (all)", **kwargs):
    if t_settling is not None:
        #if np.isinf(t_settling):
        if True:
            xmin, xmax = psi.x.get_lim()
            t_settling = xmin + (xmax - xmin) * 2/3
        psi2 = psi.crop(xStart=t_settling)
    else:
        psi2 = psi
    numberOfEnsembles = psi.y.numberOfEnsembles
    if args.maxAll < numberOfEnsembles:
        numberOfEnsembles = args.maxAll
    for i in range(numberOfEnsembles):
        psi_tmp = psi2.slice_ensembles(i)
        kwargs_cur = kwargs.copy()
        if scatter:
            kwargs_cur['s'] = 2
            kwargs_cur['alpha'] = 0.1
            kwargs_cur['marker'] = '.'
        else:
            kwargs_cur['alpha'] = 0.8
            kwargs_cur['linewidth'] = 0.5
        if time_as_color:
            kwargs_cur['colors'] = psi_tmp.x
            kwargs_cur['cperiod'] = 0.5
        else:
            kwargs_cur['colors'] = psi_tmp.x
            kwargs_cur['cperiod'] = None
            kwargs_cur['colormap'] = "viridis_r"
        kwargs_cur.setdefault("withColorBar", not args.hide_colorbars)
        if i != 0:
            kwargs_cur['withColorBar'] = False
        psi_tmp.plotLissajou(ax=ax, scatter=scatter, **kwargs_cur)
    if title is not None and t_settling is not None:
        title = title[:-1] + ", settled" + title[-1]
    ax.set_title(title)
    return ax

def plot_ts_depinning(axes, t_settling, label_ax=None, unit=None):
    if not np.isinf(t_settling):
        for ax in axes:
            ax.axvline(t_settling, color='black', ls="--")
        if label_ax is not None:
            t_settling_label = f"$t_s^\\mathrm{{depin}} = {t_settling:.2f} {unit}$"
            ypos = label_ax.get_ylim()[0] + 0.05 * (label_ax.get_ylim()[1] - label_ax.get_ylim()[0])
            label_ax.annotate(t_settling_label, (t_settling * 1.05, ypos))
    return None

def plot_ts_structure(axes, t_settling, label_ax=None, unit=None):
    if not np.isinf(t_settling):
        for ax in axes:
            ax.axvline(t_settling, color='gray', ls="--")
        if label_ax is not None:
            t_settling_label = f"$t_s^\\mathrm{{struc}} = {t_settling:.2f} {unit}$"
            ypos = label_ax.get_ylim()[0] + 0.05 * (label_ax.get_ylim()[1] - label_ax.get_ylim()[0])
            label_ax.annotate(t_settling_label, (t_settling * 1.05, ypos))
    return None

def plot_state_classification(axes, args, state_ratios: dict=None, fmt=".2f", translate=None,
                              kw_translate=None, bgcolor=None, kw_bgcolor=None, show_missing=False,
                              kw_annotate=None):
    if translate is None:
        translate = lambda x: x
    if kw_translate is None:
        kw_translate = {}
    if kw_annotate is None:
        kw_annotate = {}
    thr4 = args.thr4
    thr6 = args.thr6
    for ax in axes:
        ax.axhline(thr6, lw=0.5, color='black')
        ax.axvline(thr4, lw=0.5, color='black')
        if state_ratios is not None:
            keys = [i for i in range(4)]
            # 0: s, 1: d, 2: h, 3: f
            pad = 0.01
            pos = {0: (thr4 + pad, 0 + pad), 1: (0 + pad, 0 + pad), 2: (0 + pad, 1 - pad), 3: (thr4 + pad, 1 - pad)}
            va = {0: 'bottom', 1: 'bottom', 2: 'top', 3: 'top'}
            for key in keys:
                if key in state_ratios:
                    ratio = state_ratios[key]
                elif show_missing:
                    ratio = 0
                else:
                    continue
                if ratio < 0.01 and ratio > 0:
                    prefix = "$<$"
                    ratio = 0.01
                else:
                    prefix = ""
                # ratio_with_percent = ratio * 100
                # ax.annotate(f"{prefix}{ratio_with_percent:{fmt}}\% {translate(key, **kw_translate)}", pos[key], xycoords='axes fraction',
                #             ha='left', va=va[key])
                ax.annotate(f"{translate(key, **kw_translate)} ({prefix}{ratio:{fmt}}$\\tau_\\omega$)", pos[key], xycoords='axes fraction',
                            ha='left', va=va[key], **kw_annotate)

    if bgcolor is not None:
        if bgcolor == "auto":
            bgcolor = state_colors
        if kw_bgcolor is None:
            kw_bgcolor = {}
        kw_bgcolor.setdefault("zorder", 0.9)
        kw_bgcolor.setdefault("alpha", 0.2)
        ax.fill_between(x=[thr4, 1], y1=[thr6, thr6], y2=[0, 0], color=bgcolor["s"], **kw_bgcolor)
        ax.fill_between(x=[0, thr4], y1=[thr6, thr6], y2=[0, 0], color=bgcolor["d"], **kw_bgcolor)
        ax.fill_between(x=[0, thr4], y1=[thr6, thr6], y2=[1, 1], color=bgcolor["h"], **kw_bgcolor)
        ax.fill_between(x=[thr4, 1], y1=[thr6, thr6], y2=[1, 1], color=bgcolor["f"], **kw_bgcolor)
    return None

def modes_with_time(modes: list):
    wanted_modes = []
    for mode in modes:
        if 'LB' not in mode:
            wanted_modes.append(mode)
    return wanted_modes

def modes_with_LB(modes: list, settled=False):
    wanted_modes = []
    for mode in modes:
        if 'LB' in mode:
            if settled and 'Settled' not in mode:
                continue
            wanted_modes.append(mode)
    return wanted_modes
