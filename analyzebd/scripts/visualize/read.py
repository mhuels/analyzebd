from analyzebd.functions.layerPosition_time import LayerPositionTime
from analyzebd.functions.angularBond_time import AngularBondTime
from analyzebd.helpers.clock import stop_time
# from copy import deepcopy
from analyzebd.scripts.visualize.analysis import analyze_settling_time_depinning
from analyzebd.scripts.visualize.processing_times import ProcessingTimes
from analyzebd.classification import Structure_Classifier, Transition_Classifier

def read_data(folder, args, clock=None, verbose=False):
    numberOfEnsembles = args.numberOfEnsembles
    f = LayerPositionTime()
    processing_times = ProcessingTimes()
    # processing_time = {'read layer positions': 0, 'read angular bonds': 0, 'calculate amplitudes': 0}
    with stop_time("reading layer positions", clock=clock, verbose=verbose):
        f.readFromFile(folder=folder, numberOfEnsembles=numberOfEnsembles, xStart=args.xStart, xEnd=args.xEnd, xUnit=args.xUnit)
    processing_times["read layer positions"] = clock(-2, -1)
    f.y.setScale("a")
    f.y.getRelative(inPlace=True)
    f.y.slice((..., args.direction), inPlace=True)
    f_avg = f.buildEnsembleAverage()

    with stop_time("calculating amplitudes", clock=clock, verbose=verbose):
        a = f.getWindowedAmplitudes(mode=args.amplitude_mode)
    processing_times["calculate amplitudes"] = clock(-2, -1)
    a_avg = a.buildEnsembleAverage()

    psi = AngularBondTime()
    with stop_time("reading angular bonds", clock=clock, verbose=verbose):
        psi.readFromFile(folder=folder, numberOfEnsembles=numberOfEnsembles, xStart=args.xStart, xEnd=args.xEnd, xUnit=args.xUnit)
    processing_times["read angular bonds"] = clock(-2, -1)
    psi_avg = psi.buildEnsembleAverage()

    numberOfEnsembles = max(f.y.numberOfEnsembles, psi.y.numberOfEnsembles)

    struc_classifier = Structure_Classifier(thr4=args.thr4, thr6=args.thr6)
    with stop_time("classifying structures", clock=clock, verbose=verbose):
        states = struc_classifier.classify(psi)
        states_avg = struc_classifier.classify(psi_avg)
    processing_times["classify structures"] = clock(-2, -1)

    # collect data
    data = {'layerPosition': f, 'angularBond': psi, 'amplitude': a, 'states': states,
            'f_avg': f_avg, 'psi_avg': psi_avg, 'a_avg': a_avg, 'states_avg': states_avg
            }
    metadata = {'numberOfEnsembles': numberOfEnsembles, 'numberOfPeriods': f.x.max(),
                'struc_classifier': struc_classifier,
                'processing times': processing_times}
    return data, metadata
