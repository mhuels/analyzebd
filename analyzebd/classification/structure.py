import numpy as np
from copy import deepcopy
from analyzebd import Quantity
from analyzebd.functions import AngularBondTime, Function

class Structure_Classifier():
    THR4_DEFAULT = 0.75
    THR6_DEFAULT = 0.75

    def __init__(self, thr4: float=None, thr6: float=None):
        self.setup()
        if thr4 is None:
            self.thr4 = self.THR4_DEFAULT
        else:
            self.thr4 = thr4
        if thr6 is None:
            self.thr6 = self.THR6_DEFAULT
        else:
            self.thr6 = thr6

    def setup(self):
        self.states = np.array(["s", "d", "h", "f"])
        self.states_long = np.array(["square", "disordered", "hexagonal", "forbidden"])
        return self

    def classify(self, psi: AngularBondTime):   #, return_as_str=False
        """
        Classify the structure of an angular bond trajectory (at each timestep, for each ensemble, etc.)
        Args:
            psi (AngularBondTime): trajectory/function to classify
        Returns:
            state_func (Function): function with same x-values and states as y-values
        """
        shape = psi.y.array.shape[:-1]      # remove the last dimension
        y = np.full(shape, np.nan, dtype=int)   # TODO: this should have shape of psi.y without the last axis
        split = [s.squeeze() for s in np.split(psi.y.getArray(), 2, axis=-1)]  # split[0] = psi4, split[1] = psi6
        lthr4 = split[0] > self.thr4    # psi4 larger than thr4
        lthr6 = split[1] > self.thr6    # psi6 larger than thr6
        # lthr4 = psi.y.array[:,0] > self.thr4    # psi4 larger than thr4
        # lthr6 = psi.y.array[:,1] > self.thr6    # psi6 larger than thr6
        y[np.logical_and(lthr4, ~lthr6)] = 0
        y[np.logical_and(~lthr4, ~lthr6)] = 1
        y[np.logical_and(~lthr4, lthr6)] = 2
        y[np.logical_and(lthr4, lthr6)] = 3

        state_func = Function(x=deepcopy(psi.x),
                              y=Quantity().setup(name="structure", array=y)
                              )
        state_func.y.numberOfEnsembles = psi.y.numberOfEnsembles
        state_func.y.metadata = psi.y.metadata
        return state_func

    def translate(self, num: np.ndarray, long=False):
        if long:
            return self.states_long[num]
        else:
            return self.states[num]
