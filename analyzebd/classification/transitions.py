import h5py
import numpy as np
from pathlib import Path
from copy import deepcopy
from analyzebd import Function
from analyzebd.classification import Structure_Classifier
from itertools import combinations

class Transition_Classifier():
    def __init__(self, struc_classifier: Structure_Classifier):
        self.setup(struc_classifier)

    def setup(self, struc_classifier: Structure_Classifier=None):
        self.struc_classifier = struc_classifier
        self.states = combinations_all_lengths(np.arange(self.struc_classifier.states.size))
        return self

    def classify(self, f: Function, Pmin: float=0.):
        """
        Classify the transition state of a Function based on its counts.
        Args:
            f (Function): function to classify
            Pmin (float): minimum ratio a state has to appear to be counted
        Returns:
            transition_state (int): transition state number
        """
        if Pmin < 0. or Pmin > 1.:
            raise ValueError(f"pmin={Pmin} has to be in the range [0,1]")
        unique, counts = f.y.count_unique(as_dict=False)
        # remove those, whose counts are too low
        relative_counts = counts / counts.sum()
        larger_than_pmin = (relative_counts > Pmin)
        unique = unique[larger_than_pmin]
        if len(unique) == 0:
            from warnings import warn
            warn(f"relative counts={relative_counts} are all < {Pmin}=pmin. Try to choose a smaller pmin.")
        unique_sorted = np.sort(unique)
        state_combination = tuple(unique_sorted)
        # translate to index from state table
        # iterate through states and stop at the first matching
        transition_state = -1
        for i, v in enumerate(self.states):
            if v == state_combination:
                transition_state = i
                break
        return transition_state

    def translate(self, num: int, long=False, long_sep="-"):
        # not so easy to allow for an array of nums because tuples don't mix well with axes
        # maybe by filling tuples to length 4 with nans
        combination = self.states[num]
        ret = ""
        sep = long_sep
        for i, c in enumerate(combination):
            if long:
                if i >= len(combination) - 1: sep = ""
                append = self.struc_classifier.states_long[c] + sep
            else:
                append = self.struc_classifier.states[c]
            ret += append
        return ret

    def write_to_file(self, filename, verbose=False):
        filename = Path(filename)
        index = np.arange(len(self.states), dtype=int)
        abr = []
        name = []
        for i in index:
            abr.append(self.translate(i))
            name.append(self.translate(i, long=True))
        with h5py.File(filename, "w") as f:
            f.create_dataset("index", data=index)
            f.create_dataset("abr", data=np.array(abr, dtype='S'))
            f.create_dataset("name", data=np.array(name, dtype='S'))
            f.attrs["thr4"] = self.struc_classifier.thr4
            f.attrs["thr6"] = self.struc_classifier.thr6
        if verbose:
            print(f"Saved transition classification legend in {filename}")
        return filename

def combinations_all_lengths(x: np.ndarray):
    y = []
    for combination_length in range(1, len(x)+1):
        y += list(combinations(x, combination_length))
    return np.array(y, dtype=object)
