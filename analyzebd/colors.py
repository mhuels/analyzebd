import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import Normalize
from matplotlib.collections import LineCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable

class IntracolorPlot:
    def __init__(self, colormap=None, norm=None):
        self.reset()
        self.setColorMap(colormap)
        if norm is None:
            self.cnorm = Normalize(0, 1)
        else:
            self.cnorm = norm

    def reset(self):
        self._lc = None
        self._cmap = None
        self.colormap = None
        self.cnorm = None
        self.cbar = None
        return self

    # def __getattr__(self, item):
    #     return self.__lc

    def get_lc(self):
        return self._lc

    def setup(self, x, y, c):
        # x, y and c need to have same lengths, therefore c needs to be stretched or compressed
        multiplier = len(x) / len(c)    # e.g. 2.5 (c needs to be repeated this often)
        arange = np.arange(len(x), dtype=int)   # e.g. [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        index_map = np.floor(arange / multiplier).astype(int)   # e.g. [0, 0, 0, 1, 1, 2, 2, 2, 3, 3]
        colors = c[index_map]   # maps c-values to stretched/compressed colors-array
        # idea from https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/multicolored_line.html
        points = np.array([x, y]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        self._lc = LineCollection(segments, cmap=self._cmap, norm=self.cnorm)
        self._lc.set_array(colors)
        return self

    def setColorMap(self, colormap):
        if colormap is None:
            self.colormap = "winter"
        else:
            self.colormap = colormap
        self._cmap = cm.get_cmap(self.colormap)
        return self._cmap

    def get_cmap(self):
        return self._cmap

    def set_linewidth(self, lw):
        self._lc.set_linewidth(lw)
        return self
