from analyzebd.helpers.file_reader import find_setting_in_file
from .metadata import *
import numpy as np

class MetadataDrivenSingleParticle(Metadata):
    def reset(self):
        super().reset()
        self.drivingAmplitude = np.nan
        self.drivingPeriod = np.nan
        self.drivingPhaseShift = np.nan
        self.substrateAmplitude = np.nan
        self.substratePeriod = np.nan
        self.substrateShiftConstant = np.nan
        self.substrateShiftAmplitude = np.nan
        self.substrateShiftPeriod = np.nan
        self.substrateShiftPhaseShift = np.nan
        self.substratePhaseShift = np.nan
        self.duration = np.nan
        self.numberOfPeriods = np.nan
        self.printPosition = np.nan
        self.printVelocity = np.nan
        self.skipSteps = np.nan
        return self

    def readFromFile(self, folder, filename="drivenSingleParticle.out"):
        super().readFromFile(folder, filename)
        kwargs = {'reversed': True, 'posEnd': 2, 'skipTo': "System Initialization"}
        self.drivingAmplitude = find_setting_in_file(self.src, "drivingAmplitude:", dtype=float, **kwargs)
        self.drivingPeriod = find_setting_in_file(self.src, "drivingPeriod:", dtype=float, **kwargs)
        self.drivingPhaseShift = find_setting_in_file(self.src, "drivingPhaseShift:", dtype=float, **kwargs)
        self.substrateAmplitude = find_setting_in_file(self.src, "substrateAmplitude:", dtype=float, **kwargs)
        self.substratePeriod = find_setting_in_file(self.src, "substratePeriod:", dtype=float, **kwargs)
        self.substrateShiftConstant = find_setting_in_file(self.src, "substrateShiftConstant:", dtype=float, **kwargs)
        self.substrateShiftAmplitude = find_setting_in_file(self.src, "substrateShiftAmplitude:", dtype=float, **kwargs)
        self.substrateShiftPeriod = find_setting_in_file(self.src, "substrateShiftPeriod:", dtype=float, **kwargs)
        self.substrateShiftPhaseShift = find_setting_in_file(self.src, "substrateShiftPhaseShift:", dtype=float, **kwargs)
        self.substratePhaseShift = find_setting_in_file(self.src, "substratePhaseShift:", dtype=float, **kwargs)
        self.duration = find_setting_in_file(self.src, "duration:", dtype=float, **kwargs)
        self.numberOfPeriods = find_setting_in_file(self.src, "numberOfPeriods:", dtype=float, **kwargs)
        self.printPosition = find_setting_in_file(self.src, "printPosition:", dtype=int, **kwargs)
        self.printVelocity = find_setting_in_file(self.src, "printVelocity:", dtype=int, **kwargs)
        self.skipSteps = find_setting_in_file(self.src, "skipSteps:", dtype=int, **kwargs)
        self.tauSub = 1 / (2 * np.pi) * self.substrateAmplitude / self.substrateAmplitude * self.kT
        return self