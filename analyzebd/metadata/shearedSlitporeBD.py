from analyzebd.helpers.file_reader import find_setting_in_file
from .metadata import *

class MetadataShearedSlitporeBD(Metadata):
    def reset(self):
        super().reset()
        self.shearRate = np.nan
        self.amplitude = np.nan
        self.phaseOffset = np.nan
        self.duration = np.nan
        self.numberOfPeriods = np.nan
        self.printLayerPosition = np.nan
        self.printLayerVelocity = np.nan
        self.printStress = np.nan
        self.printStressFourier = np.nan
        self.printEnergy = np.nan
        self.printAngularBond = np.nan
        self.printSnapshots = np.nan
        self.printPairCorrelation = np.nan
        self.tauSub = 1.22e-3
        self.amplitudeCrit = 214
        self.latticeConstant = 1.0342
        return self

    def readFromFile(self, folder, filename="shearedSlitporeBD.out"):
        super().readFromFile(folder, filename)
        kwargs = {'reversed': True, 'posEnd': 2, 'skipTo': "System Initialization"}
        #TODO: make this faster by opening the file only once
        self.shearRate = find_setting_in_file(self.src, "shearRate:", dtype=float, **kwargs)
        self.amplitude = find_setting_in_file(self.src, "amplitude:", dtype=float, **kwargs)
        self.phaseOffset = find_setting_in_file(self.src, "phaseOffset:", dtype=float, **kwargs)
        self.duration = find_setting_in_file(self.src, "duration:", dtype=float, **kwargs)
        self.numberOfPeriods = find_setting_in_file(self.src, "numberOfPeriods:", dtype=float, **kwargs)
        self.printLayerPosition = find_setting_in_file(self.src, "printLayerPosition:", dtype=int, **kwargs)
        self.printLayerVelocity = find_setting_in_file(self.src, "printLayerVelocity:", dtype=int, **kwargs)
        self.printStress = find_setting_in_file(self.src, "printStress:", dtype=int, **kwargs)
        self.printStressFourier = find_setting_in_file(self.src, "printStressFourier:", dtype=int, **kwargs)
        self.printEnergy = find_setting_in_file(self.src, "printEnergy:", dtype=int, **kwargs)
        self.printAngularBond = find_setting_in_file(self.src, "printAngularBond:", dtype=int, **kwargs)
        self.printSnapshots = find_setting_in_file(self.src, "printSnapshots:", dtype=int, **kwargs)
        self.printPairCorrelation = find_setting_in_file(self.src, "printPairCorrelation:", dtype=int, **kwargs)
        return self

    def get_identifier(self, beautify=False, float_fmt=".6e"):
        if beautify:
            from analyzebd.helpers.format import beautify_scientific
            wrapper = lambda s: beautify_scientific(s)
        else:
            wrapper = lambda s: s
        identifier = ""
        if beautify: identifier += "$"
        if beautify: identifier += "\\dot{\\gamma}_0 \\tau_\\mathrm{B}"
        else: identifier += "amplitude tB"
        identifier += "="
        identifier += wrapper(f"{self.amplitude:{float_fmt}}")
        identifier += ","
        if beautify: identifier += "\\tau_\\omega / \\tau_\\mathrm{B}"
        else: identifier += "period/tB"
        identifier += "="
        identifier += wrapper(f"{self.period:{float_fmt}}")
        if beautify: identifier += "$"
        identifier += "\n"
        if beautify: identifier += "$"
        if beautify: identifier += "N"
        else: identifier += "#ensembles"
        identifier += "="
        identifier += f"{self.numberOfEnsembles}"
        identifier += ","
        if beautify: identifier += "T/\\tau_\\omega"
        else: identifier += "#periods"
        identifier += "="
        identifier += f"{self.numberOfPeriods:.0f}"
        if beautify: identifier += "$"
        return identifier

def get_used_cputimes(folder, filename="shearedSlitporeBD.out", numberOfEnsembles=None, verbose=False):
    """
    Get a statistic of needed cputimes for a given calculation.
    :param folder: str or Path
    :param filename: str
    :param numberOfEnsembles: int or None
    :param verbose: bool
    :return: list of list of floats (shape=(numberOfEnsembles, numberOfRestarts))
    """
    path = Path(folder) / filename
    if verbose:
        print(f"Reading used cputimes in {path} ... ", end='')
    keywords = ["Task finished after", "Task finished in", "Task interrupted after"]
    try:
        numberOfEnsemblesInFile = find_setting_in_file(path, "numberOfEnsembles", dtype=int, skipComment=False)
        if numberOfEnsemblesInFile is None: numberOfEnsemblesInFile = 1
    except FileNotFoundError:
        return None
    if numberOfEnsembles is None:
        numberOfEnsembles = numberOfEnsemblesInFile
    if numberOfEnsembles > numberOfEnsemblesInFile:
        from warnings import warn
        message = f"Calculation in {path} only contains {numberOfEnsemblesInFile} ensembles, but you requested {numberOfEnsembles}."
        warn(message)
    cputimes = []
    if numberOfEnsembles == 1 and numberOfEnsemblesInFile == 1:
        current_ensemble = 0
        cputimes.append([])
    else:
        current_ensemble = -1
    with open(path, 'r') as f:
        for line in f:
            if numberOfEnsemblesInFile > 1 and "#Ensemble" in line:
                current_ensemble += 1
                if current_ensemble >= numberOfEnsembles:
                    break
                cputimes.append([])
            elif any(keyword in line for keyword in keywords):
                linesplit = line.split()
                current_cputime = float(linesplit[3])
                cputimes[current_ensemble].append(current_cputime)
    return cputimes
