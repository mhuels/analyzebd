from pathlib import Path
from analyzebd.helpers.file_reader import find_setting_in_file as find
import numpy as np

class Metadata:
    def __init__(self, folder=None, **kwargs):
        self.reset()
        if (folder is not None):
            self.readFromFile(folder=folder, **kwargs)

    def reset(self):
        self.src = None
        self.period = np.nan
        self.dt = np.nan
        self.kT = np.nan
        self.mu = np.nan
        self.D0 = np.nan
        self.numberOfTimesteps = np.nan
        self.tauSub = np.nan
        self.numberOfEnsembles = np.nan
        return self

    def readFromFile(self, folder, filename):
        self.src = Path(folder) / filename
        if not self.src.exists():
            # # try finding an ensemble file
            # dirs = [d for d in Path(folder).glob('ens*')]
            # if len(dirs) > 0:
            #     self.src = dirs[0] / filename
            # if not self.src.exists():
            #     raise FileNotFoundError(f"{self.src} does not exist!")
            raise FileNotFoundError(f"{self.src} does not exist!")
        kwargs = {'reversed': True, 'posEnd': 2, 'skipTo': "System Initialization"}
        self.period = find(self.src, "period:", dtype=float, **kwargs)
        # temporary
        if self.period is None:
            self.period = find(self.src, "oscillationPeriod:", dtype=float, **kwargs)
        self.dt = find(self.src, "dt:", dtype=float, **kwargs)
        self.kT = find(self.src, "kT:", dtype=float, **kwargs)
        self.mu = find(self.src, "mu:", dtype=float, **kwargs)
        self.D0 = find(self.src, "D0:", dtype=float, **kwargs)
        if self.D0 is None:
            self.D0 = self.mu * self.kT
        self.numberOfTimesteps = find(self.src, "numberOfTimesteps:", dtype=int, **kwargs)
        return self
