from copy import deepcopy

class Unit:
    def __init__(self, name="1", conversion=1, latex=None, latex_fft=None):
        self.set(name, conversion, latex, latex_fft)

    def __str__(self):
        return self.name

    def __float__(self):
        return self.conversion

    def set(self, name, conversion=1, latex=None, latex_fft=None):
        """
        conversion = old_unit/new_unit
        """
        self.name = name
        self.conversion = conversion
        if latex is None:
            self.latex = self.name
        else:
            self.latex = latex
        if latex_fft is None:
            self.latex_fft = invertString(self.latex, suffix="^{-1}")
        else:
            self.latex_fft = latex_fft
        return self

    def getLatex(self, fft=False):
        if fft:
            return self.latex_fft
        else:
            return self.latex

    def invert(self, inPlace=False):
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        obj.conversion = 1 / self.conversion
        obj.name = invertString(self.name)
        obj.latex = invertString(self.latex, suffix="^{-1}")
        obj.latex_fft = invertString(self.latex_fft, suffix="^{-1}")
        return obj

def invertString(name, suffix="-1"):
    # TODO: doesn't work with multiple suffixes (e.g. d-1 tB-1)
    if name.endswith(suffix):
        new_name = name[:-len(suffix)]
    else:
        new_name = name + suffix
    return new_name
