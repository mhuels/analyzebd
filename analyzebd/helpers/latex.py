from copy import deepcopy

pav_enclosing = ("\\mkern 1.5mu\\overline{\\mkern-1.5mu", "\\mkern-1.5mu}\\mkern 1.5mu")

class Modulo:
    def __init__(self, modulo: float, fmt=".1f"):
        self.modulo = modulo
        self.fmt = fmt

    def getString(self, sep="\\ "):
        return f"{sep}\%{sep}{self.modulo:{self.fmt}}"

    def __str__(self):
        return self.getString()

class LatexLabel:
    def __init__(self, **kwargs):
        self.reset()
        self.setup(**kwargs)

    def reset(self):
        self.name = "x"
        self.subscripts = [""]
        self.superscripts = [""]
        self.enclosings = [("", "")]
        self._modulo = None
        return self

    def setup(self, name, subscripts: list=None, superscripts: list=None, enclosings: list=None, modulo: Modulo=None):
        self.name = name
        self.setSubScripts(subscripts)
        self.setSuperScripts(superscripts)
        self.setEnclosings(enclosings)
        self.setModulo(modulo)
        return self

    def __str__(self):
        return self.getLabel()

    def getLabel(self, with_dollars=True, with_modulo=False):
        # TODO: subcripts, superscripts and starter/finisher enclosings can be LatexLabels
        s = ""
        if with_dollars:
            s += "$"
        # enclosing starter
        for enc in self.enclosings:
            s += enc[0]
        # the main name
        s += self.name
        # subscripts
        if len(self.subscripts[0].strip()):
            s += "_{"
            N = len(self.subscripts)
            for i in range(N):
                s += self.subscripts[i]
                if i < N - 1:
                    s += ", "
            s += "}"
        # superscripts
        if len(self.superscripts[0].strip()):
            s += "^{"
            N = len(self.superscripts)
            for i in range(N):
                s += self.superscripts[i]
                if i < N - 1:
                    s += ", "
            s += "}"
        # enclosing finisher
        for enc in reversed(self.enclosings):
            s += enc[1]
        if with_modulo and self._modulo is not None:
            s += f" {self._modulo}"
        if with_dollars:
            s += "$"
        return s

    def setName(self, name: str):
        self.name = name
        return self

    def appendName(self, s: str):
        self.name = self.name + s
        return self

    def prependName(self, s: str):
        self.name = s + self.name
        return self

    def setSubScripts(self, indices: list=None):
        if indices is None:
            indices = [""]
        if isinstance(indices, str):
            indices = [indices]
        self.subscripts = indices
        return self

    def appendSubScript(self, s: str):
        remove_if_empty(self.subscripts, -1)
        self.subscripts.append(s)
        return self

    def prependSubScript(self, s: str):
        remove_if_empty(self.subscripts, 0)
        self.subscripts.insert(0, s)
        return self

    def getSubScripts(self):
        return deepcopy(self.subscripts)

    def setSuperScripts(self, superscripts: list=None):
        if superscripts is None:
            superscripts = [""]
        if isinstance(superscripts, str):
            superscripts = [superscripts]
        self.superscripts = superscripts
        return self

    def appendSuperScript(self, s: str):
        remove_if_empty(self.superscripts, -1)
        self.superscripts.append(s)
        return self

    def prependSuperScript(self, s: str):
        remove_if_empty(self.superscripts, 0)
        self.superscripts.insert(0, s)
        return self

    def getSuperScripts(self):
        return deepcopy(self.superscripts)

    def setEnclosings(self, enclosings: list=None):
        if enclosings is None:
            enclosings = [("", "")]
        if isinstance(enclosings, tuple):
            enclosings = [enclosings]
        if not isinstance(enclosings, list):
            raise TypeError(f"enclosings must be of type 'list', but is of type {type(enclosings)}.")
        if len(enclosings) < 1:
            raise ValueError(f"enclosings is an empty list!")
        if not isinstance(enclosings[0], tuple):
            raise TypeError(f"element 0 in enclosings must be of type 'tuple', but is of type {type(enclosings[0])}")
        # TODO: check this for every element
        self.enclosings = enclosings
        return self

    def appendEnclosing(self, e: tuple):
        # TODO: remove_if_empty?
        self.enclosings.append(e)
        return self

    def prependEnclosing(self, e: tuple):
        self.enclosings.insert(0, e)
        return self

    def getEnclosings(self):
        return deepcopy(self.enclosings)

    def setModulo(self, modulo: Modulo):
        if isinstance(modulo, Modulo) or modulo is None:
            self._modulo = modulo
        elif isinstance(modulo, float):
            self._modulo = Modulo(modulo)
        else:
            raise TypeError(f"modulo={modulo} is of type {type(modulo)}, but only types Modulo and float are allowed.")
        return self

    def getModulo(self):
        return self._modulo

def remove_if_empty(l: list, pos: int):
    if len(l[pos].strip()) == 0:
        l.pop(pos)
    return l
