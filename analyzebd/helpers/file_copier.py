

def replace_strings_in_file(filename, old_strings, new_strings, verbose=True):
    """
    Replace certain strings within a file (inplace).
    :param filename: str or pathlib.Path
    :param old_strings: list of str
    :param new_strings: list of str
    :param verbose: bool
    :return: bool (True if changes were made/necessary.)
    """
    # TODO: might be better to merge old_strings and new_strings into a single list with tuple elements
    # Safely read the input filename using 'with'
    with open(filename) as f:
        s = f.read()
        found_one = False
        for old_string in old_strings:
            if old_string not in s:
                if verbose:
                    print(f"'{old_string}' not found in {filename}.")
            else:
                found_one = True
    if not found_one:
        return False

    # Safely write the changed content, if found in the file
    with open(filename, 'w') as f:
        for old_string, new_string in zip(old_strings, new_strings):
            if verbose:
                print(f"Changing '{old_string}' to '{new_string}' in {filename}")
            s = s.replace(old_string, new_string)
        f.write(s)
    return True