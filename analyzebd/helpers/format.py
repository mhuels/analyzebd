import string
import numpy as np
from itertools import cycle
from math import ceil, log10, floor

def label_axes(axes: list, labels=None, loc=None, enclosing: tuple=None, **kwargs):
    """
    Walks through axes and labels each.

    kwargs are collected and passed to `annotate`

    Parameters
    ----------
    fig : Figure
         Figure object to work on
    labels : iterable or None
        iterable of strings to use to label the axes.
        If None, lower case letters are used.
    loc : len=2 tuple of floats
        Where to put the label in axes-fraction units
    enclosing: tuple of str
        strings to surround each label with
    **kwargs:
        kwargs are forwarded to ax.annotate()
    """
    if labels is None:
        labels = string.ascii_lowercase
    if enclosing is None:
        enclosing = ("", "")

    # re-use labels rather than stop labeling
    labels = cycle(labels)
    if loc is None:
        loc = (.01, .98)
    if not isinstance(loc, list):
        loc = [loc]
    loc = cycle(loc)
    kwargs.setdefault("va", "top")
    for ax, label, l in zip(axes, labels, loc):
        before, after = enclosing
        label = f"{before}{label}{after}"
        ax.annotate(label, xy=l,
                    xycoords='axes fraction',
                    **kwargs)
    return labels

def beautify_scientific(formatted_number: str, dollar=False, cdot=False):
    """
    Converted scientific number formats to a latex format
    :param formatted_number: str
    :return: str
    """
    e_loc = formatted_number.find('e')
    exponent = int(formatted_number[(e_loc+1):])
    number = formatted_number[:e_loc]
    if dollar:
        dollar_env = "$"
    else:
        dollar_env = ""
    if cdot:
        multiplier = "\\! \\cdot \\!"   #\! is a small negative space in LaTeX
    else:
        multiplier = "\\!\\! \\times \\!\\!"
    if number == "1":
        beautified = f"{dollar_env} 10^{{{exponent}}}{dollar_env}"
    else:
        beautified = f"{dollar_env}{number} {multiplier} 10^{{{exponent}}}{dollar_env}"
    return beautified

def format_cbar_limits(cbar, smaller_min=False, larger_max=False, fmt='.0e', min_label=None, max_label=None):
    # test if fmt is valid
    number = -12.345
    number_fmt = f"{number}:{fmt}"
    # float format
    if fmt.endswith('f'):
        def format_ticklabel(tick):
            return f"{tick:{fmt}}"
    elif fmt.endswith('e'):
        # math formatter
        math_format = {'dollar': True, 'cdot': True}
        def format_ticklabel(tick):
            return beautify_scientific(f"{tick:{fmt}}", **math_format)
    else:
        raise ValueError(f"fmt={fmt} is not supported.")
    # create ticks
    vmin = cbar.vmin
    vmax = cbar.vmax
    zticks = get_ticks(vmin, vmax)
    # TODO: get minor ticks as well
    # set new labels
    zticklabels = [format_ticklabel(t) for t in zticks]
    if smaller_min:
        # add lower tick and ticklabel
        if ticks_are_close(zticks[0], vmin, cbar=cbar):
            zticks.pop(0)
            zticklabels.pop(0)
        zticks = [vmin] + zticks
        if min_label is None:
            min_label = "$<$" + format_ticklabel(vmin)
        zticklabels = [min_label] + zticklabels
    if larger_max:
        # add upper tick and ticklabel
        if ticks_are_close(zticks[-1], vmax, cbar=cbar):
            zticks.pop(-1)
            zticklabels.pop(-1)
        zticks = zticks + [vmax]
        if max_label is None:
            max_label = "$>$" + format_ticklabel(vmax)
        zticklabels = zticklabels + [max_label]
    # set new ticks and ticklabels
    cbar.set_ticks(zticks)
    cbar.set_ticklabels(zticklabels)
    return cbar

def get_ticks(vmin, vmax):
    ticks = []
    # first tick is next larger power of 10 starting from vmin
    first_tick = 10 ** ceil(log10(vmin))
    ticks.append(first_tick)
    # get powers of 10 until vmax
    next_tick = first_tick * 10
    while(next_tick < vmax):
        ticks.append(next_tick)
        next_tick *= 10
    return ticks

def ticks_are_close(tick1, tick2, cbar):
    vmin = cbar.vmin
    vmax = cbar.vmax
    scale = cbar.ax.get_yscale()
    if scale == "log":
        nmin = log10(vmin)
        nmax = log10(vmax)
        n1 = log10(tick1)
        n2 = log10(tick2)
    elif scale == "linear":
        nmin = vmin
        nmax = vmax
        n1 = tick1
        n2 = tick2
    else:
        raise ValueError(f"scale={scale} is not supported.")
    rel_diff = abs(n1 - n2) / abs(nmax - nmin)  # value between 0 and 1
    # plot maximally 4 labels TODO: depends on yaxis- and font-size
    if rel_diff <= 1/4:
        return True
    else:
        return False

# source: https://stackoverflow.com/questions/45332056/decompose-a-float-into-mantissa-and-exponent-in-base-10-without-strings
from decimal import Decimal
def fexp(number: float):
    """
    Return exponent of float.
    :param number: float
    :return: int
    """
    (sign, digits, exponent) = Decimal(number).as_tuple()
    return len(digits) + exponent - 1

def fman(number: float):
    """
    Return mantisse of float.
    :param number: float
    :return: float
    """
    return float(Decimal(number).scaleb(-fexp(number)).normalize())
##############################################################################################################################

def shift_decimal(decimal: float, shift: int):
    """
    Move the dot of a decimal n times to the left (or right if negative).
    :param decimal: float
    :param shift: int
    :return: str
    """
    decimal = str(decimal)
    pos_dot = decimal.find('.')
    without_dot = decimal.replace('.', '')
    if shift < 0:
        without_dot2 = without_dot + shift * '0'
        new_pos_dot = pos_dot + shift
    elif shift > 0:
        without_dot2 = shift * '0' + without_dot
        new_pos_dot = pos_dot
    else:
        without_dot2 = without_dot
        new_pos_dot = pos_dot
    with_dot = without_dot2[:new_pos_dot] + '.' + without_dot2[new_pos_dot:]
    return with_dot

def format_mean_err(mean: float, err: float):
    # determine mantisse and exponent of mean
    mean_m = fman(mean)
    mean_e = fexp(mean)
    if err is None:
        err_m = 0
        err_e = mean_e
    # extra case if err == inf
    elif np.isinf(err):
        err_m = np.inf
        err_e = mean_e
        # TODO: truncate mean_m to two or three digits
    else:
        # determine mantisse and exponent of err and shift them to match mean exponent
        err_m = fman(err)
        err_m = round(err_m, 1)     # round error to two significant digits
        err_e = fexp(err)
        diff_e = mean_e - err_e     # exponent difference
        # shift err onto same exponent
        err_e += diff_e
        err_m = shift_decimal(err_m, diff_e)
        # truncate/extend mean mantisse to precision of error mantisse
        mean_m = str(Decimal(mean_m).quantize(Decimal(err_m)))
    # combine all
    exp_sign = '+' if mean_e >= 0 else '-'
    s = f"({mean_m} +/- {err_m}) e{exp_sign}{abs(mean_e):02d}"
    return s

def latex_append_index(latex_name: str, app: str):
    if '_' in latex_name:
        bracePos = latex_name.rfind('}')
        new = latex_name[:bracePos] + f",{app}" + latex_name[bracePos:]
    else:
        new = latex_name + f"_{{{app}}}"
    return new

def round_significant(number: float, ndigits=3):
    delta_digits = int(floor(log10(abs(number)))) + 1
    rounded = round(number, ndigits=ndigits - delta_digits)
    s = str(rounded).rstrip('0').rstrip('.')
    return s

