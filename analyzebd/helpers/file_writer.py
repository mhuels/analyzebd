import sys
import json
import pkg_resources
from pathlib import Path
import matplotlib.pyplot as plt
from analyzebd.helpers.clock import Clock
from re import search

def writeSourcesToFile(fname="resources.txt", verbose=False):
    fname = Path(fname)
    installed_packages = pkg_resources.working_set
    # needed step to sort list
    installed_packages_list = sorted([f"{i.key}=={i.version}" for i in installed_packages])
    installed_packages_str = ""
    for s in installed_packages_list:
        installed_packages_str += f"{s}\n"
    with open(fname, "w") as f:
        f.write(installed_packages_str)
    if verbose:
        print(f"Saved resources to {fname}")
    return True

def writeArgvToFile(fname="command.txt", verbose=False):
    fname = Path(fname)
    with open(fname, 'w') as f:
        argv = sys.argv
        for i, arg in enumerate(argv):
            if search(r"\s", arg):  #add double quotes (") around arg if whitespaces are present
                argv[i] = "\"" + arg + "\""
        print(' '.join(sys.argv), file=f)
    if verbose:
        print(f"Saved logfile to {fname}")
    return True

def saveany(fname, formats, save_func, save_resources=True, save_log=True, verbose=True, **kwargs):
    """
    Save the figure or animation with the specified formats.
    :param fname: str
    :param formats: str or list of str (default: ['.png', '.pdf'])
    :param save_func: Function object that expects a filename and **kwargs
    :param save_resources: bool
    :param verbose: bool
    :return: outnames: list of str
    """
    clock = Clock()
    fname = Path(fname).resolve()
    fname.parent.mkdir(parents=True, exist_ok=True)
    if isinstance(formats, str):
        formats = [formats]
    outnames = [fname.with_suffix(fname.suffix + suffix) for suffix in formats]
    for outname in outnames:
        clock.lap()
        save_func(outname, **kwargs)
        if verbose:
            clock.lap()
            print(f"Saved plot to {outname} ... {clock(-2, -1)}")
    if save_resources:
        writeSourcesToFile(fname.with_suffix(fname.suffix + ".res"), verbose=verbose)
    if save_log:
        writeArgvToFile(fname.with_suffix(fname.suffix + ".cmd"), verbose=verbose)
    return outnames

def savefig(fname, formats=None, save_resources=True, save_log=True, verbose=True, **kwargs):
    """
    Save the current figure with the specified formats.
    :param fname: str
    :param formats: str or list of str (default: ['.png', '.pdf'])
    :param save_resources: bool
    :param verbose: bool
    :return: outnames: list of str
    """
    if formats is None:
        formats = ['.png', '.pdf']
    kwargs.setdefault("bbox_inches", 'tight')
    kwargs.setdefault("dpi", 600)
    return saveany(fname=fname, formats=formats, save_func=plt.savefig,
                   save_resources=save_resources, save_log=save_log, verbose=verbose, **kwargs)

def saveanimation(fname, obj, formats='.mp4', save_resources=True, verbose=True, **kwargs):
    """
    Save the given animation with the specified formats.
    :param fname: str
    :param formats: str or list of str (default: ['.png', '.pdf'])
    :param save_func: Function object that expects a filename and **kwargs
    :param save_resources: bool
    :param verbose: bool
    :return: outnames: list of str
    """
    return saveany(fname, formats=formats, save_func=obj.save, save_resources=save_resources, verbose=verbose, **kwargs)

def dump_json(obj: dict, outname: str, verbose=False):
    with open(outname, "w") as f:
        return_obj = json.dump(obj, f)
    if verbose: print(f"Dumped {outname}")
    return return_obj
