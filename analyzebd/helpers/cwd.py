import os
import contextlib

@contextlib.contextmanager
#def cwd(path, mkdir=False, debug=False):
def cwd(path, mkdir=False):
    """
    Change curring working direction intermediately.
    Usage:
        with cwd(some_path):
            do so some stuff in some_path
        do so some other stuff in old cwd
    Args:
        path: str or Path
        mkdir: bool
            create directory if not existing
    Returns:
        nothing
    """
    CWD = os.getcwd()
    if os.path.exists(path) == False and mkdir:
        os.makedirs(path)
#    if debug:
#        os.chdir(path)
#        yield
#        os.chdir(CWD)
#        return
    os.chdir(path)
    yield
    os.chdir(CWD)
    return True
