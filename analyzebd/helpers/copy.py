from copy import deepcopy

def copy_inplace(obj: object, inPlace: bool):
    if inPlace:
        obj_new = obj
    else:
        obj_new = deepcopy(obj)
    return obj_new
