import numpy as np

def is_monotonously_increasing(array: np.ndarray):
    diff = np.diff(array)
    x = (diff >= 0)
    return x.all()

def is_monotonously_decreasing(array: np.ndarray):
    diff = np.diff(array)
    x = (diff <= 0)
    return x.all()

def is_sorted(array: np.ndarray):
    return (is_monotonously_increasing(array) or is_monotonously_decreasing(array))

def window_intervals(x: np.ndarray, period: float, n: int=None):
    """
    Get window intervals w[i] of 1D-array x[i] such that x[i+w[i]] >= x[i] + period.
    Args:
        x (np.nadarray): 1D-array
        period (float): window length
        n (int): length of output array (defaults to smallest stable value; larger values might work)
    Returns:
        w (np.nadarray): 1D-array
    """
    N = len(x)
    dx = x[1:] - x[:-1]  # len(dx) = N - 1
    numberOfPeriods = int((x.max() - x.min()) / period)
    pointsPerPeriod = int(period / dx[0])
    if n is None:
        n = pointsPerPeriod * numberOfPeriods
        # TODO: this is true for equidistant arrays, might be problematic for others
    w = np.ones(n, dtype=int)
    # if x-values are equidistant, we can do a shortcut
    if np.isclose(np.percentile(dx, 1), np.percentile(dx, 99)):  # percentiles instead of min/max in case of outliers
        w = np.full(n, pointsPerPeriod)
    else:
        from warnings import warn
        from tqdm import tqdm
        warn(f"x-values are not equidistant, determining window lengths will take a while...")
        for i in tqdm(range(n)):
            inc_sum = 0
            j = 0
            for j in range(N - i - 1):  # -1 because x_inc is on index shorter
                if inc_sum > period: break
                inc_sum += dx[i + j]
            w[i] = j
    return w

def interpolate_discrete(x: np.ndarray, xp: np.ndarray, fp: np.ndarray):
    if not is_monotonously_increasing(xp):
        raise ValueError(f"xp is not monotoneously increasing!")
    # determine i such that xp[i] < x[j] < xp[i+1]
    i = get_left_index(x, xp)
    # compute distance to left and right value
    dl = x - xp[i]
    dr = xp[i+1] - x
    # snap onto left function value if dl is smaller and onto right function value if dr is smaller
    i_snap = i
    i_snap[dl > dr] += 1
    y = fp[i_snap]
    return y

def get_left_index(x: np.ndarray, xp: np.ndarray, save_memory=False):
    # xp needs to be sorted from small to large
    if save_memory:     ### variant A: less memory, slower
        i = np.full_like(x, -1, dtype=int)
        for j, xpj in enumerate(xp):
            x_is_larger = (x >= xpj)
            i[x_is_larger] += 1
    else:               ### variant B: more memory, faster
        expanded_xp = np.repeat(xp[:, np.newaxis], x.size, axis=-1)
        expanded_x = np.repeat(x[np.newaxis, :], xp.size, axis=0)
        x_is_larger = (expanded_x >= expanded_xp)
        i = x_is_larger.sum(axis=0) - 1
    return i

def move_wished_first(unsorted: np.ndarray, wished: np.ndarray):
    unsorted = list(unsorted)
    sorted = []
    # add elements from wished first if they are present and remove them from unsorted
    for i in wished:
        if i in unsorted:
            sorted.append(i)
            unsorted.remove(i)
    # add any further elements from unsorted that are still missing
    sorted += unsorted
    sorted = np.array(sorted)
    return sorted
