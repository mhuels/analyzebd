def extractNameAndUnitFromString(s):
    posColon = s.find(':')
    posLBracket = s.find('[')
    posRBracket = s.find(']')
    name = s[posColon + 1:posLBracket].lstrip().rstrip()  # removed leading and trailing whitespaces
    unit = s[posLBracket + 1:posRBracket].lstrip().rstrip()
    return name, unit

def findFirstCharacter(s: str, keys):
    if not isinstance(keys, list):
        keys = [keys]
    min_pos = len(s)
    for k in keys:
        pos = s.find(k)
        if pos != -1 and pos < min_pos:
            min_pos = pos
    return min_pos