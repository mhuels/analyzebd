from copy import deepcopy

def print_supported_modes(modes):
    s = "Available modes: \n"
    for i, mode in enumerate(modes):
        s += f"{i: 2d}: {mode}\n"
    return s

def parse_modes(modes_in, supported_modes):
    if "all" in modes_in:
        modes = deepcopy(supported_modes)
    else:
        modes = deepcopy(modes_in)
    for i, mode in enumerate(modes):
        if mode.isdigit():
            m = int(mode)
            try:
                modes[i] = supported_modes[m]
            except IndexError:
                print(f"There is no mode representing number {m}. {print_supported_modes(supported_modes)}")
                exit(1)
    for mode in modes:
        if mode not in supported_modes:
            print(f"mode '{mode}' is not supported.\n{print_supported_modes(supported_modes)}")
            exit(1)
    return modes

def set_scales(ax, mode, args):
    if mode == "heatmap":
        xscale = args.xscale
        yscale = args.yscale
    elif mode == "xslices":
        xscale = args.yscale
        yscale = args.zscale
    elif mode == "yslices":
        xscale = args.xscale
        yscale = args.zscale
    else:
        raise ValueError(f"mode={mode} is not supported.")
    ax.set_xscale(xscale)
    ax.set_yscale(yscale)
    return xscale, yscale
