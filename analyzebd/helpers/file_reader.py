import numpy as np
from pathlib import Path
import json
import re

def find_setting_in_file(fil, keyword, posStart=None, posEnd=None, next_line=0, dtype=str, skipComment=True, comment='#',
                         reversed=False, skipTo=None, num=1):
    """
    Search the first occurence of a keyword in a file and return the string
    after the first whitespace.
    Args:
        fil (str): filename
        keyword (str)
        posStart (int): position (counted after whitespaces) to start
        posEnd (int): position where to end
        next_line (int): use the information from the n-th next line
        dtype (type): convert setting to given datatype
        skipComment (bool): skip comments
        comment (str): character that indicates comments
        reversed (bool): read from end to start
        skipTo (str): skip to first occurence of this string and keep looking for the keyword afterwards
                      (not affected by skipComment)
        num (int): number of occurences to find (-1 = all)
    Returns:
        value (str): string after keyword joined by whitespaces
    """
    if posStart is None:
        if next_line == 0:
            posStart = 1
        else:
            posStart = 0
    values = []
    num_found = 0
    if reversed:
        from file_read_backwards import FileReadBackwards
        opener = FileReadBackwards
        if next_line > 0:
            line_cache = (next_line + 1) * [None]     # n previous lines and the current line
    else:
        opener = open
    with opener(fil) as f:
        skipTo_reached = False
        for line in f:
            if (skipTo is not None) and (skipTo_reached is False):
                if skipTo in line:
                    skipTo_reached = True
                continue
            if reversed and next_line > 0:
                line_cache.pop(0)   # remove first element
                line_cache.append(line)     # add current element
            if keyword in line:
                if skipComment and line.startswith(comment):
                    continue
                if reversed and next_line > 0:
                    line = line_cache[0]
                else:
                    try:
                        for i in range(next_line):
                            line = next(f)
                    except StopIteration: break
                if line is not None:
                    linesplit = line.split()
                    value = ' '.join(linesplit[posStart:posEnd])
                    if value == '': value = None
                    else: value = dtype(value)
                else:
                    value = None
                values.append(value)
                num_found += 1
                if num_found == num:
                    break
    if len(values) == 0:
        return None
    elif len(values) == 1:
        return values[0]
    else:
        return values

# taken from https://stackoverflow.com/questions/17151210/numpy-loadtxt-skip-first-row
def skipper(fname, comment='#'):
    with open(fname) as fin:
        no_comments = (line for line in fin if not line.lstrip().startswith(comment))
        for row in no_comments:
            yield row


def params_from_dir_struc(folder, parameter_names, dtypes=float, delim="_|:"):
    """
    Extract parameter values from subfolder names of a directory.
    :param folder: string or Path
    :param parameter_names: str or list of str
    :param dtypes: type or list of types
    :param delim: str
    :return: dict with keys=parameter_names, containing np.arrays
    """
    folder = Path(folder)
    dir_names = [p for p in folder.iterdir() if p.is_dir()]
    if isinstance(parameter_names, str):
        parameter_names = [parameter_names]
    if type(dtypes) == type:
        dtypes = [dtypes] * len(parameter_names)
    parameter_values = {}
    # initialize
    for pname in parameter_names:
        parameter_values[pname] = []
    for dir_name in dir_names:
        split = re.split(delim, dir_name.name)
        for pindex, pname in enumerate(parameter_names):
            for i, s in enumerate(split):
                if pname == s:
                    value = dtypes[pindex](split[i + 1])
                    parameter_values[pname].append(value)
    for key in parameter_values:
        parameter_values[key] = np.unique(parameter_values[key])
    return parameter_values

def load_json(filename: str, verbose=False):
    with open(filename, "r") as f:
        obj = json.load(f)
    if verbose: print(f"Loaded {filename}")
    return obj
