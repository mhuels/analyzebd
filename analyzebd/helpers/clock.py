import datetime

class Clock:
    def __init__(self):
        self.reset()

    def reset(self):
        self.timePoints = []
        self.pauses = []
        self.pausePoints = []
        self.resumePoints = []
        self.paused = False
        self.lap()
        return self

    def __call__(self, *args, **kwargs):
        if len(args) == 0:
            if self.paused:
                return self.pausePoints[-1] - self.timePoints[0]
            else:
                now = datetime.datetime.utcnow()
                duration = now - self.timePoints[0]
                pause_duration = self.__getPauseDuration(self.timePoints[0], now)
                duration -= pause_duration
                if 'unit' in kwargs and kwargs['unit'] == "s":
                    return duration.total_seconds()
                else:
                    return duration
        elif len(args) == 1:
            return self.getTimePoint(*args)
        elif len(args) == 2:
            duration = self.getDuration(*args)
            if 'unit' in kwargs and kwargs['unit'] == "s":
                return duration.total_seconds()
            else:
                return duration
        else:
            raise ValueError(f"{self.__repr__()} call() was given {len(args)} arguments. Valid are only 0, 1 or 2.")

    def lap(self):
        if not self.paused:
            self.timePoints.append(datetime.datetime.utcnow())
            return len(self.timePoints) - 1
        else:
            raise RuntimeError("Can't take lap here, because clock is paused.")

    def getTimePoint(self, i):
        return self.timePoints[i]

    def getDuration(self, i, j):
        # total duration between timepoints i < j
        duration = self.timePoints[j] - self.timePoints[i]
        pause_duration = self.__getPauseDuration(self.timePoints[i], self.timePoints[j])
        return duration - pause_duration

    # returns total pause duration between timePoints i and j
    def __getPauseDuration(self, startPoint, endPoint):
        pause_duration = datetime.timedelta()  # start at 0
        # sum up all pauses that are between i < j
        for k in range(len(self.pauses)):
            if self.pausePoints[k] > startPoint and self.resumePoints[k] < endPoint:
                pause_duration += self.pauses[k]
        return pause_duration

    def pause(self):
        if not self.paused:
            self.pausePoints.append(datetime.datetime.utcnow())
        self.paused = True
        return self

    def resume(self):
        if self.paused:
            self.resumePoints.append(datetime.datetime.utcnow())
            self.pauses.append(self.resumePoints[-1] - self.pausePoints[-1])
        self.paused = False
        return self

import contextlib

@contextlib.contextmanager
def stop_time(name: str, clock: Clock=None, sep: str=" ... ", tqdm=False, verbose: bool=True):
    if clock is None:
        clock = Clock()
    if verbose: print(f"{name}{sep}", end="")   #, flush=True
    if tqdm:
        if verbose: print("")
    clock.lap()
    yield
    clock.lap()
    duration = clock(-2, -1)
    if verbose: print(duration)
    # return True
    return duration.total_seconds()     # can't really be accessed, infer from clock(-2, -1) from outside instead

@contextlib.contextmanager
def pause_clock(clock: Clock, verbose: bool=False):
    if verbose: print("Pausing clock")
    clock.pause()
    yield
    if verbose: print("Resuming clock")
    clock.resume()
    return True
