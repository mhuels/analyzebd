import os
import numpy as np
from pathlib import Path
from shutil import rmtree
from math import ceil
import subprocess as sp
from analyzebd.helpers.clock import Clock
from analyzebd.helpers.cwd import cwd
from analyzebd.quantities.quantity import scaleAs
from analyzebd.restarts import globals

def runCalculation(amplitude, period, workdir, numberOfEnsembles=1, Fsub0=1, numberOfPeriods=1, deterministic=False, skipTauSub=0,
                   reset=False, verbose=False, read_only=False, reversed=False):
    workdir = Path(workdir)
    executable = f"{os.getenv('HOME')}/local/drivenSingleParticle/build/bin/drivenSingleParticle"
    if not reversed:
        workdir = workdir / f"amplitude_{amplitude:e}_period_{period:e}"
    else:
        workdir = workdir / f"period_{period:e}_amplitude_{amplitude:e}"
    if read_only:
        return workdir
    if workdir.exists() and reset:
        rmtree(workdir)
        if verbose:
            print(f"Removed {workdir}")
    command = f"{executable}"
    command += f" --numberOfPeriods {numberOfPeriods} --printPositionPeriod 1e-3"
    command += f" --drivingAmplitude {amplitude} --drivingPeriod {period}"
    command += f" --substrateAmplitude {Fsub0} --substratePeriod 1 --substratePhaseShift 1"
    if deterministic:
        command += " --deterministic"
    if skipTauSub > 0:
        tauSub = 1 / Fsub0 / (2 * np.pi)
        skipPeriods = ceil(skipTauSub * tauSub / period)    # skip at least 1 period, but at least 10 substrate periods
        command += f" --skipPeriods {skipPeriods}"
    clock = Clock()
    # don't run if calculation has already been done
    # TODO: implement recognition of how many ensembles are missing
    if not (workdir / "positions.out").exists():
        if verbose:
            print(f"Generating {workdir} ... ", end='')
        with cwd(workdir, mkdir=True):
            with open("drivenSingleParticle.out", "a") as f:
                print(f"#numberOfEnsembles: {numberOfEnsembles}", file=f)
            with open("positions.out", "a") as f:
                print(f"#numberOfEnsembles: {numberOfEnsembles}", file=f)
            for i in range(numberOfEnsembles):
                if numberOfEnsembles > 1:
                    with open("drivenSingleParticle.out", "a") as f:
                        print(f"#Ensemble: {i}", file=f)
                    with open("positions.out", "a") as f:
                        print(f"#Ensemble: {i}", file=f)
                with open("drivenSingleParticle.out", "a") as f:
                    sp.run(command.split(), stdout=f)
                    # sp.run(command.split(), stdout=f, preexec_fn=lambda : os.setpgrp())     #  os.setpgrp() allows forwarding signals
                    # sp.Popen(command.split(), stdout=f, shell=True)
                if globals.ret_code != 0:
                    rmtree(workdir)
                    print(f"Removed {workdir} because of interrupting signal.")
                    return "interrupted"
        if verbose:
            print(clock())
    return workdir
