import numpy as np
from analyzebd.quantities.quantity import scaleAs
from analyzebd.metadata.metadata import Metadata
from analyzebd.quantities.time import Time
from analyzebd.quantities.position.position import Position
from analyzebd.functions.function import Function
from tqdm import tqdm
from numba import jit

class OverdampedBrownianDynamicsSimulator:
    def __init__(self, *args, **kwargs):
        self.setup(*args, **kwargs)

    def reset(self):
        self.metadata = Metadata()
        self.time = Time()
        self.position = Position()
        self.calculateForce = None
        self.__x = 0.
        self.__v = 0.
        self.__dt = 1e-5
        self.__timestep = 0
        return self

    def setup(self, force_func):
        self.reset()
        self.calculateForce = force_func
        self.time.metadata = self.metadata
        self.position.metadata = self.metadata
        return self

    def equationOfMotion(self):
        t = self.__timestep * self.__dt
        F = self.calculateForce(self.__x, t)
        self.__x += F * self.__dt
        self.__timestep += 1
        return self

    # @jit
    def simulate(self, time: float, unit=None):
        # self.time.array = np.arange(0., self.time._v2a(time), self.dt)
        # self.time.array = np.arange(0., numberOfTimesteps * self.__dt, self.__dt)
        self.time.array = np.arange(0., time, self.__dt)
        self.position.array = np.zeros_like(self.time.array)
        numberOfTimesteps = len(self.time.array)
        for i in tqdm(range(numberOfTimesteps)):
            self.position.array[i] = self.__x
            self.equationOfMotion()
        return self

    def getTrajectory(self):
        return Function(x=self.time, y=self.position)