def pad(v: float, dist: float, ax, axis: str):
    """
    Return a new position (data coordinates) that is padded by <dist> (figure coordinates) from <v> (data coordinates).
    Args:
        v (float): old position in data coordinates
        dist (float): padding distance in figure coordinates [0, 1]
        ax (plt.ax object): the ax that is currently used
        axis (str): 'x' or 'y'

    Returns:
        v_new (float): the padded position in data coordinates
    """
    if axis == 'x':
        vmin, vmax = ax.get_xlim()
        scale = ax.axes.get_xscale()
    else:
        vmin, vmax = ax.get_ylim()
        scale = ax.axes.get_yscale()
    if scale == 'linear':
        v_new = v + (vmax - vmin) * dist
    elif scale == 'log':
        v_new = v * (vmax / vmin) ** dist
    else:
        raise NotImplementedError(f"scale={scale} is not supported.")
    return v_new

def xpad(x, dist: float, ax):
    return pad(v=x, dist=dist, ax=ax, axis='x')

def ypad(y, dist: float, ax):
    return pad(v=y, dist=dist, ax=ax, axis='y')
