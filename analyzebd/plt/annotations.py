

def add_annotations(ax_mode_map: dict, settings: dict, **kwargs):
    """
    Add annotations from dictionary-type settings.
    Args:
        ax_mode_map (dict):
        settings (dict): e.g. {'a1': {'text': 'a1', 'xy': (1, 1)}}
        **kwargs:

    Returns:
        annotations (dict): e.g. {'a1: {'mode1': Annotation1}}
    """
    annotations = {}
    kwargs.setdefault("clip_on", False)     # annotations should reach over the axis limits
    for key in settings:
        kw = settings[key]
        kw.update(kwargs)
        kw.setdefault("modes", list(ax_mode_map.keys()))
        modes = kw.pop("modes")
        list2tuple(kw, "xy")
        list2tuple(kw, "xytext")
        list2tuple(kw, "xycoords")
        annotations[key] = {}
        for mode in modes:
            ax = ax_mode_map[mode]
            annotations[key][mode] = ax.annotate(**kw)
    return annotations

def list2tuple(d: dict, key: str):
    if key in d:
        entry = d.pop(key)
        d[key] = tuple(entry)
    return d
