import numpy as np
import matplotlib.pyplot as plt
from analyzebd.plt import plt_global
from math import ceil

def setup_mode_subplots(modes: list, spw: float=None, spr: float=None, verbose=False):
    num = len(modes)
    ncols = int(np.sqrt(num))  # rather less columns than rows
    nrows = int(ceil(num / ncols))
    if spw is None:
        subplot_width = plt_global['subplot_width']
    else:
        subplot_width = spw
    if spr is None:
        ratio = plt_global['ratio']
    else:
        ratio = spr
    subplot_height = subplot_width / ratio
    figsize = (subplot_width * ncols, subplot_height * nrows)
    fig, axes = plt.subplots(nrows, ncols, figsize=figsize)
    if verbose:
        print(f"Set up figure with {len(fig.axes)} axes (ncols: {ncols}, nrows: {nrows}).")
    plt.subplots_adjust(hspace=0.4, wspace=0.4, top=0.8)  # TODO: adjust top and wspace according to nrows and ncols
    # hide remaining axes
    for i in range(len(modes), nrows * ncols):
        fig.axes[i].axis('off')
    return fig, axes
