def add_markers(ax_mode_map: dict, markers: dict, **kwargs):
    """
    Add markers to axes.
    Args:
        ax: dict with modes as keys
        markers: dict with entries 'xy' and 'markers' or 'label'
        **kwargs: kwargs for ax.annotate() or ax.scatter()
    Returns:

    """
    kwargs.setdefault("clip_on", False)     # markers should reach over the axis limits
    for key in markers:
        m = markers[key]
        m.setdefault("modes", list(ax_mode_map.keys()))
        m.setdefault("markers", ["x"])
        num = len(m["markers"])
        m.setdefault("s", 15)
        m["s"] = expand(m["s"], num)
        m.setdefault("c", "black")
        m["c"] = expand(m["c"], num)
        m.setdefault("linewidths", 0.75)
        m["linewidths"] = expand(m["linewidths"], num)
        m.setdefault("empty", False)
        m["empty"] = expand(m["empty"], num)
        for i, marker in enumerate(m["markers"]):
            if m["empty"][i] is True:
                facecolors = 'none'
                edgecolors = m["c"]
                c = [None] * num
            else:
                facecolors = None
                edgecolors = None
                c = m["c"]
            for mode in m["modes"]:
                ax = ax_mode_map[mode]
                ax.scatter(*tuple(m["xy"]), marker=marker, c=c[i], s=m["s"][i],
                           facecolors=facecolors, edgecolors=edgecolors, linewidths=m["linewidths"][i], **kwargs)
    return None

def expand(x, num):
    if not isinstance(x, list):
        return [x] * num
    else:
        return x
