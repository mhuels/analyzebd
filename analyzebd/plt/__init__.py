import matplotlib.pyplot as plt

from .pads import xpad, ypad    # TODO: these should not be imported automatically by import *
from .color import lighten_color

plt_global = {'subplot_width': 4.8,     # this is adjusted to fit onto a 4k screen (3840x2160)
              'ratio': 16 / 9,
              'max_cols': 4,
              'max_rows': 4,
              }
plt_global['subplot_height'] = plt_global['subplot_width'] / plt_global['ratio']
plt_global['dpi'] = 2550 / (plt_global['max_cols'] * plt_global['subplot_width'])
# replace 2550 by horizontal screen size (+ scale)
plt.rcParams.update({'figure.dpi': plt_global['dpi']})

# latex
plt.rcParams.update({"text.usetex": True,
                     "font.family": "serif",
                     "font.sans-serif": ["Computer Modern Roman"]})
                     # "font.family": "sans-serif",
                     # "font.sans-serif": ["Computer Modern Serif"]})
