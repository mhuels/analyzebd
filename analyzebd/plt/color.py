import numpy as np
from matplotlib.colors import ListedColormap, CSS4_COLORS, Normalize

def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.
    source: https://stackoverflow.com/questions/37765197/darken-or-lighten-a-color-in-matplotlib

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])

def add_color_legend(values: np.ndarray, labels: np.ndarray, ax, cmap, cnorm,
                     extra_handles_labels: list=None, **kwargs):
    from matplotlib.patches import Patch
    handles = []
    for i, _ in enumerate(labels):
        handles.append(Patch(facecolor=cmap(cnorm(values[i]))))
    if extra_handles_labels is None:
        extra_handles_labels = []
    labels = list(labels)
    for entry in extra_handles_labels:
        handle, label = entry
        handles.append(handle)
        labels.append(label)
    kwargs.setdefault("handlelength", 1.)
    legend = ax.legend(handles, labels, **kwargs)
    return legend

def discrete_colormap(state_numbers: np.ndarray=None, name="discrete", color_palette: dict=None):
    if color_palette is None:
        color_palette = CSS4_COLORS
    all_colors = list(color_palette.keys())
    if state_numbers is None:
        state_numbers = np.arange(len(all_colors))
    cnorm = Normalize(state_numbers.min(), state_numbers.max())     # not sure if this works for min!=0
    colormap = ListedColormap(all_colors, N=state_numbers.size, name=name)
    return colormap, cnorm
