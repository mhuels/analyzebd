import numpy as np
from analyzebd.plt import *
from pathlib import Path
from copy import deepcopy
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from math import log10
from matplotlib.colors import Normalize, LogNorm
from analyzebd.helpers.strings import extractNameAndUnitFromString
from analyzebd.helpers.arrays import is_monotonously_increasing
from analyzebd import Quantity
from analyzebd.helpers.arrays import interpolate_discrete
import h5py
from analyzebd.helpers.copy import copy_inplace
from analyzebd import Function
from analyzebd.helpers import Clock

class Function2D:
    def __init__(self, **kwargs):
        self.reset()
        self.setup(**kwargs)

    def reset(self):
        self.src = None
        self.x = None
        self.y = None
        self.z = None
        self.xFmt = "e"
        self.yFmt = "e"
        self.resetPlot()
        self.attrs = {}
        return self

    def resetPlot(self):
        self.ax = None
        self.cbar = None
        self.colormap = None
        self._cmap = None
        self.cnorm = None
        self.shading = "auto"
        return self

    def setup(self, x: Quantity=None, y: Quantity=None, z: Quantity=None):
        """
        z contains function values z[i, j] corresponding to x[j], y[i].
        :param x: Quantity (shape=(N_x,))
        :param y: Quantity (shape=(N_y,))
        :param z: Quantity (shape=(N_y, N_x))
        :return:
        """
        if x is None:
            x = Quantity(name="x")
        if y is None:
            y = Quantity(name="y")
        if z is None:
            z = Quantity(name="z")
        self.x = x
        self.y = y
        self.z = z
        return self

    def __deepcopy__(self, memodict=None):
        if memodict is None:
            memodict = {}
        new = type(self).__new__(type(self))  # self can be a child this way (no __init__ called)
        memodict[id(self)] = new
        new.x = deepcopy(self.x, memodict)
        new.y = deepcopy(self.y, memodict)
        new.z = deepcopy(self.z, memodict)
        new.src = self.src
        new.attrs = self.attrs.copy()

        # should be resetted
        new.resetPlot()

        # should not be deepcopied
        # new.ax = self.ax
        # new.cbar = self.cbar
        # new.colormap = self.colormap
        # new.cnorm = self.cnorm
        # new.shading = self.shading
        return new

    def readFromFile(self, filename, folder='.'):
        self.src = Path(folder) / filename
        self.__readHeader()
        data = np.loadtxt(self.src)
        # https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.pcolormesh.html#axes-pcolormesh-grid-orientation
        self.x.setArray(data[0, 1:])  # first row
        self.y.setArray(data[1:, 0])  # first column
        self.z.setArray(
            data[1:, 1:])  # matrix z[i, j], where i is the row and j the column index (standard matrix notation)
        # i =  y, j = x
        return self

    def __readHeader(self, comment='#'):
        # self.src needs to be set
        with open(self.src, 'r') as f:
            for line in f:
                if not line.startswith(comment):
                    break
                if f"{comment} x:" in line:
                    self.x.name, unit = extractNameAndUnitFromString(line)
                    self.x.setScale(unit, conversion=1.)     # TODO: either don't fix conversion or make setScale()'s conversion factor relative
                elif f"{comment} y:" in line:
                    self.y.name, unit = extractNameAndUnitFromString(line)
                    self.y.setScale(unit, conversion=1.)
                elif f"{comment} z:" in line:
                    self.z.name, unit = extractNameAndUnitFromString(line)
                    self.z.setScale(unit, conversion=1.)

    def read_suitable(self, *args, **kwargs):
        """
        Alias of newer load_suitable().
        """
        return self.load_suitable(*args, **kwargs)

    def load_suitable(self, filename, folder='.', reset=False, inPlace=False, verbose=False, **kwargs):
        """
        :param filename: str or Path
        :param folder: str or Path
        :param reset: bool
        :param inPlace: bool
        :param verbose: bool
        :param kwargs: additional kwargs passed to load-function
        :return: self of deepcopy of self
        """
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        sd_in = deepcopy(obj)
        if reset is False:
            try:
                if Path(filename).name.endswith("hdf5"):
                    sd_in.load_hdf5(filename=filename, folder=folder, **kwargs)
                else:
                    sd_in.readFromFile(folder=folder, filename=filename)
            except FileNotFoundError:
                sd_in = None
        # check if data was read successfully
        if sd_in is None or len(sd_in) == 0:
            reset = True
        # check if x- and y-values are the required ones
        elif len(obj.x) != len(sd_in.x) \
                or len(obj.y) != len(sd_in.y) \
                or not np.isclose(obj.x.getArray(), sd_in.x.getArray()).min() \
                or not np.isclose(obj.y.getArray(), sd_in.y.getArray()).min():
            reset = True
        # check if all keys in attrs are the same
        elif sd_in.attrs_identical(obj.attrs) is False:
            reset = True
        if reset is True:
            # discard if not the same
            if verbose:
                print(f"Read data from {filename} is not suitable. Initializing with NaN-values...")
            obj.z.setArray(np.full((len(obj.y), len(obj.x)), np.nan))
        else:
            obj.z.setArray(sd_in.z.getArray())
            obj.src = Path(folder) / filename
            obj.attrs = sd_in.attrs.copy()
            # if inPlace:   # doesn't work for some reason
            #     self = sd_in
            # else:
            #     obj = sd_in
        return obj

    def attrs_identical(self, attrs: dict) -> bool:
        identical = True
        for key in attrs.keys():
            if key not in self.attrs.keys():
                identical = False
                break
            elif self.attrs[key] != attrs[key]:
                identical = False
                break
        return identical

    def writeToFile(self, filename, verbose=False, **kwargs):
        # create merged numpy array of x, y and z
        data = self.z.getArray()
        ## insert row (axis=0) in the top with x-values
        data = np.insert(data, 0, values=self.x.getArray(), axis=0)
        ## insert column (axis=1) on the left with y-values
        y = np.insert(self.y.getArray(), 0, 0)
        data = np.insert(data, 0, values=y, axis=1)

        if "header" not in kwargs:
            header = f"x: {self.x.name} [{self.x.unit}] \n"
            header += f"y: {self.y.name} [{self.y.unit}] \n"
            header += f"z: {self.z.name} [{self.z.unit}] \n"
            header += f"first row [0, 1:]: x \t first column [1:, 0]: y \n"
            header += f"the rest [1:, 1:]: z"
            kwargs["header"] = header
        if "delimiter" not in kwargs:
            kwargs["delimiter"] = "\t"
        if "fmt" not in kwargs:
            kwargs["fmt"] = "%e"
        np.savetxt(filename, X=data, **kwargs)
        if verbose:
            print(f"Saved Function2D to {filename}")
        return self

    def save_hdf5(self, filename, mode="a", verbose=False):
        clock = Clock()
        with h5py.File(filename, mode) as f:
            f.attrs["dtype"] = "Function2D"
            self.x.save_as_dataset(f=f, name="x")
            self.y.save_as_dataset(f=f, name="y")
            self.z.save_as_dataset(f=f, name="z")
            # metadata
            for key in self.attrs:
                f.attrs[key] = self.attrs[key]
        if verbose:
            print(f"Saved function {self.z.name_abr}({self.x.name_abr},{self.y.name_abr}) in {filename} ... {clock()}")
        return self

    def load_hdf5(self, filename, folder='.', force=False, verbose=False):
        clock = Clock()
        self.src = Path(folder) / filename
        with h5py.File(self.src, "r") as f:
            if not force:
                if f.attrs["dtype"] != "Function2D":
                    raise TypeError(f"Reading {filename} is not possible, because it has the wrong data type. If you are aware of the risk, try setting force=True to ignore this error.")
            for key in ["x", "y", "z"]:
                if key not in f.keys():
                    raise TypeError(f"The file {filename} is missing the required key {key}.")
            self.x.array = np.array(f["x"])
            self.x.name = f["x"].attrs["name"]
            self.x.name_abr = f["x"].attrs["name_abr"]
            self.x.name_latex = f["x"].attrs["name_latex"]
            self.x.setScale(unit=f["x"].attrs["unit"], conversion=f["x"].attrs["conversion"],
                            latex=f["x"].attrs["unit_latex"])
            self.y.array = np.array(f["y"])
            self.y.name = f["y"].attrs["name"]
            self.y.name_abr = f["y"].attrs["name_abr"]
            self.y.name_latex = f["y"].attrs["name_latex"]
            self.y.setScale(unit=f["y"].attrs["unit"], conversion=f["y"].attrs["conversion"],
                            latex=f["y"].attrs["unit_latex"])
            self.z.array = np.array(f["z"])
            self.z.name = f["z"].attrs["name"]
            self.z.name_abr = f["z"].attrs["name_abr"]
            self.z.name_latex = f["z"].attrs["name_latex"]
            self.z.setScale(unit=f["z"].attrs["unit"], conversion=f["z"].attrs["conversion"],
                            latex=f["z"].attrs["unit_latex"])
            # metadata
            for key in f.attrs.keys():
                self.attrs[key] = f.attrs[key]
        if verbose:
            print(f"Loaded function {self.z.name_abr}({self.x.name_abr},{self.y.name_abr}) from {self.src} ... {clock()}")
        return self

    def __str__(self):
        return str(self.z)

    def __len__(self):
        try:
            return len(self.z)
        except Exception:
            return 0

    def __getitem__(self, key):
        return self.z[key]

    def __setitem__(self, key, value):
        self.z[key] = value
        return self

    def __call__(self, x: float=None, y: float=None):
        if x is not None:
            j = self.x(v=x)
        else:
            j = None
        if y is not None:
            i = self.y(v=y)
        else:
            i = None
        if x is not None and y is not None:
            return self.z[i, j]
        elif x is not None and y is None:
            z = self.z.slice(slc=(slice(None), j))
            # new_x = self.x(i=j)     # TODO: needs Function call to return Quantity
            new_x = deepcopy(self.x)
            new_x.array = new_x.array[j]    # might need np.newaxis to keep it a numpy array
            return Function(x=deepcopy(self.y), y=z,
                            metadata={self.x.name: new_x})
        elif x is None and y is not None:
            z = self.z.slice(slc=(i, slice(None)))
            new_y = deepcopy(self.y)
            new_y.array = new_y.array[i]
            return Function(x=deepcopy(self.x), y=z,
                            metadata={self.y.name: new_y})
        else:
            raise ValueError(f"Can't call function value with x=None and y=None.")
            # TODO: maybe just return a copy of self

    def slice(self, x_slice=(slice(None),), y_slice=(slice(None),), inPlace=False):
        """
        :param x_slice: tuple of slices
        :param y_slice: tuple of slices
        :param inPlace: bool
        :return: self or deepcopy(self)
        """
        if not isinstance(x_slice, tuple) or not isinstance(y_slice, tuple):
            raise TypeError(f"Only tuples are allowed, e.g. (slice(0),)")
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        obj.x.slice(x_slice, inPlace=True, keep_dims=True)
        obj.y.slice(y_slice, inPlace=True, keep_dims=True)
        obj.z.slice(y_slice + x_slice, inPlace=True, keep_dims=True)
        return obj

    def interpolate_missing(self, inPlace=False, discrete=False):
        """
        Interpolate intermediate z-nan-values along y-axis first and then along x-axis.
        Example: 1   1 nan 1      1   1 nan 1      1   1   1 1
                 3 nan   5 7  --> 3 2.5   5 7  --> 3 2.5   5 7
                 4   4   6 8      4   4   6 8      4   4   6 8
        Args:
            inPlace: bool
        Returns:
            self
        """
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        obj.interpolate_missing_along_y(inPlace=True, discrete=discrete)
        obj.interpolate_missing_along_x(inPlace=True, discrete=discrete)
        return obj

    def interpolate_missing_along_x(self, inPlace=True, discrete=False):
        """
        Interpolate intermediate z-nan-values along x-axis.
        Example: 1   1 1      1   1 1
                 3 nan 5  --> 3   4 5
                 4   4 6      4   4 6
        Args:
            inPlace: bool
        Returns:
            self
        """
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        x = obj.x.array
        if not is_monotonously_increasing(x):
            raise NotImplementedError(f"interpolation only works for monotoneously increasing x-coordinates")
        z = obj.z.array
        Ny, Nx = z.shape
        for i in range(Ny):
            # remove nan-values on the outside; zslice is a pointer (everything is changed within obj.z)
            zslice = z[i, :]
            imin, imax = outer_non_nan(zslice)
            zslice = zslice[imin:imax + 1]
            xslice = x[imin:imax + 1]
            z_is_nan = np.isnan(zslice)
            # overwrite nan-positions
            if discrete:
                interpolator = interpolate_discrete
            else:
                interpolator = np.interp
            zslice[z_is_nan] = interpolator(x=xslice[z_is_nan],  # x-values at nan-positions
                                         xp=xslice[~z_is_nan],  # x-values at non-nan-positions
                                         fp=zslice[~z_is_nan],  # z-values at non-nan-positions
                                         )
        return obj

    def interpolate_missing_along_y(self, inPlace=True, discrete=False):
        """
        Interpolate intermediate z-nan-values along y-axis.
        Example: 1   1 1      1   1 1
                 3 nan 5  --> 3 2.5 5
                 4   4 6      4   4 6
        Args:
            inPlace: bool
        Returns:
            self
        """
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        y = obj.y.array
        if not is_monotonously_increasing(y):
            raise NotImplementedError(f"interpolation only works for monotoneously increasing x-coordinates")
        z = obj.z.array
        Ny, Nx = z.shape
        for i in range(Nx):
            # remove nan-values on the outside; zslice is a pointer (everything is changed within obj.z)
            zslice = z[:, i]
            imin, imax = outer_non_nan(zslice)
            zslice = zslice[imin:imax + 1]
            yslice = y[imin:imax + 1]
            z_is_nan = np.isnan(zslice)
            # overwrite nan-positions
            if discrete:
                interpolator = interpolate_discrete
            else:
                interpolator = np.interp
            zslice[z_is_nan] = interpolator(x=yslice[z_is_nan],  # y-values at nan-positions
                                         xp=yslice[~z_is_nan],  # y-values at non-nan-positions
                                         fp=zslice[~z_is_nan],  # z-values at non-nan-positions
                                         )
        return obj

    def plotHeatmap(self, ax=None, colormap=None, cnorm=None, cscale='linear', bad_color=None, withColorBar=True, **kwargs):
        if ax is None:
            ax = plt.subplot()
        self.ax = ax
        if cnorm is not None:
            self.cnorm = cnorm
        if self.cnorm is None:
            if cscale == "linear":
                normalizer = Normalize
            elif cscale == "log":
                normalizer = LogNorm
            else:
                raise ValueError(f"scale={cscale} is not supported. Available options: linear, log")
            self.cnorm = normalizer(self.z.min(), self.z.max())
        if colormap is not None:
            self.setColorMap(colormap)
        if self.colormap is None:
            self.setColorMap("Spectral_r")
        if bad_color is not None:
            self._cmap.set_bad(bad_color)   # this should not work, but it does (pcolormesh might need self._cmap instead of self.colormap)
        kwargs.setdefault("shading", self.shading)
        self.shading = kwargs["shading"]
        kwargs.setdefault("rasterized", True)
        # TODO: add option for logarithmic axes to manually choose edges (https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.pcolormesh.html)
        mesh = ax.pcolormesh(self.x.getArray(), self.y.getArray(), self.z.getArray(),
                             norm=self.cnorm, cmap=self.colormap, **kwargs)
        if withColorBar:
            self.addColorBar()
        self.ax.set_xlim(self.x.min(), self.x.max())
        self.ax.set_ylim(self.y.min(), self.y.max())
        self.ax.set_xlabel(self.x.getLatexLabel())
        self.ax.set_ylabel(self.y.getLatexLabel())
        return mesh

    def plotSlices(self, ax=None, axis='x', colormap=None, cnorm=None, cscale="linear",
                   withLegend=False, legendFmt=".2e", reverseLegend=False,
                   withColorBar=False, **kwargs):
        """
        Plot z(y) at different x.
        :param ax: pyplot.subplot.axes
        :param axis: 'x' (z(y)) or 'y' (z(x))
        :param kwargs: kwargs that go to ax.plot()
        :return: list of line handles
        """
        if ax is None:
            ax = plt.subplot()
        self.ax = ax
        line_handles = []
        if axis == 'x':
            slicer = self.x
            plot_xaxis = self.y
        elif axis == 'y':
            slicer = self.y
            plot_xaxis = self.x
        else:
            raise ValueError("axis-option only accepts 'x' or 'y'!")
        # color settings
        if colormap is not None:
            self.setColorMap(colormap)
        if self.colormap is None:
            self.setColorMap("cool")
        if cnorm is not None:
            self.cnorm = cnorm
        if self.cnorm is None:
            if cscale == "linear":
                normalizer = Normalize
            elif cscale == "log":
                normalizer = LogNorm
            else:
                raise ValueError(f"scale={cscale} is not supported. Available options: linear, log")
            self.cnorm = normalizer(slicer.min(), slicer.max())
        for index, value in enumerate(slicer.getArray()):
            # z[i,j] = z(x[j], y[i])
            if axis == 'x':
                plot_yaxis = self.z[:, index]
            else:
                plot_yaxis = self.z[index, :]
            # if colormap is not None:
            kwargs["color"] = self._cmap(self.cnorm(value))
            line, = self.ax.plot(plot_xaxis.getArray(), plot_yaxis, **kwargs)
            line_handles.append(line)
        self.ax.set_xlabel(plot_xaxis.getLatexLabel())
        self.ax.set_ylabel(self.z.getLatexLabel())
        self.ax.set_xlim(plot_xaxis.min(), plot_xaxis.max())
        self.ax.set_ylim(self.z.min(), self.z.max())
        if withLegend:
            labels = [f"{x:{legendFmt}}" for x in slicer.getArray()]
            if reverseLegend:
                line_handles = reversed(line_handles)
                labels = reversed(labels)
            ax.legend(line_handles, labels, title=f"{slicer.getLatexLabel()}", loc='center left',
                      bbox_to_anchor=(1.0, 0.5))
        if withColorBar:
            self.addColorBar(label=slicer.getLatexLabel())
        return line_handles

    def show_plot(self):
        plt.show()
        return True

    def addColorBar(self, norm=None, colormap=None, label=None, pad=0.05):
        if norm is None:
            norm = self.cnorm
        if colormap is None:
            colormap = self.colormap
        divider = make_axes_locatable(self.ax)
        cax = divider.append_axes("right", size="5%", pad=pad)
        self.cbar = plt.colorbar(cm.ScalarMappable(norm=norm, cmap=colormap), ax=self.ax, cax=cax)
        if label is None:
            label = self.z.getLatexLabel()
        self.cbar.ax.set_ylabel(label)
        return self.cbar

    def add_color_legend(self, values: np.ndarray, labels: np.ndarray, **kwargs):
        from analyzebd.plt.color import add_color_legend
        # maybe add an automatism to retrieve values (and provide a translator for the labels; if None print the numbers)
        legend = add_color_legend(values=values, labels=labels,
                                  ax=self.ax, cmap=self.get_cmap(), cnorm=self.cnorm, **kwargs)
        return legend

    def center_cnorm(self, center, scale='linear'):
        """
        center the colormap value range around a certain value (limits chosen based on z-values
        :param scale: 'linear' or 'log'
        :return:
        """
        if scale == 'linear':
            zmax = self.z.max()
            zmin = self.z.min()
            dmax = zmax - center     # d = delta
            dmin = center - zmin
            dabsmax = max(abs(dmax), abs(dmin))
            zmin_new = center - dabsmax
            zmax_new = center + dabsmax
            self.cnorm = Normalize(zmin_new, zmax_new)
        elif scale == 'log':
            logmax = log10(self.z.max())
            logmin = log10(self.z.min())
            logcenter = log10(center)
            dlogmax = logmax - logcenter
            dlogmin = logcenter - logmin
            dlogabsmax = max(abs(dlogmax), abs(dlogmin))
            if dlogabsmax == 0: dlogabsmax = 1.
            zmin_new = pow(10, logcenter - dlogabsmax)
            zmax_new = pow(10, logcenter + dlogabsmax)
            self.cnorm = LogNorm(zmin_new, zmax_new)
        else:
            raise ValueError(f"scale={scale} not supported.")
        return self.cnorm

    def get_cmap(self):
        return self._cmap

    def setColorMap(self, colormap):
        if colormap is None:
            self.colormap = "winter"
        else:
            self.colormap = colormap
        self._cmap = cm.get_cmap(self.colormap)
        return self._cmap

    def save_colormap_info(self, fname: str, verbose=False):
        with h5py.File(fname, "w") as f:
            if isinstance(self.colormap, str):
                f.attrs["colormap"] = self.colormap
            else:
                f.attrs["colormap"] = self.colormap.name
            f.attrs["cscale"] = type(self.cnorm).__name__
            f.attrs["vmin"] = self.cnorm.vmin
            f.attrs["vmax"] = self.cnorm.vmax
        if verbose:
            print(f"Saved colormap information in {fname}")
        return self

    def load_colormap_info(self, fname: str, dataset: str=None, verbose=False):
        print(f"Loading colormap info is not implemented yet! Continuing without it.")
        try:
            with h5py.File(fname, "r") as f:
                if dataset is not None:
                    attrs = f[dataset].attrs
                else:
                    attrs = f.attrs
                self.setColorMap(attrs["colormap"])
                if attrs["cscale"] == "Normalize":
                    normalizer = Normalize
                elif attrs["cscale"] == "LogNorm":
                    normalizer = LogNorm
                else:
                    raise KeyError(f"normalizer={attrs['cscale']} is invalid. Choose either 'Normalize' or 'LogNorm'!")
                self.cnorm = normalizer(vmin=attrs["vmin"], vmax=attrs["vmax"])
            if verbose:
                print(f"Loaded colormap information from {fname}")
        except:
            print(f"Could not load cinfo from {fname}. Choosing the colormap automatically...")
        return self

    def cropx(self, start: float=None, end: float=None, inPlace=False):
        # check that x is one-dimensional
        if len(self.x.getArray().shape) > 1:
            raise IndexError(f"self.x has shape {self.x.getArray().shape}. It must be one-dimensional to crop.")
        obj = copy_inplace(self, inPlace)
        obj.x.crop(start=start, end=end, inPlace=True)
        slc = (slice(None), slice(obj.x.iStart, obj.x.iEnd, None))
        obj.z.slice(slc=slc, inPlace=True)
        return obj

    def cropy(self, start: float=None, end: float=None, inPlace=False):
        # check that x is one-dimensional
        if len(self.x.getArray().shape) > 1:
            raise IndexError(f"self.x has shape {self.x.getArray().shape}. It must be one-dimensional to crop.")
        obj = copy_inplace(self, inPlace)
        obj.y.crop(start=start, end=end, inPlace=True)
        slc = (slice(obj.y.iStart, obj.y.iEnd, None), slice(None))
        obj.z.slice(slc=slc, inPlace=True)
        return obj

    def crop(self, xStart, xEnd, yStart, yEnd, inPlace=False):
        obj = copy_inplace(self, inPlace)
        obj.cropx(start=xStart, end=xEnd, inPlace=True)
        obj.cropy(start=yStart, end=yEnd, inPlace=True)
        return obj

def remove_outer_nan(array):
    is_nan = np.isnan(array)
    # determine first and last index which is non-nan
    indices_non_nan = np.nonzero(~is_nan)[0]
    smallest = indices_non_nan[0]
    largest = indices_non_nan[-1]
    # remove values outside of them
    return array[smallest:largest + 1]

def outer_non_nan(array):
    """
    determine first and last index which is non-nan
    Args:
        array: np.array
    Returns:
        smallest_index, largest_index
    """
    is_nan = np.isnan(array)
    indices_non_nan = np.nonzero(~is_nan)[0]
    return indices_non_nan[0], indices_non_nan[-1]
