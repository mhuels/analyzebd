import numpy as np
from copy import deepcopy
from analyzebd.functions.function import Function
from analyzebd.functions2D.function2D import Function2D
from analyzebd.helpers.arrays import *

def remove_duplicates(x_indices: np.ndarray, y_indices: np.ndarray, mode='lowest'):
    if mode == 'lowest':
        opt_func = np.min
    elif mode == 'highest':
        opt_func = np.max
    else:
        raise ValueError(f"Invalid mode: '{mode}'")
    new_x_indices, unique_ids = np.unique(x_indices, return_index=True)
    new_y_indices = y_indices[unique_ids]
    max_id = len(x_indices)
    nr_duplicates = np.diff(unique_ids, append=max_id) - 1
    for i, id in enumerate(unique_ids):
        if nr_duplicates[i] > 0:
            yvalues = y_indices[id:id+nr_duplicates[i]+1]
            optimal_value = opt_func(yvalues)
            new_y_indices[i] = optimal_value
    return new_x_indices, new_y_indices

def interpolate_contour_line(func2D: Function2D, x_indices: np.ndarray, y_indices: np.ndarray, z_target: float):
    """
    Interpolate the contour line in func2D along the line of (x_indices, y_indices) and the shifted line at one y_index
    higher.
    :param func2D: Function2D
    :param x_indices: np.ndarray
    :param y_indices: np.ndarray
    :param z_target: float
    :return: Function
    """
    f = Function(x=deepcopy(func2D.x), y=deepcopy(func2D.y))    # copy units, names, etc.
    # get rough estimate first (underestimation)
    f.x.setArray(func2D.x[x_indices])
    f.y.setArray(func2D.y[y_indices])
    # do linear regression for each x-value in func2D
    y_array = func2D.y.getArray()
    z_array = func2D.z.getArray()
    for k, j in enumerate(x_indices):
        i = y_indices[k]    #j = x_indices[k]
        y1 = y_array[i]
        y2 = y_array[i+1]
        dy = y2 - y1
        z1 = z_array[i, j]
        z2 = z_array[i+1, j]
        dz = z2 - z1
        f.y[k] = dy / dz * (z_target - z1) + y1
    return f

def extract_contour_line(func2D: Function2D, z_target: float, mode='lowest'):
    """
    Extracts a contour line of the given 2D-function f(x,y) as 1D-function y(x).
    :param func2D: Function2D
    :param z_target: float
    :param mode: 'lowest' or 'highest'
    :return: Function
    """
    # make sure, both x- and y-values are sorted
    if not is_sorted(func2D.x.getArray()):
        raise ValueError(f"object of type {type(func2D.x)} is not sorted! Can't build contour line.")
    if not is_sorted(func2D.y.getArray()):
        raise ValueError(f"object of type {type(func2D.y)} is not sorted! Can't build contour line.")
    z = func2D.z.getArray()
    sign_map = np.sign(z - z_target)   # +1 if value above contour_value, 0 if equal and -1 if below, nan if nan
    axis = 0    # TODO: add option reverse, to get x(y) instead (needs a lot of adjustments)
    sign_swap_map = np.diff(sign_map, axis=axis)
    # +2 if z_i        < z_target < z_i+1
    # +1 if z_i        < z_target = z_i+1      OR
    # +1 if z_i        = z_target < z_i+1
    #  0 if z_i, z_i+1 < z_target              OR
    #  0 if              z_target < z_i, z_i+1
    # -1 if z_i+1      < z_target = z_i        OR
    # -1 if z_i+1      = z_target < z_i
    # -2 if z_i+1      < z_target < z_i
    # nan if z_i = nan OR z_i+1 = nan
    positive_swaps = (sign_swap_map > 0.)
    negative_swaps = (sign_swap_map < 0.)
    # can't do sign_swap_map != 0, because it would count nans as well
    either_swaps = np.logical_or(positive_swaps, negative_swaps)
    y_indices, x_indices = np.where(either_swaps)
    # sort along x_indices
    index_map = np.argsort(x_indices)
    x_indices = x_indices[index_map]
    y_indices = y_indices[index_map]
    # remove duplicate entries of x_indices, picking the lowest/highest y-index
    x_indices, y_indices = remove_duplicates(x_indices, y_indices, mode=mode)
    f = interpolate_contour_line(func2D, x_indices, y_indices, z_target=z_target)
    return f