from .function import *
from analyzebd.quantities.velocity.velocity import *
from analyzebd.quantities.time import *

class VelocityTime(Function):
    def setup(self, **kwargs):
        if 'x' not in kwargs:
            kwargs['x'] = Time()
        if 'y' not in kwargs:
            kwargs['y'] = Velocity()
        super().setup(**kwargs)
        return self

    def readFromFile(self, folder, filename="velocities.out", **kwargs):
        self.y.readMetadata(folder)
        self.x.metadata.dt = self.y.metadata.dt
        self.x.metadata.period = self.y.metadata.period
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def generateLockedLimit(self, longtime=False):
        pi = math.pi
        d = 1 #particle diameter
        tB = d * d / self.y.metadata.D0   #tB = tau_Brownian = Brownian time
        tw = self.y.metadata.drivingPeriod   #tw = tau_omega = oscillation period
        tw_tilde = tw / tB   #scaled oscillation period
        kT = self.y.metadata.kT
        Fdr0 = self.y.metadata.drivingAmplitude
        Fdr0_tilde = Fdr0 / kT * d
        Fsub0 = self.y.metadata.substrateAmplitude
        Fsub0_tilde = Fsub0 / kT * d
        a = self.y.metadata.substratePeriod
        a_tilde = a / d
        tsub = 1 / (2 * pi) * a_tilde / Fsub0_tilde #tsub = tau_sub = substrate relaxation time
        tsub_tilde = tsub / tB  #scaled substrate relaxation time
        tt = 2 * pi * tsub / tw #inverse oscillation period scaled by substrate relaxation time
        t = self.x.array.copy()
        # correction in case of skipped steps
        if self.y.metadata.skipSteps is not None:
            t += self.y.metadata.skipSteps * self.x.metadata.dt

        vmax = Fdr0_tilde * d / tB * tt / math.sqrt(1 + tt * tt)
        phi = - pi / 2 + math.atan(a_tilde / (Fsub0_tilde * tw_tilde))
        self.y.array = vmax * np.cos(2 * pi / tw * t - phi)
        if not longtime:
            v2 = d / tB * Fdr0_tilde / (1 + tt * tt)
            correction = v2 * np.exp(-t / tsub)
            self.y.array += correction
        return self

    def plot(self, **kwargs):
        line = super().plot(**kwargs)
        self.ax.set_title("$\\tau_\\omega=$" f"{self.y.metadata.drivingPeriod}" "$\\tau_B,$" 
                          "$F_{dr,0}=$" f"{self.y.metadata.drivingAmplitude}" "$k_BTd^{-1}$")
        return line

    def plotFFT(self, **kwargs):
        line = super().plotFFT(**kwargs)
        self.ax.set_title("$\\tau_\\omega=$" f"{self.y.metadata.drivingPeriod}" "$\\tau_B,$"
                     "$F_{dr,0}=$" f"{self.y.metadata.drivingAmplitude}" "$k_BTd^{-1}$")
        return line
