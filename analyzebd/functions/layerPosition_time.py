from .function import *
from analyzebd.quantities.position.layerPosition import *
from analyzebd.quantities.time import *
from analyzebd.helpers.format import beautify_scientific

class LayerPositionTime(Function):
    def setup(self, **kwargs):
        if 'x' not in kwargs:
            kwargs['x'] = Time()
        if 'y' not in kwargs:
            kwargs['y'] = LayerPosition()
        super().setup(**kwargs)
        return self

    def readFromFile(self, folder, filename="layerPositions.out", **kwargs):
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def _set_title(self):
        if self.y.metadata is not None:
            self.ax.figure.suptitle(self.y.metadata.get_identifier(beautify=True))
        return self
