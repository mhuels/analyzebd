from .function import *
from analyzebd.quantities.angularBond import *
from analyzebd.quantities.time import *
from analyzebd.helpers.format import beautify_scientific

class AngularBondTime(Function):
    def setup(self, **kwargs):
        if 'x' not in kwargs:
            kwargs['x'] = Time()
        if 'y' not in kwargs:
            kwargs['y'] = AngularBond()
        super().setup(**kwargs)
        return self

    def readFromFile(self, folder, filename="angularBond.out", **kwargs):
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def plot(self, **kwargs):
        line = super().plot(**kwargs)
        return line

    def plotFFT(self, **kwargs):
        line = super().plotFFT(**kwargs)
        return line

    def plotLissajou(self, **kwargs):
        if 'x' not in kwargs:
            kwargs['x'] = self.y.slice((..., 0))
            kwargs['slice'] = (..., 1)
        # kwargs.setdefault("colormap", "viridis_r")
        line = super().plotLissajou(**kwargs)
        self.ax.set_xlim(0, 1)
        self.ax.set_ylim(0, 1)
        return line

    def _set_title(self):
        if self.y.metadata is not None:
            self.ax.figure.suptitle(self.y.metadata.get_identifier(beautify=True))
        return self
