from .function import *
from analyzebd.quantities.position.position import *
from analyzebd.quantities.time import *
import math

class PositionTime(Function):
    def setup(self, **kwargs):
        if 'x' not in kwargs:
            kwargs['x'] = Time()
        if 'y' not in kwargs:
            kwargs['y'] = Position()
        super().setup(**kwargs)
        return self

    def readFromFile(self, folder, filename="positions.out", **kwargs):
        super().readFromFile(folder=folder, filename=filename, **kwargs)
        return self

    def plot(self, **kwargs):
        line = super().plot(**kwargs)
        self.ax.set_title("$\\tau_\\omega=$" f"{self.y.metadata.drivingPeriod}" "$\\tau_B,$"
                          "$F_{dr,0}=$" f"{self.y.metadata.drivingAmplitude}" "$k_BTd^{-1}$")
        return line

    def plotFFT(self, **kwargs):
        line = super().plotFFT(**kwargs)
        self.ax.set_title("$\\tau_\\omega=$" f"{self.y.metadata.drivingPeriod}" "$\\tau_B,$"
                          "$F_{dr,0}=$" f"{self.y.metadata.drivingAmplitude}" "$k_BTd^{-1}$")
        return line

    def generateLockedLimit(self, longtime=False):
        pi = math.pi
        d = 1  # particle diameter
        tB = d * d / self.y.metadata.D0  # tB = tau_Brownian = Brownian time
        tw = self.y.metadata.drivingPeriod  # tw = tau_omega = oscillation period
        tw_tilde = tw / tB  # scaled oscillation period
        kT = self.y.metadata.kT
        Fdr0 = self.y.metadata.drivingAmplitude
        Fdr0_tilde = Fdr0 / kT * d
        Fsub0 = self.y.metadata.substrateAmplitude
        Fsub0_tilde = Fsub0 / kT * d
        a = self.y.metadata.substratePeriod
        a_tilde = a / d
        tsub = 1 / (2 * pi) * a_tilde / Fsub0_tilde  # tsub = tau_sub = substrate relaxation time
        tsub_tilde = tsub / tB  # scaled substrate relaxation time
        tt = 2 * pi * tsub / tw  # inverse oscillation period scaled by substrate relaxation time
        t = self.x.array.copy()
        # correction in case of skipped steps
        if self.y.metadata.skipSteps is not None:
            t += self.y.metadata.skipSteps * self.x.metadata.dt

        xmax = a / (2 * pi) * Fdr0 / Fsub0 / math.sqrt(1 + tt * tt)
        phi = - pi / 2 + math.atan(a_tilde / (Fsub0_tilde * tw_tilde))
        self.y.array = xmax * np.sin(2 * pi / tw * t - phi)
        if not longtime:
            x2 = d * tsub_tilde * Fdr0_tilde / (1 + tt * tt)
            correction = - x2 * np.exp(-t / tsub)
            self.y.array += correction
        return self

    def generateRunningLimit(self):
        pi = math.pi
        d = 1  # particle diameter
        tB = d * d / self.y.metadata.D0  # tB = tau_Brownian = Brownian time
        tw = self.y.metadata.drivingPeriod  # tw = tau_omega = oscillation period
        w = 2 * pi / tw  # angular frequency
        w_tilde = w * tB  # scaled angular frequency
        kT = self.y.metadata.kT
        Fdr0 = self.y.metadata.drivingAmplitude
        Fdr0_tilde = Fdr0 / kT * d
        t = self.x.array.copy()
        # correction in case of skipped steps
        if self.y.metadata.skipSteps is not None:
            t += self.y.metadata.skipSteps * self.x.metadata.dt
        self.y.array = Fdr0_tilde * d / w * np.sin(w * t)
        return self
