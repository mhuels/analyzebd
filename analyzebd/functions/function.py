from pathlib import Path
import numpy as np
from scipy.fft import fftfreq
from analyzebd.plt import *
from analyzebd.colors import IntracolorPlot
from copy import deepcopy
from matplotlib.colors import Normalize, LogNorm
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from analyzebd.helpers.clock import Clock
from tqdm import tqdm
from analyzebd.quantities.quantity import Quantity
from analyzebd.helpers.arrays import window_intervals
from analyzebd.helpers.copy import copy_inplace

# DEFAULTS
DEFAULT_COLORMAP = "winter"

class Function:
    def __init__(self, **kwargs):
        self.reset()
        self.setup(**kwargs)

    def reset(self):
        self.x = None
        self.y = None
        self.metadata = None
        self.src = None
        self.ax = None
        self.lc = None
        self.cnorm = None
        self.setColormap(DEFAULT_COLORMAP)
        self.cbar = None
        return self

    def setup(self, x: Quantity=None, y: Quantity=None, metadata=None):
        if x is None:
            x = Quantity(name="x", name_latex="x")
        if y is None:
            y = Quantity(name="y", name_latex="y")
        self.x = x
        self.y = y
        self.metadata = metadata
        return self

    def __deepcopy__(self, memodict=None):
        if memodict is None:
            memodict = {}
        new = type(self).__new__(type(self))  # self can be a child this way (no __init__ called)
        memodict[id(self)] = new
        new.x = deepcopy(self.x, memodict)
        new.y = deepcopy(self.y, memodict)
        new.src = self.src
        # should not be deepcopied
        new.ax = self.ax
        new.lc = self.lc
        new.cnorm = self.cnorm
        new._colormap = self._colormap
        new._cmap = self._cmap
        new.cbar = self.cbar
        return new

    def readFromFile(self, folder, filename, xStart=None, xEnd=None, xUnit=None, dry=False, verbose=False,
                     **kwargs):
        self.src = Path(folder) / filename
        try:
            self.x.readMetadata(folder)
        except Exception:
            self.y.readMetadata(folder)
            self.x.metadata = self.y.metadata
        if dry: return self
        kwargs_x = kwargs.copy()
        # kwargs_x.pop('columns', None)
        kwargs_x['numberOfEnsembles'] = 1  # x-quantity is the same for all ensembles
        kwargs_x['columns'] = 0     # TODO: check if this breaks anything
        #reading x is usually less time consuming (only one ensemble, only one column)
        self.x.readFromFile(folder=folder, filename=filename, **kwargs_x)
        if xUnit is not None:
            self.x.setScale(xUnit)
        if xStart is not None or xEnd is not None:
            self.x.crop(start=xStart, end=xEnd, inPlace=True)
        kwargs['iStart'] = self.x.iStart
        kwargs['iEnd'] = self.x.iEnd
        self.y.readFromFile(folder=folder, filename=filename, **kwargs)
        if verbose:
            print(f"Read function {self.y.name}({self.x.name}) from {filename}.")
        return self

    def read_suitable(self, filename, folder='.', reset=False, inPlace=False, verbose=False):
        """
        :param filename: str or Path
        :param folder: str or Path
        :param reset: bool
        :param inPlace: bool
        :param verbose: bool
        :return: self of deepcopy of self
        """
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        f_in = deepcopy(obj)
        if reset is False:
            try:
                f_in.readFromFile(folder=folder, filename=filename)
                # TODO: this fails because readFromFile and writeToFile() are not working together
            except FileNotFoundError:
                f_in = None
        # check if data was read successfully
        if f_in is None or len(f_in) == 0:
            reset = True
        # check if x-values are the required ones
        elif len(obj.x) != len(f_in.x) \
                or not np.isclose(obj.x.getArray(), f_in.x.getArray()).min():
            reset = True
        if reset is True:
            # discard if not the same
            if verbose:
                print("Data set is not suitable. Reading data files again...")
            obj.y.setArray(np.full((len(obj.x),), np.nan))
        else:
            obj.y.setArray(f_in.y.getArray())
        return obj

    def writeToFile(self, filename, verbose=False, **kwargs):
        clock = Clock()
        # create merged numpy array of x and y
        if len(self.y.array.shape) > 1:
            raise NotImplementedError(
                f"y-array has shape {self.y.array.shape}, for which file-writing is not implemented yet.")
        stacked = np.stack([self.x.getArray(), self.y.getArray()], axis=-1)
        if "delimiter" not in kwargs:
            kwargs["delimiter"] = "\t"
        if "header" not in kwargs:
            header = f"x: {self.x.name} [{self.x.unit}] \n"
            header += f"y: {self.y.name} [{self.y.unit}] \n"
            header += f"{'x':>10s}{kwargs['delimiter']}{'y':>12s}"
            kwargs["header"] = header
        if "fmt" not in kwargs:
            kwargs["fmt"] = "%e"
        np.savetxt(filename, X=stacked, **kwargs)
        clock.lap()
        if verbose:
            print(f"Saved function {self.y.name}({self.x.name}) in {filename} ... {clock.getDuration(0, -1)}")
        return self

    def __str__(self):
        concat = np.column_stack([self.x[:], self.y[:]])
        return str(concat)

    def __len__(self):
        if len(self.x) != len(self.y):
            raise ValueError(f"{self.x.name} and {self.y.name} have different lengths!")
        return len(self.y)

    def __getitem__(self, key):
        return self.y[key]

    def __setitem__(self, key, value):
        self.y[key] = value
        return self

    def __add__(self, other):
        if not isinstance(other, Function):
            raise TypeError(f"'+' or '-' operator is not defined for {type(other)} and {type(self)}")
        elif other.x.array.shape != self.x.array.shape:
            raise TypeError(f"you are trying to add two Functions with different x-shapes! ({other.x.array.shape} != {self.x.array.shape})")
        elif other.y.array.shape != self.y.array.shape:
            raise TypeError(f"you are trying to add two Functions with different y-shapes! ({other.y.array.shape} != {self.y.array.shape})")
        result = deepcopy(self)
        result.y += other.y
        return result

    def __neg__(self):
        result = deepcopy(self)
        result.y = -result.y
        return result

    def __sub__(self, other):
        return self + (-other)

    def __mul__(self, other):
        if not isinstance(other, Function):
            raise TypeError(f"'*' operator is not defined for {type(other)} and {type(self)}")
        elif other.x.array.shape != self.x.array.shape:
            raise TypeError(f"you are trying to multiply two Functions with different x-shapes! ({other.x.array.shape} != {self.x.array.shape})")
        elif other.y.array.shape != self.y.array.shape:
            raise TypeError(f"you are trying to multiply two Functions with different y-shapes! ({other.y.array.shape} != {self.y.array.shape})")
        result = deepcopy(self)
        result.y *= other.y
        return result

    def __truediv__(self, other):
        if not isinstance(other, Function):
            raise TypeError(f"'/' operator is not defined for {type(other)} and {type(self)}")
        elif other.x.array.shape != self.x.array.shape:
            raise TypeError(f"you are trying to divide two Functions with different x-shapes! ({other.x.array.shape} != {self.x.array.shape})")
        elif other.y.array.shape != self.y.array.shape:
            raise TypeError(f"you are trying to divide two Functions with different y-shapes! ({other.y.array.shape} != {self.y.array.shape})")
        result = deepcopy(self)
        result.y /= other.y
        return result

    def __call__(self, x: float):
        """
        Return the function value(s) f(x).
        :param x: float
        :return: float or np.array
        """
        i = self.x(v=x)
        axis = find_suiting_axis(self)
        value = np.take(self.y.getArray(), i, axis=axis)
        return value    #TODO: return quantity instead

    def crop(self, xStart: float=None, xEnd: float=None, inPlace=False):
        # check that x is one-dimensional
        if len(self.x.getArray().shape) > 1:
            raise IndexError(f"self.x has shape {self.x.getArray().shape}. It must be one-dimensional to crop.")
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        obj.x.crop(start=xStart, end=xEnd, inPlace=True)
        obj.y.slice(start=obj.x.iStart, stop=obj.x.iEnd, inPlace=True)
        return obj

    def rcrop(self, xDiff: float=None, inPlace=False):
        # check that x is one-dimensional
        if len(self.x.getArray().shape) > 1:
            raise IndexError(f"self.x has shape {self.x.getArray().shape}. It must be one-dimensional to crop.")
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        obj.x.rcrop(diff=xDiff, inPlace=True)
        obj.y.slice(start=obj.x.iStart, stop=obj.x.iEnd, inPlace=True)
        return obj

    def slice(self, slc=None, inPlace=False, **kwargs):
        """
        Slice y-Quantity. Use crop() or rcrop() in case of slicing along the x-Quantity!
        Args:
            inPlace:
            **kwargs:

        Returns:

        """
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        # obj.x.slice(inPlace=True, **kwargs)   # this is only needed for crop()-functionality
        obj.y.slice(slc=slc, inPlace=True, **kwargs)
        return obj

    def slice_ensembles(self, stop, start=None, step=None, inPlace=False):
        obj = copy_inplace(self, inPlace)
        obj.y.slice_ensembles(start=start, stop=stop, step=step, inPlace=True)
        return obj

    def reduceSize(self, xUnit="tw", maxLength=1000):
        prev_xUnit = self.x.unit
        self.x.setScale(xUnit)
        numberOfPeriods = round((max(self.x) - min(self.x)))
        lenPerPeriod = len(self) / numberOfPeriods
        if lenPerPeriod > maxLength:
            factor = round(lenPerPeriod / maxLength)
            self.slice(factor)
        self.x.setScale(prev_xUnit)
        return self

    def build_periodic_avg(self, period=1, inPlace=False):
        """
        Build average over a period along the x-axis.
        :param period: float
        :param inPlace: bool
        :return: Function
        """
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        # N = len(obj.x)
        numberOfPeriods = int((max(obj.x) - min(obj.x)) / period)
        if numberOfPeriods == 0:
            return obj
        x = obj.x.getArray()
        w = window_intervals(x, period)     # n can be chosen automatically because we require an integer
                                            # number of periods
        pointsPerPeriod = w[0]  # problematic if not always the same
        y = obj.y.getArray()
        axis = find_suiting_axis(obj)  # determine position of x_dim in y-Quantity
        new_x = x[:pointsPerPeriod]     # note: it's intended to leave the x-values as they are
        old_shape = y.shape
        new_shape = old_shape[:axis] + (pointsPerPeriod,) + old_shape[axis + 1:]
        new_y = np.full(new_shape, np.nan)
        ### slow for-loop
        # for i in range(pointsPerPeriod):
        #     sum = 0
        #     index = i
        #     for j in range(numberOfPeriods):
        #         source_slice = [slice(None)] * len(old_shape)
        #         source_slice[axis] = index
        #         source_slice = tuple(source_slice)
        #         sum += y[source_slice]
        #         index += w[index]
        #     target_slice = [slice(None)] * len(new_shape)
        #     target_slice[axis] = i
        #     target_slice = tuple(target_slice)
        #     new_y[target_slice] = sum / numberOfPeriods
        ### vectorized version of the above for-loop
        sum = 0     # gets expanded into the suiting shape later
        index = np.arange(pointsPerPeriod)
        for j in range(numberOfPeriods):    # maybe even vectorize this loop?
            source_slice = [slice(None)] * len(old_shape)
            source_slice[axis] = index
            source_slice = tuple(source_slice)
            sum += y[source_slice]
            index += w[index]
        target_slice = [slice(None)] * len(new_shape)
        target_slice[axis] = np.arange(pointsPerPeriod)
        target_slice = tuple(target_slice)
        new_y[target_slice] = sum / numberOfPeriods

        # finalize
        obj.x.setArray(new_x)
        obj.y.setArray(new_y)
        # naming
        obj.x.name_latex.setModulo(period)
        obj.y.name_latex.prependEnclosing(("\\left<", "\\right>_{{\\tau_\\omega}}"))
        return obj

    def getFFT(self, kind="raw"):
        N = len(self)
        dx = self.x[1] - self.x[0]
        xf = fftfreq(N, dx)
        yf = self.y.getFFT(kind=kind)
        if (N % 2 == 0):  # even
            delim = N // 2
        else:  # uneven
            delim = (N + 1) // 2
        if(kind == "raw"):
            # order from - to +
            xf = np.concatenate((xf[delim:], xf[0:delim]))
        else:
            xf = xf[0:delim]
        return xf, yf

    def filterNoise(self, inPlace=False, **kwargs):
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        obj.y.filterNoise(inPlace=True, **kwargs)
        return obj

    def plot(self, slice=None, ax=None, filter=None, colors=None, set_lims=False, scatter=False, **kwargs):
        """
        Plot y(x).
        Args:
            slice: tuple
            ax: axes
            filter:
            colors: Quantity
            set_lims: bool
            **kwargs: forwarded to ax.plot()

        Returns:

        """
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            # ax = plt.subplot()
        self.ax = ax
        x = self.x
        y = self.y
        if set_lims:
            self.ax.set_xlim(x.get_lim())
            self.ax.set_ylim(y.get_lim())
        if filter is not None:
            y = self.filterNoise(filter=filter).y
        if slice is not None:
            y = y.slice(slice)
        if scatter:
            kwargs.setdefault("edgecolors", "none")
        if colors is not None:
            line = self._plotWithIntraColor(x, y, c=colors, scatter=scatter, **kwargs)
        else:
            if scatter:
                line = ax.scatter(x, y, **kwargs)
            else:
                line = ax.plot(x, y, **kwargs)
        ax.set_xlabel(self.x.getLatexLabel())
        ax.set_ylabel(self.y.getLatexLabel())
        self._set_title()
        return line

    def plotAverageWithError(self, ax=None, asymmetric=False, with_label=False, alpha_outer=None, **kwargs):
        if self.y.numberOfEnsembles == 1:
            return self.plot(ax=ax, **kwargs)
        if ax is None:
            ax = plt.subplot()
        self.ax = ax
        x = self.x
        y_avg = self.y.buildEnsembleAverage()
        if asymmetric:
            y_std_lower = self.y.buildEnsembleStd(side="lower")
            y_std_upper = self.y.buildEnsembleStd(side="upper")
        else:
            y_std_lower = self.y.buildEnsembleStd()
            y_std_upper = y_std_lower
        y_lower = y_avg - y_std_lower
        y_upper = y_avg + y_std_upper
        kwargs.setdefault("alpha", 1.)
        kwargs_outer = {}
        if alpha_outer is None:
            alpha_outer = kwargs["alpha"] / 3
        kwargs_outer["alpha"] = alpha_outer
        self.ax.set_xlim(x.get_lim())
        if with_label:
            kwargs.setdefault("label", y_avg.name_latex.getLabel())
        line_avg, = self.ax.plot(x, y_avg, **kwargs)
        kwargs_outer["color"] = line_avg.get_color()
        # line_upper, = self.ax.plot(x, y_upper, **kwargs_outer)
        # line_lower, = self.ax.plot(x, y_lower, **kwargs_outer)
        poly_between = self.ax.fill_between(x, y_lower, y_upper, **kwargs_outer)
        ax.set_xlabel(self.x.getLatexLabel())
        ax.set_ylabel(self.y.getLatexLabel())
        # return line_avg, line_lower, line_upper, poly_between
        self._set_title()
        return line_avg, poly_between

    def _plotWithIntraColor(self, x: Quantity, y: Quantity, c: Quantity, cperiod=None, scatter=False,
                            colormap=None, cnorm=None, withColorBar=False, kw_colorbar=None,
                            **kwargs):
        """
        Plot line segments with color information.
        Args:
            x (Quantity): y-values
            y (Quantity): x-values
            c (Quantity): color information
            cperiod (float or None): period of c
            scatter (bool): use ax.scatter() instead of ax.plot()
            colormap (str): colormap identifier
            cnorm: color normalization
            withColorBar (bool): add colorbar to the axis
            kw_colorbar (dict): kwargs for colorbar call
            **kwargs: ax.plot() arguments
        Returns:
            line (LineCollection if scatter=False, else PathCollection)
        """
        ax = self.ax
        if cperiod is not None:
            c = c.map_periodic(cperiod)
        if colormap is not None:
            self.setColormap(colormap)
        if cperiod is not None and self._colormap == DEFAULT_COLORMAP:
            self.setColormap("twilight")
        if cnorm is not None:
            self.cnorm = cnorm
        if self.cnorm is None:
            if cperiod is not None:
                self.cnorm = Normalize(0, cperiod)
            else:
                self.cnorm = Normalize(*c.get_lim())
        if scatter:
            kwargs.setdefault("s", 10)
            line = ax.scatter(x=x, y=y, c=c, cmap=self._colormap, norm=self.cnorm, **kwargs)
        else:
            self.lc = IntracolorPlot(colormap=self._colormap, norm=self.cnorm)
            self.lc.setup(x=x, y=y, c=c)
            self.lc.get_lc().set_linewidth(1 if "linewidth" not in kwargs.keys() else kwargs["linewidth"])
            self.lc.get_lc().set_alpha(1 if "alpha" not in kwargs.keys() else kwargs["alpha"])
            line = ax.add_collection(self.lc.get_lc())
        if withColorBar:
            if kw_colorbar is None:
                kw_colorbar = {}
            self.cbar = self.addColorBar(ax, label=c.getLatexLabel(), **kw_colorbar)
        ax.set_xlim(x.get_lim())
        ax.set_ylim(y.get_lim(buffer=0.1))
        return line

    def show_plot(self):
        plt.show()
        return True

    def plotFFT(self, slice=None, ax=None, plot_func="plot", kind="abs", imag=False, **kwargs):
        if (ax is None):
            ax = plt.subplot()
        self.ax = ax
        xf, yf = self.getFFT(kind=kind)
        if (imag):
            yf = np.imag(yf)
        else:
            yf = np.real(yf)
        if slice is not None:
            yf = yf[slice]
        if plot_func == "fill_between":
            line = ax.fill_between(xf, yf, **kwargs)
        elif plot_func == "plot":
            line, = ax.plot(xf, yf, **kwargs)
        else:
            raise ValueError(f"plot_func={plot_func} not supported!")
        xName = self.x.name_latex_fft
        if xName is None:
            xName = f"\\xi_{{{self.x.name_latex}}}"
        xUnit = self.x.unit_latex_fft
        if xUnit is None:
            xUnit = f"{self.x.unit_latex}^{{-1}}"
        self.ax.set_xlabel(f"${xName}/{xUnit}$")
        self.ax.set_ylabel(self.y.getLatexLabel(fft=True, fft_kind=kind))
        self._set_title()
        return line

    def plotDistribution(self, ax=None, bins=10, withColorBar=False, colorbar_kwargs=None, **kwargs):
        if (ax is None):
            ax = plt.subplot()
        self.ax = ax
        x = self.x
        if self.y.numberOfEnsembles == 1:
            raise ValueError(f"Not enough ensembles to plot a distribution.")
        kwargs_build = {}
        if "limits" in kwargs:
            kwargs_build["limits"] = kwargs.pop("limits")
        psi, y = self.y.buildDistribution(bins=bins, **kwargs_build)
        if "norm" not in kwargs or kwargs["norm"] is None:
            psi_max = np.percentile(psi, 99)   #99th percentile
            kwargs["norm"] = Normalize(0, psi_max)
        self.cnorm = kwargs["norm"]
        kwargs.setdefault("cmap", "binary")
        self.setColormap(kwargs["cmap"])  # pcolormesh cmap is colormap in my definition
        kwargs.setdefault("shading", "auto")
        self.shading = kwargs["shading"]
        mesh = ax.pcolormesh(x, y, psi, **kwargs)
        if withColorBar:
            if colorbar_kwargs is None: colorbar_kwargs = {}
            colorbar_kwargs.setdefault("label", f"$W({self.y.name_latex.getLabel(with_dollars=False)})/{self.y.unit.invert().getLatex()}$")
            self.addColorBar(**colorbar_kwargs)
        ax.set_xlabel(self.x.getLatexLabel())
        ax.set_ylabel(self.y.getLatexLabel())
        self._set_title()
        return mesh

    def plotLissajou(self, x: Quantity=None, ax=None, slice=None, colors=None, with_label=False, scatter=True, **kwargs):
        """
        Plot a parametric Lissajou curve of f.y (needs at least two
        Args:
            x: Quantity
            ax: axes
            slice: tuple
            colors: Quantity
            with_label: bool
            **kwargs:
        Returns:
            line
        """
        if x is None:
            raise ValueError(f"x=None is not valid.")
        if ax is None:
            ax = plt.subplot()
        self.ax = ax
        if slice is not None:
            y = self.y.slice(slice)
        else:
            y = self.y
        if with_label:
            parameter_label = self.x.name_latex     # TODO: check if x and y have same parameters
            kwargs.setdefault('label', f"[{x.getLatexLabel()}({parameter_label}), {y.getLatexLabel()}({parameter_label})]")
        if colors is not None:
            line = self._plotWithIntraColor(x, y, c=colors, scatter=scatter, **kwargs)
        else:
            if scatter:
                line = ax.scatter(x, y, **kwargs)
            else:
                line = ax.plot(x, y, **kwargs)
        ax.set_xlabel(x.getLatexLabel())
        ax.set_ylabel(y.getLatexLabel())
        self._set_title()
        return line

    def addColorBar(self, ax=None, pad=0.05, label=None, labelPos='y', labelpad=None):
        if ax is None:
            ax = self.ax
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=pad)
        self.cbar = plt.colorbar(cm.ScalarMappable(norm=self.cnorm, cmap=self._cmap), ax=ax, cax=cax)
        if label is not None:
            if labelPos == 'x':
                if labelpad is None: labelpad = 25
                self.cbar.ax.set_xlabel(label, labelpad=labelpad)     # TODO: choose labelpad smarter
                # TODO: labelPos='x' not working at the moment
            elif labelPos == 'y':
                if labelpad is None: labelpad = 5
                self.cbar.ax.set_ylabel(label, labelpad=labelpad)
            else:
                raise ValueError(f"labelPos={labelPos} not supported. Choose either 'x' (default) or 'y'")
        return self.cbar

    def get_cmap(self):
        return self._cmap

    def setColormap(self, colormap):
        # if colormap is None:
        #     self._colormap = "winter"
        # else:
        self._colormap = colormap
        self._cmap = cm.get_cmap(self._colormap)
        return self._cmap

    def clonePeriod(self, forward=1, backward=0, inPlace=False):
        if len(self.y.array.shape) > 1:
            raise NotImplementedError(f"Not implemented for shape={self.y.array.shape} yet.")
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        N = len(obj)
        numberOfCopies = 1 + forward + backward  # including itself
        xarray = obj.x.array
        yarray = obj.y.array
        obj.x.array = np.zeros(numberOfCopies * N)
        obj.y.array = np.zeros(numberOfCopies * N)

        dx = xarray[1] - xarray[0]
        xrange = xarray.max() - xarray.min() + dx  # extrapolate one point further

        counter = 0
        for i in range(-backward, forward + 1):
            i0 = counter * N
            iEnd = i0 + N
            obj.x.array[i0:iEnd] = xarray + i * xrange
            obj.y.array[i0:iEnd] = yarray
            counter += 1
        return obj

    def prependConstant(self, value: float, xStart: float, inPlace=False):
        if len(self.y.array.shape) > 2:
            raise NotImplementedError(f"Not implemented for shape={self.y.array.shape} yet.")
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        # from analyzebd.helpers.arrays import is_monotonously_increasing
        len_before = len(obj.x)
        obj.x.extrapolate(start=xStart, inPlace=True)
        len_after = len(obj.x)
        # TODO: consistency check
        # duplicate for each ensemble
        prepend = np.repeat(value[..., np.newaxis], len_after - len_before, axis=-1)  # works for floats as well as arrays
        obj.y.setArray(np.concatenate((prepend, obj.y.getArray()), axis=1))     # insert in the front
        return obj

    def getWindowedAmplitude(self, x: float, period=1., mode="retarded"):
        """
        Get amplitude of a function f(x) in the interval [x, x+period].
        amplitude = (f_max - f_min) / 2
        The y-value of function needs to be one-dimensional (f.y.array.shape=(N,)).
        :param x: Start of considered interval.
        :param period: length of the considered interval
        :param mode: str ('retarded', 'precocious', 'centered')
        :return: float
        """
        if mode == "retarded":  # forward
            xStart = x
            xEnd = x + period
            f_in = self
        elif mode == "precocious":  # backward
            xStart = x - period
            xEnd = x
            f_in = self.prependConstant(self.y[0], self.x.min() - period)
        elif mode == "centered":
            xStart = x - period / 2
            xEnd = x + period / 2
            f_in = self.prependConstant(self.y[0], self.x.min() - period / 2)
        else:
            raise ValueError(f"mode={mode} is not available. Choose between 'retarded', 'precocious' and 'centered'.")
        f_windowed = f_in.crop(xStart=xStart, xEnd=xEnd)
        if f_windowed.y.numberOfEnsembles > 1:
            axis = 1
        else:
            axis = None
        if f_windowed.y.array.shape[-1] < 1:
            raise TypeError(f"No values available in interval x=[{xStart}, {xEnd}].")
        vmin = f_windowed.y.min(axis=axis)
        vmax = f_windowed.y.max(axis=axis)
        amplitude = (vmax - vmin) / 2
        return amplitude

    def getWindowedAmplitudes(self, period: float = 1., axis=None, mode="retarded"):
        """
        Get amplitudes of a function f(x) in the interval [x, x+period] for all available x-values [x_min, x_max-period].
        amplitude = (f_max - f_min) / 2
        The y-value of function needs to be one-dimensional (f.y.array.shape=(N,)).
        :param period: length of the considered interval
        :param mode: str ('retarded', 'precocious', 'centered')
        :return: Function
        """
        if axis is None:
            axis = find_suiting_axis(self)
        xStart = self.x.min()
        if mode == "retarded":
            xEnd = self.x.max() - period
            f_in = self
        else:
            if self.y.numberOfEnsembles == 1:
                constant = self.y[0]
            else:
                constant = self.y[:, 0]
            if mode == "precocious":
                xEnd = self.x.max()
                f_in = self.prependConstant(constant, self.x.min() - period)
            elif mode == "centered":
                xEnd = self.x.max() - period / 2
                f_in = self.prependConstant(constant, self.x.min() - period / 2)
            else:
                raise ValueError(f"mode={mode} is not available. Choose between 'retarded', 'precocious' and 'centered'.")
        amplitudes = f_in.crop(xStart, xEnd)    #crop suiting to chosen mode
        n = len(amplitudes.x)
        # N = len(f_in.x)   # N > n
        amplitudes.y.array.fill(0)
        x = f_in.x.getArray()
        w = window_intervals(x, period, n=n)    # window length w[i] such that x[i+w[i]] >= x[i] + period
        # n can be a bit larger than the default len(w) here because we are not strictly bound to an integer
        # number of periods
        y = f_in.y.getArray()
        for i in range(n):
            source_slice = [slice(None)] * len(y.shape)
            source_slice[axis] = slice(i, i + w[i])
            source_slice = tuple(source_slice)
            y_window = y[source_slice]
            a_window = (y_window.max(axis=axis) - y_window.min(axis=axis)) / 2
            target_slice = [slice(None)] * len(y.shape)
            target_slice[axis] = i
            target_slice = tuple(target_slice)
            amplitudes.y[target_slice] = a_window
        # naming
        amplitudes.y.name += " amplitude"
        amplitudes.y.name_latex.prependEnclosing(("A \\left[", "\\right]"))
        return amplitudes

    def buildEnsembleAverage(self, inPlace=False):
        if inPlace:
            obj = self
        else:
            obj = deepcopy(self)
        obj.y.buildEnsembleAverage(inPlace=True)
        return obj

    def getAveragePlusError(self):
        """
        Get ensemble average and error range (+/- standard deviation).
        :return: f_avg, f_lower, f_upper
        """
        f = self
        f_avg = deepcopy(f)
        f_avg.y.buildEnsembleAverage(inPlace=True)
        f_std = deepcopy(f)
        f_std.y.buildEnsembleStd(inPlace=True)
        f_lower = f_avg - f_std
        f_upper = f_avg + f_std
        return f_avg, f_lower, f_upper

    def _set_title(self):
        if self.ax is not None:
            # self.ax.figure.suptitle("")
            pass
        return self

def find_suiting_axis(f: Function):
    """
    Find the axis number of f.y that corresponds to the 1-D f.x.
    :param f: Function
    :return: axis (int)
    """
    xshape = f.x.array.shape
    if len(xshape) > 1:
        raise ValueError(f"Only 1-dimensional x-quantities are allowed (x.array.shape={x.array.shape})")
    xlen = xshape[0]
    yshape = f.y.array.shape
    axis = None
    for i, ylen in enumerate(yshape):
        if xlen == ylen:
            if axis is None:
                axis = i
            else:
                raise IndexError(f"It is not clear, which is the suiting axis, since they are both equally large.\n"
                                          f"xshape={xshape}, yshape={yshape}")
    return axis
