class FrequencyFilter:
    def __init__(self, threshold=None, type=None):
        self.type = type
        self.threshold = threshold
        if self.type is None:
            self.type = "avg"
        if threshold is None:
            if self.type == "abs":
                raise ValueError(f"for type={type}, threshold needs to be given")
            elif self.type == "rel":
                self.threshold = 0.2
            elif self.type == "avg":
                self.threshold = 10
