from .state_diagram import addargs_sd_data_collection
from .state_diagram import addargs_sd_plot
from .state_diagram import addargs_units

from .classification import addargs_struc_classification

from .settling_time import addargs_settling_time

from .plt import addargs_layout
