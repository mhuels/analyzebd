from argparse import ArgumentParser
from copy import deepcopy
from analyzebd.helpers.modes import print_supported_modes

def addargs_ranges(parser: ArgumentParser=None, inPlace=True):
    if parser is None:
        new_parser = ArgumentParser("Adjust ranges of x and y")
    else:
        if inPlace:
            new_parser = parser
        else:
            new_parser = deepcopy(parser)
    new_parser.add_argument("--xmin", type=float, help="lower limit of x")
    new_parser.add_argument("--xmax", type=float, help="upper limit of x")
    new_parser.add_argument("--xcut", type=int, help="maximally use n xvalues")
    new_parser.add_argument("--ymin", type=float, help="lower limit of y")
    new_parser.add_argument("--ymax", type=float, help="upper limit of y")
    new_parser.add_argument("--ycut", type=int, help="maximally use n yvalues")
    return new_parser

def addargs_sd_data_collection(parser: ArgumentParser=None, inPlace=True):
    if parser is None:
        new_parser = ArgumentParser("Collect state diagram data")
    else:
        if inPlace:
            new_parser = parser
        else:
            new_parser = deepcopy(parser)
    addargs_ranges(new_parser)
    new_parser.add_argument("--workdir", type=str, help="data location")
    new_parser.add_argument("--dry", "-n", action='store_true', help="make a dry run (without reading data)")
    new_parser.add_argument("--reset", action='store_true', help="retrieve z-data anew")
    new_parser.add_argument("--debug", action='store_true', help="make a test run for a very limited set of data")
    new_parser.add_argument("--numberOfEnsembles", "-e", type=int, help="maximum number of ensembles")
    new_parser.add_argument("--numberOfPeriods", "-N", type=float, help="maximum number of periods")
    new_parser.add_argument("--outname", "-o", type=str, help="path (and name) of output file")
    new_parser.add_argument("--skip_first", type=int, help="skip reading the first x files (useful for debugging purposes)")
    return new_parser

def addargs_sd_plot(parser: ArgumentParser=None, supported_modes=None, inPlace=True):
    if parser is None:
        new_parser = ArgumentParser("Plot state diagram")
    else:
        if inPlace:
            new_parser = parser
        else:
            new_parser = deepcopy(parser)
    if supported_modes is None:
        supported_modes = ["heatmap"]
    new_parser.add_argument("--modes", "-m", type=str, nargs='+', default=["all"],
                        help=f"plots to be considered, default='all'\n{print_supported_modes(supported_modes)}")
    new_parser.add_argument("--as_periods", action='store_true', help="show x-axis as periods instead of frequency")
    new_parser.add_argument("--no_interpolation", action='store_true', help="don't interpolate missing values")
    addargs_ranges(new_parser)
    new_parser.add_argument("--xscale", type=str, default='log', help="xscale")
    new_parser.add_argument("--yscale", type=str, default='log', help="yscale")
    new_parser.add_argument("--zscale", type=str, default='log', help="zscale")
    new_parser.add_argument("--plain", action='store_true', help="don't show annotates")
    new_parser.add_argument("--noshow", action='store_true', help="don't show interactive panels")
    new_parser.add_argument("--save", type=str, nargs='?', const='auto', help="save plot (filename optional)")
    new_parser.add_argument("--formats", type=str, nargs='+', default=[".png", ".pdf"], help="save file formats (e.g. '.pdf')")
    new_parser.add_argument("--colormap_heatmap", type=str, default="bwr", help="colormap of heatmap")
    new_parser.add_argument("--colormap_xslices", type=str, default="summer", help="colormap of xslices")
    new_parser.add_argument("--colormap_yslices", type=str, default="cool", help="colormap of yslices")
    new_parser.add_argument("--xlabel", type=str, help="axis label of x-values")
    new_parser.add_argument("--ylabel", type=str, help="axis label of y-values")
    new_parser.add_argument("--zlabel", type=str, help="axis label of z-values")
    return new_parser

def addargs_units(parser: ArgumentParser=None, zunits=None, inPlace=True):
    if parser is None:
        new_parser = ArgumentParser("Units")
    else:
        if inPlace:
            new_parser = parser
        else:
            new_parser = deepcopy(parser)
    if zunits is None:
        zunits = ["a"]
    new_parser.add_argument("--zunit", type=str, default="a", help=f"set unit of z-axis; options: {zunits}")
    return new_parser
