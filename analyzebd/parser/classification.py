from argparse import ArgumentParser
from copy import deepcopy
from analyzebd.classification import Structure_Classifier

def addargs_struc_classification(parser: ArgumentParser, new_group=False, inPlace=True):
    if parser is None:
        new_parser = ArgumentParser("Structure state classification")
    else:
        if inPlace:
            new_parser = parser
        else:
            new_parser = deepcopy(parser)
    if new_group:
        new_group = new_parser.add_argument_group("Structure classification options")
        addto = new_group
    else:
        addto = new_parser
    addto.add_argument("--thr4", type=float, default=Structure_Classifier.THR4_DEFAULT, help="structure classification: psi4 threshold")
    addto.add_argument("--thr6", type=float, default=Structure_Classifier.THR6_DEFAULT, help="structure classification: psi6 threshold")
    addto.add_argument("--Pmin", type=float, default=0.01, help="minimal participation to count towards mixed state")
    addto.add_argument("--period_avg", action='store_true', help="build the period-average before classifying")
    addto.add_argument("--ens_avg", action='store_true', help="build the ensemble-average before classifying")
    if new_group:
        return new_parser, new_group
    else:
        return new_parser
