from argparse import ArgumentParser
from copy import deepcopy
from analyzebd.plt import plt_global

def addargs_layout(parser: ArgumentParser=None, inPlace=True):
    if parser is None:
        new_parser = ArgumentParser("Plot subplots")
    else:
        if inPlace:
            new_parser = parser
        else:
            new_parser = deepcopy(parser)
    new_parser.add_argument("--spw", type=float, help=f"subplot width, default={plt_global['subplot_width']:.1f}")
    new_parser.add_argument("--spr", type=float, help=f"subplot ratio, default={plt_global['ratio']:.2f}")
    new_parser.add_argument("--fontsize", type=float, help=f"fontsize of labels, annotates, etc.")
    return new_parser
