from argparse import ArgumentParser
from copy import deepcopy

def addargs_settling_time(parser: ArgumentParser, new_group=False, inPlace=True):
    if parser is None:
        new_parser = ArgumentParser("Settling time analysis")
    else:
        if inPlace:
            new_parser = parser
        else:
            new_parser = deepcopy(parser)
    if new_group:
        new_group = new_parser.add_argument_group("Settling time options")
        addto = new_group
    else:
        addto = new_parser

    addto.add_argument("--eps", type=float, default=1e-1, help="epsilon to evaluate settling time")
    addto.add_argument("--t_min", type=float, default=1., help="minimal possible settling time")
    addto.add_argument("--amplitude_mode", type=str, default="retarded",
                       help="window mode that is used to calculate amplitudes ('retarded', 'precocious' or 'centered')")
    if new_group:
        return new_parser, new_group
    else:
        return new_parser
