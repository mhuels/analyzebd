import math
import numpy as np
from pathlib import Path
import itertools as it
from analyzebd.plt import *
import matplotlib.gridspec as gs
from matplotlib import cm
from analyzebd.helpers.clock import Clock
from copy import deepcopy
from analyzebd.quantities.quantity import Quantity
from tqdm import tqdm
from warnings import warn

class ParameterSpace:
    def __init__(self, property, X=None, Y=None):
        self.reset(property)
        self.setup(X, Y)

    def reset(self, property):
        self.property = property
        self.resetData()
        self.resetPlot()
        return self

    def resetData(self):
        self.X = None
        self.Y = None
        self.XFmt = "f"
        self.YFmt = "f"
        self.src = None
        self.__data = []
        self.ylim = None
        return self

    def resetPlot(self):
        self.fig = None
        self.grid = None
        self.axes = []  # TODO: maybe make this its own class that possesses its own __iter__ and __next__ functions
        self.ax_big = None
        self.clabel = None  #colorbar label
        self.bigLabelFontsize = 24
        self.innerLabelSize = 10
        self.setColorMap()
        self.cnorm = None
        return self

    def setup(self, X, Y):
        self.X = X
        self.Y = Y
        return self

    def setupPlot(self, Xsize: float=3., Ysize: float=2., kw_label_outer=None):
        lenX = len(self.X)
        lenY = len(self.Y)
        figsizeX = lenX * Xsize
        figsizeY = lenY * Ysize
        # self.fig = plt.figure(figsize=(figsizeX, figsizeY), dpi=200)
        self.fig = plt.figure(figsize=(figsizeX, figsizeY))
        self.grid = gs.GridSpec(lenY, lenX, figure=self.fig)
        for i, X in enumerate(self.X):
            self.axes.append([])
            for j, Y in enumerate(self.Y):
                # gridspec by default counts from top to bottom, we want this reversed
                self.axes[i].append(self.fig.add_subplot(self.grid[lenY - 1 - j, i]))
        if len(self.__data) > 0:
            self.labelInner()
        if kw_label_outer is None: kw_label_outer = {}
        self.labelOuter(**kw_label_outer)
        return self

    def __deepcopy__(self, memodict={}):
        new = type(self).__new__(type(self))
        memodict[id(self)] = new
        new.property = self.property
        new.X = deepcopy(self.X, memodict)
        new.Y = deepcopy(self.Y, memodict)
        new.__data = deepcopy(self.__data, memodict)
        new.ylim = deepcopy(self.ylim, memodict)

        # this should not be deepcopied
        new.src = self.src
        new.XFmt = self.XFmt
        new.YFmt = self.YFmt
        new.fig = self.fig
        new.grid = self.grid
        new.axes = self.axes
        new.ax_big = self.ax_big
        new.bigLabelFontsize = self.bigLabelFontsize
        new.setColorMap(self.__colormap)
        new.cnorm = self.cnorm
        return new

    def __getitem__(self, item):
        return self.__data[item]

    def __call__(self, X, Y):
        i = self.X(v=X)
        j = self.Y(v=Y)
        if i is None or j is None:
            return None
        else:
            return self[i][j]

    # use in for-loops, cycles through all __data entries
    def __iter__(self):
        self.iterator = np.ndindex(len(self.X), len(self.Y))
        return self     # self.iterator would instead (falsely) return i, j in for-loops

    def __next__(self):
        i, j = next(self.iterator)
        while(self[i][j] is None):  #skip entries that are None
            i, j = next(self.iterator)
        return self[i][j]

    def readFolder(self, folder, inversed=False, verbose=False, manipulate=None, dry=False, Xname=None, Yname=None, **kwargs):
        """
        Read an entire folder structure, which accommodates subfolders named with the pattern xname_xvalue_yname_yvalue.
        :param folder: str or Path
        :param inversed: bool (swap x and y in subfolder naming pattern)
        :param verbose: bool
        :param manipulate: func (function that takes self.property as sole input)
        :param kwargs: kwargs passed to property.readFromFile()
        :return: self
        """
        if Xname is None:
            Xname = self.X.name
        if Yname is None:
            Yname = self.Y.name
        self.src = Path(folder)
        with tqdm(range(len(self.X) * len(self.Y))) as counter:
            for i, X in enumerate(self.X.array):  # need the natural units (w/o conversion factor)
                self.__data.append([])
                for j, Y in enumerate(self.Y.array):
                    if inversed:
                        src_dir = self.src / f"{Yname}_{Y:{self.YFmt}}_{Xname}_{X:{self.XFmt}}"
                    else:
                        src_dir = self.src / f"{Xname}_{X:{self.XFmt}}_{Yname}_{Y:{self.YFmt}}"
                    if verbose:
                        print(f"Reading {src_dir} ... ", end='')
                    clock = Clock()
                    data = self.property()
                    try:
                        if not dry:
                            data.readFromFile(folder=src_dir, **kwargs)
                            if manipulate is not None:
                                data = manipulate(data)
                    except FileNotFoundError:
                        data = None
                    self[i].append(data)
                    if verbose:
                        print(clock())
                    counter.update()
        if dry:
            warn(f"This was a dry-read.")
        return self

    def getData(self):
        return self.__data

    def setColorMap(self, colormap="winter"):
        self.__colormap = colormap
        self.cmap = cm.get_cmap(self.__colormap)
        return self.cmap

    def getColorMap(self):
        return self.__colormap

    def _setScalarFormatter(self, ax, axis="both"):
        from matplotlib import ticker
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_scientific(True)
        formatter.set_powerlimits((-2, 2))
        if axis == "x" or axis == "both":
            ax.xaxis.set_major_formatter(formatter)
        if axis == "y" or axis == "both":
            ax.yaxis.set_major_formatter(formatter)
        return formatter

    def _getScientificNotation(self, eFmt):
        ePos = eFmt.find('e')
        prefix = eFmt[:ePos]
        exponent = eFmt[(ePos + 1):]
        exponent = f"{int(exponent):d}"
        return f"${prefix} \\cdot 10^{{{exponent}}}$"

    def labelInner(self, separate_cols=False, separate_rows=False, labelsize=None):
        for i, j in np.ndindex(len(self.X), len(self.Y)):
            ax = self.axes[i][j]
            ax.set_title("")
            ax.label_outer()
            if labelsize is not None:
                self.innerLabelSize = labelsize
            if separate_cols:
                ax.yaxis.set_tick_params(labelleft=True, labelsize=self.innerLabelSize)
            if separate_rows:
                ax.xaxis.set_tick_params(labelbottom=True)
            ax.xaxis.set_tick_params(labelsize=self.innerLabelSize)

            # scientific tick formatter
            self._setScalarFormatter(ax, axis="y")  # axis='both' might be better
            ax.yaxis.get_offset_text().set_fontsize(self.innerLabelSize)  # adjust offset text fontsize as well
        # make sure that labels are shown even if outer one is None
        self.__label_outer()
        if separate_cols:
            plt.subplots_adjust(wspace=0.25)
        else:
            plt.subplots_adjust(wspace=0.1)
        if separate_rows:
            plt.subplots_adjust(hspace=0.3)
        else:
            plt.subplots_adjust(hspace=0.15)
        return self

    def _formatTickLabels(self, iterable, fmt=None, useMathText=True):
        ticklabels = []
        for s in iterable:
            if fmt is not None:
                ticklabel = f"{s:{fmt}}"
            else:
                ticklabel = f"{s}"
            if useMathText and 'e' in ticklabel:
                ticklabel = self._getScientificNotation(ticklabel)
            ticklabels.append(ticklabel)
        return ticklabels

    def labelOuter(self, XLabel=None, YLabel=None, xtick_fmt=".1e", ytick_fmt=".1e", useMathText=True, tickFontSize=None, ytick_pad=10):
        if XLabel is None:
            if isinstance(self.X, Quantity):
                XLabel = self.X.getLatexLabel()
            else:
                XLabel = "X"
        if YLabel is None:
            if isinstance(self.Y, Quantity):
                YLabel = self.Y.getLatexLabel()
            else:
                YLabel = "Y"
        if self.ax_big is None:
            self.ax_big = self.fig.add_subplot(111, frameon=False)

        # hide ticks
        self.ax_big.tick_params(length=0)
        # big ax tick values (label on top and right side)
        wspace = self.fig.subplotpars.wspace
        hspace = self.fig.subplotpars.hspace
        xticks = self.getOuterTickPositions(N=len(self.X), epsilon=wspace)
        self.ax_big.set_xticks(xticks)

        # xticklabels = [f"{s:{xtick_fmt}}" for s in self.X]
        xticklabels = self._formatTickLabels(self.X, xtick_fmt, useMathText=useMathText)
        self.ax_big.set_xticklabels(xticklabels)
        self.ax_big.xaxis.tick_top()
        yticks = self.getOuterTickPositions(N=len(self.Y), epsilon=hspace)
        self.ax_big.set_yticks(yticks)
        # yticklabels = [f"{s:{ytick_fmt}}" for s in self.Y]
        yticklabels = self._formatTickLabels(self.Y, ytick_fmt, useMathText=useMathText)
        self.ax_big.set_yticklabels(yticklabels, rotation='vertical', va='center')
        self.ax_big.yaxis.tick_right()
        if tickFontSize is not None:
            self.ax_big.xaxis.set_tick_params(labelsize=tickFontSize)
            self.ax_big.yaxis.set_tick_params(labelsize=tickFontSize)
        self.ax_big.yaxis.set_tick_params(pad=ytick_pad)
        # big ax labels
        # it's important that the label positions (top and right) are set first
        self.ax_big.xaxis.set_label_position('top')
        self.ax_big.yaxis.set_label_position('right')
        self.ax_big.set_xlabel(XLabel, va='bottom', fontsize=self.bigLabelFontsize)
        self.ax_big.set_ylabel(YLabel, ha='center', va='top', fontsize=self.bigLabelFontsize)
        return self

    def getOuterTickPositions(self, N, epsilon):
        x = 1 / (N + (N - 1) * epsilon)
        tickPositions = []
        for i in range(N):
            x_i = (i * (1 + epsilon) + 0.5) * x
            tickPositions.append(x_i)
        return tickPositions

    def calculate_ylim(self, separate_rows=False, separate_cols=False, **kwargs):
        ylims = []
        # get all ylims and sort into nested list
        for i, X in enumerate(self.X):
            ylims.append([])
            for j, Y in enumerate(self.Y):
                data = self[i][j]
                if data is not None:
                    ylims[i].append(data.y.get_lim(**kwargs))
                else:
                    ylims[i].append([None, None])

        # determine overall/row-specific minima and maxima
        if separate_rows and separate_cols:  # everything separate
            pass
        elif separate_rows:  # only separate rows
            for j, Y in enumerate(self.Y):
                ymin = math.inf
                ymax = -math.inf
                for i, X in enumerate(self.X):
                    cur_ymin = ylims[i][j][0]
                    cur_ymax = ylims[i][j][1]
                    if cur_ymin is not None:
                        ymin = min(ymin, cur_ymin)
                    if cur_ymax is not None:
                        ymax = max(ymax, cur_ymax)
                for i, X in enumerate(self.X):
                    ylims[i][j] = [ymin, ymax]
        elif separate_cols:  # only separate columns
            for i, X in enumerate(self.X):
                ymin = math.inf
                ymax = -math.inf
                for j, Y in enumerate(self.Y):
                    cur_ymin = ylims[i][j][0]
                    cur_ymax = ylims[i][j][1]
                    if cur_ymin is not None:
                        ymin = min(ymin, cur_ymin)
                    if cur_ymax is not None:
                        ymax = max(ymax, cur_ymax)
                for j, Y in enumerate(self.Y):
                    ylims[i][j] = [ymin, ymax]
        else:  # everything homogeneous
            ymin = math.inf
            ymax = -math.inf
            for i, j in np.ndindex(len(self.X), len(self.Y)):
                cur_ymin = ylims[i][j][0]
                cur_ymax = ylims[i][j][1]
                if cur_ymin is not None:
                    ymin = min(ymin, cur_ymin)
                if cur_ymax is not None:
                    ymax = max(ymax, cur_ymax)
            for i, j in np.ndindex(len(self.X), len(self.Y)):
                ylims[i][j] = [ymin, ymax]
        self.ylim = ylims
        return self.ylim

    def set_xlims(self, xlim=None, xticks=None, xticklabels=None, scale=None):
        for i, j in np.ndindex(len(self.X), len(self.Y)):
            ax = self.axes[i][j]
            if xlim is not None:
                ax.set_xlim(xlim)
            if xticks is not None:
                ax.set_xticks(xticks)
            if xticklabels is not None:
                ax.set_xticklabels(xticklabels)
            if scale is not None:
                ax.set_xscale(scale)
        return self

    def set_ylims(self, ylim=None, factor=None, scale=None, **kwargs):
        """
        Set ylims of all axes. If ylim="auto", first calculates ylims in each data entry.
        kwargs are passed to calculate_ylim().
        :param ylim: "auto" or [ymin, ymax] or list of [ymin, ymax]
        :param factor: float (extends min and max beyong their range by (max-min)*factor/2)
        :param kwargs: kwargs passed to calculate_ylim() if ylim="auto"
        :return: self
        """
        if ylim is None:
            ylim = [None, None]
        if not isinstance(ylim, list):
            raise TypeError(f"ylim is not of type 'list'!")
        # initialize ylims as nested lists
        if not isinstance(ylim[0], list):
            ylim_tmp = []
            for i in range(len(self.X)):
                ylim_tmp.append([])
                for j in range(len(self.Y)):
                    ylim_tmp[i].append(deepcopy(ylim))
            ylim = deepcopy(ylim_tmp)
        if factor is None:
            factor = 0.1
        # calculate ylims if any ylim is None (it might be better here, if only the needed lims are calculated)
        calc_ylims = False
        for i, j in np.ndindex(len(self.X), len(self.Y)):
            if None in ylim[i][j]:
                calc_ylims = True
                break
        if calc_ylims and self.ylim is None:    # only calculate if not done before
            self.calculate_ylim(**kwargs)
        # overwrite any None values with the calculated ones (and add factors to it)
        for i, j in np.ndindex(len(self.X), len(self.Y)):
            if ylim[i][j][0] is None:
                ylim_tmp[i][j][0] = self.ylim[i][j][0]
            else:
                ylim_tmp[i][j][0] = ylim[i][j][0]
            if ylim[i][j][1] is None:
                ylim_tmp[i][j][1] = self.ylim[i][j][1]
            else:
                ylim_tmp[j][j][1] = ylim[i][j][1]
        # implemented ylims in ax-objects
        for i, j in np.ndindex(len(self.X), len(self.Y)):
            ax = self.axes[i][j]
            if self[i][j] is None:
                continue
            ylim_cur = ylim_tmp[i][j]
            diff = ylim_cur[1] - ylim_cur[0]
            # add factors to ylims if they were calculated
            if ylim[i][j][0] is None:
                ylim_cur[0] = ylim_cur[0] - factor * diff / 2
            if ylim[i][j][1] is None:
                ylim_cur[1] = ylim_cur[1] + factor * diff / 2
            ax.set_ylim(ylim_cur)
            if scale is not None:
                ax.set_yscale(scale)
        return self

    def addColorBar(self, colormap=None, cnorm=None, label=None, label_props=None, ticks="auto", pos="auto"):
        if label_props is None:
            label_props = {}
        if colormap is None:
            cmap = self.cmap
        else:
            cmap = cm.get_cmap(colormap)
        if cnorm is None:
            cnorm = self.cnorm
            if cnorm is None:
                raise ValueError("ParameterSpace.cnorm needs to be set first, for example by using plot(color_parameterSpace=)")
        height = 0.2
        width = self.getColorBarWidth(height)
        if pos == "auto":
            plt.gcf().canvas.draw()  # need to draw first to get_window_extent()
            left = self.adjustColorBarLeft(width)
            bottom = self.fig.subplotpars.bottom
        else:
            left, bottom = pos
        cbaxes = self.fig.add_axes([left, bottom, width, height])
        self.cbar = plt.colorbar(cm.ScalarMappable(norm=cnorm, cmap=cmap), ax=self.ax_big, cax=cbaxes)
        if label is not None:
            self.clabel = label
        if 'labelpad' not in label_props:
            label_props['labelpad'] = self.get_clabel_pad()
        self.cbar.ax.set_xlabel(self.clabel, **label_props)
        # if ticks == "auto":
        #     ticks = [0, 0.5, 1]
        if ticks != "auto":
            self.cbar.set_ticks(ticks)
        return self.cbar

    def get_clabel_pad(self):
        plt.gcf().canvas.draw()     # need to draw first to get_window_extent()
        xtick_height = self.axes[0][0].xaxis.majorTicks[0].label.get_window_extent().height
        return xtick_height

    def adjustColorBarLeft(self, width_rel):
        labelLeft_abs = self.ax_big.yaxis.label.get_position()[0]
        labelWidth_rel = self.ax_big.yaxis.label.get_window_extent().width / self.fig.get_window_extent().width
        # label width is always greater than colorbar width -> width_delta > 0
        width_delta = labelWidth_rel - width_rel
        left = labelLeft_abs / self.fig.get_window_extent().width + 0.5 * width_delta
        return left

    def getColorBarWidth(self, height_rel=None, ratio2height=0.2):
        plt.gcf().canvas.draw()     # need to draw first to get_window_extent()
        width_abs = 0.7 * self.ax_big.yaxis.label.get_window_extent().width
        # reduce width according to height if necessary
        if height_rel is not None:
            height_abs = self.ax_big.get_window_extent().height * height_rel
            if width_abs > ratio2height * height_abs:
                width_abs = ratio2height * height_abs
        width_rel = (width_abs) / self.fig.get_window_extent().width
        return width_rel

    # def plot_single(self, i: int, j: int, plot_func, colors: Quantity, **kwargs):

    def plot(self, plot_func, condition=None, color_parameterSpace=None, verbose=False, **kwargs):
        """

        :param plot_func:
        :param condition: np.array with same shape (len(X), len(Y))
        :param color_parameterSpace:
        :param kwargs:
        :return:
        """
        kwargs_def = {'ls': '-'}
        kwargs_def.update(kwargs)
        line = None
    # with tqdm(range(len(self.X) * len(self.Y))) as counter:
        for i, j in np.ndindex(len(self.X), len(self.Y)):
            if condition is not None:
                if condition[i, j] == False:
                    continue  # skip
            data = self[i][j]
            ax = self.axes[i][j]
            if data is None:    #skip if data not available
                self.plot_none(ax)
                continue
            if verbose:
                print(f"Plotting {data.src} ... ", end='')
            clock = Clock()
            if color_parameterSpace is not None:
                kwargs_def['colorArray'] = color_parameterSpace[i][j].y
                working_i = i
                working_j = j
            line = plot_func(data, ax=ax, **kwargs_def)
            if verbose:
                print(clock())
            # counter.update()
        if color_parameterSpace is not None:
            self.cnorm = data.lc.cnorm
            self.clabel = color_parameterSpace[working_i][working_j].y.getLatexLabel()
        self.labelInner()
        return line

    def reduceSize(self, maxLength=1000, **kwargs):
        for i, j in np.ndindex(len(self.X), len(self.Y)):
            data = self[i][j]
            data.reduceSize(maxLength=maxLength, **kwargs)
        return self

    def plot_none(self, ax, label="calculation\nmissing"):
        ax.annotate(label, (0.5, 0.5), ha='center', va='center')
        # ax.axis('off')
        # ax.xaxis.set_ticks([])
        # ax.yaxis.set_ticks([])
        return self

    def __label_outer(self):
        # xlabels
        for i in range(len(self.X)):
            j = 0   # bottom row
            ax = self.axes[i][j]
            if not ax.get_xlabel():
                new_label = ""
                while(not new_label and j < len(self.Y) - 1):
                    j += 1
                    next_data = self[i][j]
                    if next_data is not None:
                        new_label = next_data.x.getLatexLabel()
                ax.set_xlabel(new_label)
        # ylabels
        for j in range(len(self.Y)):
            i = 0   # left column
            ax = self.axes[i][j]
            if not ax.get_ylabel():
                new_label = ""
                while(not new_label and i < len(self.X) - 1):
                    i += 1
                    next_data = self[i][j]
                    if next_data is not None:
                        new_label = next_data.y.getLatexLabel()
                ax.set_ylabel(new_label)
        return self
