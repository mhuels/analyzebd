import numpy as np
from .parameter_space import ParameterSpace

class PeriodAmplitudeParameterSpace(ParameterSpace):
    def __init__(self, property, periods=None, amplitudes=None):
        super().reset(property)
        if periods is not None and amplitudes is not None:
            self.setup(periods=periods, amplitudes=amplitudes)

    def setup(self, periods, amplitudes):
        super().setup(periods, amplitudes)
        # self.XFmt = ".0e"
        # self.YFmt = "d"
        # self.XLabel = "$\\tau_\\omega/\\tau_B$"
        # self.YLabel = "$F_{dr,0}/k_BTd^{-1}$"
        return self

    def set_xlims(self, xlim="auto", xticks="auto", xticklabels="auto"):
        if xlim == "auto":
            xlim = [0, 1]  # TODO: calculate_xlim() needed
        #
        if xticks == "auto":
            xticks = np.linspace(xlim[0], xlim[1], num=6)
            # if xticklabels == "auto":
            #     # xticklabels = [f"{xtick:.1f}" for xtick in xticks]
            #     # reduce to relevant number of digits (could be better)
            #     if xticks[0] == 0:
            #         xticklabels[0] = "0"
            #     if xticks[-1] == 1:
            #         xticklabels[-1] = "1"
        for i, j in np.ndindex(len(self.X), len(self.Y)):
            ax = self.axes[i][j]
            ax.set_xlim(xlim)
            ax.set_xticks(xticks)
            if xticklabels != "auto":
                ax.set_xticklabels(xticklabels)
        return self
