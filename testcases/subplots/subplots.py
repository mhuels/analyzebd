import numpy as np
from analyzebd.functions.function import Function
from analyzebd.plt import *
from analyzebd.helpers.file_writer import savefig

def main():
    print("hello")

    f = Function()
    f.x.array = np.linspace(0, 1, num=50)
    f.y.array = np.power(f.x.array, 2)

    fig = plt.figure()
    ax = fig.add_subplot(111)

    f.plot(ax=ax)
    savefig("subplot")
    return 0

if __name__ == "__main__":
    main()