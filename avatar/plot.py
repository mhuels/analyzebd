import json
from analyzebd.plt import *
from argparse import ArgumentParser
from pathlib import Path
from math import log10, ceil
import numpy as np
from analyzebd.helpers.clock import Clock, stop_time
from analyzebd.metadata.drivenSingleParticle import MetadataDrivenSingleParticle as Metadata
from analyzebd.quantities.time import Time
from analyzebd.quantities.position.position import Position
from analyzebd.quantities.force import Force
from analyzebd.functions2D.function2D import Function2D
from analyzebd.helpers.file_writer import savefig
from analyzebd.functions2D.contour_lines import extract_contour_line
from analyzebd.helpers.format import format_mean_err
from src.solutions import *
from matplotlib.colors import Normalize, LogNorm    # needed in cinfo.json
from math import ceil
from copy import deepcopy
from lmfit import Model
from analyzebd.helpers.modes import print_supported_modes, parse_modes
from analyzebd.parser import addargs_sd_plot, addargs_units, addargs_layout
from analyzebd.plt.subplots import setup_mode_subplots

zunits = ["a", "a/2"]
supported_modes = ["heatmap", "xslices", "yslices"]

def load_xmax(path, args):
    path = Path(path).resolve()
    sd = Function2D(x=Time(name="period", name_latex="\\tau_\\omega", metadata=Metadata()),
                    y=Force(name="driving force amplitude", name_latex="F_{\\mathrm{dr},0}"),
                    z=Position(name="xmax", name_latex="x_{\\max}"))
    sd.readFromFile(folder=path.parent, filename=path.name)
    sd.x.setScale(unit="tSub", conversion=1, latex="\\tau_\\mathrm{sub}")
    sd.y.setScale(unit="Fsub0", conversion=1, latex="F_{\\mathrm{sub},0}")
    if sd.z.unit.name == "a/2":
        sd.z.array /= 2     # internal array should be saved in units of [a]
        sd.z.setScale(unit="a", conversion=1)
    elif sd.z.unit.name == "a":
        pass
    else:
        raise NotImplementedError(f"zunit={sd.z.unit} is not implemented.")
    if args.zunit == "a/2":
        sd.z.setScale(unit="a/2", conversion=2)
        pass
    elif args.zunit == "a":
        sd.z.setScale(unit="a", conversion=1)
    else:
        raise NotImplementedError(f"zunit={args.zunit} is not implemented.")
    sd.x = sd.x.convert2Frequency()
    sd.slice(x_slice=(slice(None, None, -1),), inPlace=True)

    # slice
    if args.xcut:
        len_before = len(sd.x)
        step = ceil(len_before / args.xcut)
        sd.slice(x_slice=(slice(None, None, step),), inPlace=True)
        print(f"Reduced number of x-values from {len_before} to {len(sd.x)}")
    if args.ycut:
        len_before = len(sd.y)
        step = ceil(len_before / args.ycut)
        sd.slice(y_slice=(slice(None, None, step),), inPlace=True)
        print(f"Reduced number of y-values from {len_before} to {len(sd.y)}")
    return sd

def set_scales(ax, mode, args):
    if mode == "heatmap":
        xscale = args.xscale
        yscale = args.yscale
    elif mode == "xslices":
        xscale = args.yscale
        yscale = args.zscale
    elif mode == "yslices":
        xscale = args.xscale
        yscale = args.zscale
    else:
        raise ValueError(f"mode={mode} is not supported.")
    ax.set_xscale(xscale)
    ax.set_yscale(yscale)
    return xscale, yscale

def plotContourLines(ax, sd, xmax, fit_model=None, fit_params=None, kw_fit=None,
                     approx_func=None, kw_approx=None, kw_approx_params=None, verbose=False,
                     annotate=None, kw_annotate=None, **kwargs):
    """

    :param ax:
    :param sd:
    :param xmax: float (target contour value in units of [a])
    :param fit_func: function with one positional argument and optional arguments b and xmax
    :param verbose:
    :param kw_fit:
    :param annotate: (x, y) (annotate position in axes fraction)
    :param kwargs:
    :return:
    """
    if kw_fit is None:
        kw_fit = {}
    if kw_approx is None:
        kw_approx = {}
    if kw_approx_params is None:
        kw_approx_params = {}
    if kw_annotate is None:
        kw_annotate = {}
    conversion = sd.z.unit.conversion   # 0.5 if unit is a, 1 if unit is a/2
    xmax_converted = xmax * conversion
    Fdr0 = extract_contour_line(sd, z_target=xmax_converted)
    fit_results = None
    if len(Fdr0) != 0:
        kwargs.setdefault('marker', '.')
        kwargs.setdefault('markersize', 1)
        kwargs.setdefault('linestyle', 'None')
        kwargs.setdefault('color', 'black')
        kwargs.setdefault('zorder', 2.1)  # should be drawn last
        Fdr0.plot(ax=ax, **kwargs)
        if annotate is not None:
            if None in annotate:
                xycoords = "data"
                x = Fdr0.x.max()
                y = Fdr0.y.max()
                if y >= sd.y.max() * 0.8:   # clip to upper border # TODO: implement function similar to ypad()
                    y = sd.y.max()
                    y = ypad(y, -0.01, ax)
                    x = xpad(x, -0.03, ax)
                elif x >= sd.x.max() * 0.8:   # clip to righter border (only one clip)
                    x = sd.x.max()
                    x = xpad(x, -0.001, ax)
                    y = ypad(y, 0.06, ax)
                annotate = (x, y)
            else:
                xycoords = "axes fraction"
            kw_annotate.setdefault("va", "top")
            kw_annotate.setdefault("ha", "right")
            kw_annotate.setdefault("fontsize", "7")
            ax.annotate(f"{xmax:f}".rstrip('0').rstrip('.'), xy=annotate, xycoords=xycoords, **kw_annotate)
        x = np.logspace(log10(sd.x.min()), log10(sd.x.max()), 256)
        if fit_model is not None and len(Fdr0) >= 2:
            fit_params['xmax'].value = xmax     # doesn't need to be converted
            fit_results = fit_model.fit(Fdr0.y.getArray(), fit_params, w=Fdr0.x.getArray())
            if verbose:
                for name in fit_results.params.keys():
                    if fit_results.params[name].vary is False: continue
                    print(f"{name}(xmax={xmax}a) = "
                          f"{format_mean_err(fit_results.params[name].value, fit_results.params[name].stderr)}")
            kw_fit.setdefault('color', 'black')
            kw_fit.setdefault('linewidth', 1)
            ax.plot(Fdr0.x.getArray(), fit_results.best_fit, **kw_fit)
        if approx_func is not None:
            kw_approx.setdefault('color', 'black')
            kw_approx.setdefault('linewidth', 1)
            kw_approx.setdefault('ls', 'dashed')
            ax.plot(x, approx_func(x, xmax=xmax, **kw_approx_params), **kw_approx)
    return Fdr0, fit_results

def main():
    clock = Clock()

    parser = ArgumentParser("Plot heatmap, x-slices and y-slices")
    parser.add_argument("infile", type=str, nargs='?', default="xmax.txt", help="input file")
    addargs_sd_plot(parser, supported_modes=supported_modes)
    addargs_units(parser, zunits=zunits)
    addargs_layout(parser)
    parser.add_argument("--linewidth", type=float, default=1, help="line width of slice plots")
    args = parser.parse_args()
    modes = parse_modes(args.modes, supported_modes)

    # read data
    sd_in = load_xmax(args.infile, args)

    # interpolate missing values
    # if args.interpolate_missing:
    #     # tmp = deepcopy(sd_in)
    #     sd_in.interpolate_missing(inPlace=True)

    # setup plot
    fig, axes = setup_mode_subplots(modes, spw=args.spw, spr=args.spr)

    # setup axes for each plot
    ax_mode_map = {}
    for i, mode in enumerate(modes):
        ax_mode_map[mode] = fig.axes[i]

    # setup objects for each plot
    sd_dict = {}
    for i, mode in enumerate(modes):
        sd = deepcopy(sd_in)
        # settings
        if mode == "heatmap":
            if sd.z.unit.name == "a":
                center = 1/2
            elif sd.z.unit.name == "a/2":
                center = 1
            sd.center_cnorm(center=center, scale=args.zscale)
            sd.setColorMap(args.colormap_heatmap)
            sd.shading = "auto"
        elif mode == "xslices":
            sd.setColorMap(args.colormap_xslices)
        elif mode == "yslices":
            sd.setColorMap(args.colormap_yslices)
        sd_dict[mode] = sd

    # scales
    for i, mode in enumerate(modes):
        set_scales(ax=ax_mode_map[mode], mode=mode, args=args)

    # plot
    for i, mode in enumerate(modes):
        with stop_time(f"Plotting {mode} ({i + 1}/{len(modes)})", clock):
            ax = ax_mode_map[mode]
            sd = sd_dict[mode]
            kw_slices = {'withColorBar': True, 'markersize': 3, 'linewidth': args.linewidth}
            # kw_slices['marker'] = '.'
            if mode == "heatmap":
                sd.plotHeatmap(ax=ax, bad_color="silver")
            elif mode == "xslices":
                sd.plotSlices(ax=ax, axis='x', cscale=args.xscale, **kw_slices)
            elif mode == "yslices":
                sd.plotSlices(ax=ax, axis='y', cscale=args.yscale, **kw_slices)
            else:
                raise ValueError(f"mode={mode} is not supported.")

    if "heatmap" in modes:
        # critical line
        print(f"\nPlotting critical contour line")
        # def f(w, b=1, xmax=1):    # returns contour line in units of
        #     return criticalDrivingForceAmplitude(w, b=b)  #xmax is not needed in this case
        # fmodel = Model(f)
        # params = fmodel.make_params(b=1, xmax=1)
        fmodel = Model(contourLineDrivingForceAmplitudePinned2)
        params = fmodel.make_params(a=1, xmax=0.5)
        params['xmax'].vary = False
        F0crit, _ = plotContourLines(ax=ax_mode_map["heatmap"], sd=sd_in, xmax=1/2, verbose=True,
                                     # fit_model=fmodel, fit_params=params, kw_fit={'label': 'Fit'},
                                     approx_func=contourLineDrivingForceAmplitudePinned2, kw_approx_params={'a': np.pi},
                                     kw_approx={'label': '$F_{\\mathrm{dr},0}^\\mathrm{crit}(\\omega)$', 'ls': 'solid'},
                                     label="$F_{\\mathrm{dr},0}^{x_\\mathrm{max}}(\\omega)$", marker='.',
                                     annotate=(None, None))

        # high frequency limit
        ax = ax_mode_map["heatmap"]
        x = np.logspace(log10(sd_in.x.min()), log10(sd_in.x.max()), 256)
        kwargs = {'lw': 1}
        # ax.plot(x, criticalDrivingForceAmplitude(x), label="$\\sqrt{1 + (\\pi \\omega \\tau_\\mathrm{sub})^2}$", **kwargs)
        ax.plot(x, criticalDrivingForceHighW(x), label="$\\pi \\omega \\tau_\\mathrm{sub}$", color='C1', ls='dashdot', **kwargs)
        ax.plot(x, criticalDrivingForceAmplitudeBesselHighW(x), label="$j_{0,1} \\omega \\tau_\\mathrm{sub}$", color='C2', ls='dashed', **kwargs)

        # for xmax in [0.03, 0.1, 2, 10, 40]:
        #     ax.plot(x, contourLineDrivingForceAmplitudeFree(x, xmax=xmax), **kwargs, color='C1')

        # test new critical line
        # print(f"\nPlotting second critical contour line")
        # def f(w, b=1, xmax=1):    # returns contour line in units of
        #     return criticalDrivingForceAmplitudeBessel(w)
        # fmodel = Model(f)
        # params = fmodel.make_params(b=1, xmax=1)
        # params['xmax'].vary = False
        # params['b'].vary = False
        # color = 'red'
        # F0crit, _ = plotContourLines(ax=ax_mode_map["heatmap"], sd=sd_in, xmax=1/2, verbose=True,
        #                              # fit_model=fmodel, fit_params=params, kw_fit={'label': 'Fit'},
        #                              approx_func=f, kw_approx={'color': color}, #kw_approx_params={'b': 1},
        #                              marker='.', annotate=(None, None), color=color)

        # contour lines below 1/2
        print(f"\nPlotting contour lines in pinned region")
        xmaxs = [0.008, 0.03, 0.1]     # xmax in units of a (lattice constant)
        for i, xmax in enumerate(xmaxs):
            fmodel = Model(contourLineDrivingForceAmplitudePinned2)
            params = fmodel.make_params(xmax=1, a=1)
            params['xmax'].vary = False
            params['a'].max = np.pi
            # alpha = 0.5
            alpha = 1
            color = 'gray'
            kw_approx = {'color': color, 'alpha': alpha, 'ls': 'solid', 'linewidth': 0.5}
            if i == len(xmaxs) - 1: kw_approx['label'] = '$F_{\\mathrm{dr},0}^{x_{\\max}, \\mathrm{pin}}(\\omega)$'
            #if i == 0: kw_approx['label'] = '$F(\\omega)$'
            plotContourLines(ax=ax_mode_map["heatmap"], sd=sd_in, xmax=xmax, verbose=True,
                             # fit_model=fmodel, fit_params=params, kw_fit={'color': color, 'alpha': alpha},
                             approx_func=contourLineDrivingForceAmplitudePinned2, kw_approx_params={'a': 1},
                             kw_approx=kw_approx,
                             annotate=(None, None), kw_annotate={'color': lighten_color(color, 1.5)},
                             marker='.', linestyle='None', linewidth=1, color=lighten_color(color, 1.2), alpha=alpha)

        # contour lines above 1/2
        print(f"\nPlotting contour lines in depinned region")
        xmaxs = [2., 10., 40.]  # xmax in units of a (lattice constant)
        # xmaxs = [1/2, 1., 2., 5., 10., 20., 40., 100.]  # xmax in units of a (lattice constant)
        for xmax in xmaxs:
            fmodel = Model(contourLineDrivingForceAmplitudeDepinnedBessel)   # works best so far, but d(xmax) is unknown
            params = fmodel.make_params(xmax=1, c=1, d=0)
            params['xmax'].vary = False
            params['c'].vary = False
            # alpha = 0.5
            alpha = 1
            color = 'gray'
            plotContourLines(ax=ax_mode_map["heatmap"], sd=sd_in, xmax=xmax, verbose=True,
                             # fit_model=fmodel, fit_params=params, kw_fit={'color': color, 'alpha': alpha},
                             # approx_func=contourLineDrivingForceAmplitudeDepinnedBessel2, kw_approx_params={'c': 1, 'd': 4},
                             kw_approx={'color': color, 'alpha': alpha},
                             annotate=(None, None), kw_annotate={'color': lighten_color(color, 1.5)},
                             marker='.', linestyle='None', linewidth=1, color=lighten_color(color, 1.2), alpha=alpha)

    # legends
    for ax in [ax_mode_map["heatmap"]]:
        ax.legend(loc='lower right', handlelength=1.5, prop={'size': 7}) #, prop={'size': 20}

    # titles
    # ax_heatmap_ss.set_title("Many particle system")

    # # boxes
    # boxprops = {'boxstyle': 'round', 'facecolor': 'white', 'alpha': 0.5}
    # if not args.plain:
    #     for ax in [ax_mode_map["heatmap"]]:
    #         ax.annotate("pinned", (0.8, 0.25), xycoords="axes fraction", va='center', ha='center',
    #                            bbox=boxprops)
    #         ax.annotate("temporarily\ndepinned", (0.25, 0.8), xycoords="axes fraction", va='center', ha='center',
    #                            bbox=boxprops)
    #

    # show/save
    if args.save is not None:
        if args.save == "auto":
            outname = Path(args.infile).resolve().with_suffix("")
        else:
            outname = Path(args.save)
        # main plot
        savefig(outname, formats=args.formats)
        print("")
        # critical line
        if "F0crit" in locals():
            F0crit.writeToFile("critical_driving_amplitude.txt", verbose=True)
        # colormap info
        sd_dict["heatmap"].save_colormap_info("cinfo_heatmap.hdf5", verbose=True)
    clock.lap()
    print(f"Total runtime: {clock(0, -1, unit='s')}s, {clock(0, -1)}")

    if not args.noshow:
        plt.show()
    plt.close()
    return 0

if __name__ == "__main__":
    main()
