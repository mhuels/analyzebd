from setuptools import setup, find_packages

setup(name='analyzebd',
      version='1.2.221014',
      packages=find_packages(),
      install_requires=['matplotlib>=3.4.1', 'pathlib', 'scipy>=1.7.1', 'pandas>=1.3.1', 'tqdm', 'file_read_backwards', 'h5py'],
      entry_points={'console_scripts':    # no '-' in console_scripts names
                          # ['visualizebd=analyzebd.scripts.visualize2:main',
                          ['visualizebd=analyzebd.scripts.visualize:main',
                           ]
                    }
      # scripts=["scripts/visualize"]     # alternative to entry_points
      )
